
class FRW: public Model{

public:

	/// Dust and curvature-only FRW model
	FRW(double _h, double _om, double _ok){
		/// Constructor: Dust+curvature FRW model
		omega_m = _om; // Matter density
		omega_k = _ok; // Curvature effective energy density
		h = _h; // Hubble parameter
		// NOTE: Parameters which reproduce a curve like in the CFL paper:
		// KR = 1e-10, A = -1e-11, M0 = 1e8 (but was this in a different unit system?)
		go();
	}
	
	double m(double r){
		/// Density-like function, m(r)
		return (3.*omega_m/(8.*M_PI*G)) * pow(fH0*h, 2.0);
	}
	
	double mprime(double r){
		/// Radial derivative of density-like function
		return 0.0;
	}

	double k(double r){
		/// Curvature-like function, k(r)
		return -1. * omega_k * pow( (fH0*h/C) , 2.0);
	}

	double kprime(double r){
		/// Radial derivative of curvature-like function, k'(r)
		return 0.0;
	}

private:

	double omega_m, omega_k;

};
