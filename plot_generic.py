#!/usr/bin/python

from pylab import *
import scipy.misc as sci
import scipy.integrate as sin

h = 0.72
G = 6.67e-11
C = 3e8
fH0 = 3.24e-18 # sec^-1
PI = 3.14159265
MPC = 3.08568e22 # m per Mpc
MSUN = 2e30 # kg
PIG = (8. * PI * G) / 3.
PCR = ((0.72*fH0)**2) / PIG

def K(r, p):
	return p['A'] * exp(-1. * (r/p['r0'])**3.)

def M(r, p):
	# M = M0, set by the Hubble rate locally
	return ( (fH0*p['h'])**2 + K(0.0, p)*C*C ) / PIG
	
def a1dot(a, r, p):
	return sqrt( PIG*M(r,p)/a - K(r,p)*C*C )

def a1dot_konly(a, r, p):
	return sqrt(a*0.0 + -1.*K(r,p)*C*C )

def f(x, args1, args2):
	print args1, args2
	return x

##argss = ["x", 5]
##print sci.derivative(f, 2.0, dx=1e-4, n=1, args=argss, order=5)

# Define parameters [m, A, r0]
params = []
kk = -1.*(0.72*fH0/C)**2

params.append( {'A': 0.5*kk, 'r0': 500*MPC, 'h': 0.72, 'mk': 'r-'} )

# Plotted variables
a = arange(0.1, 2.0, 0.0001)
rrange = arange(0.0, 10.0, 0.1)

# Add curves to plot
subplot(111)

rr = arange(0.0, 2.0, 0.1)
aarange = arange(0.1, 2.0, 0.1)
p = params[0]

i = 0
for aa in aarange:
	plot(rr, a1dot(aa, rr*p['r0'], p)/(fH0*aa), color=str(0.7 - i*0.7/len(aarange)), linestyle='-')
	i+=1
	
grid(True)
title('h(r) at different a, from a=0.1 (top) to a=2.0 (bottom)')

xlabel('r/r0')
ylabel('h(r)')

#xscale('log')
#yscale('log')

show()
