#!/usr/bin/python

import numpy
import scipy.optimize as opt
from pylab import *

# Get data
r = []
a1 = []
a1dot = []
a2 = []
a2dot = []

f = open("data/background", 'r')
for line in f.readlines():
	tmp = line.split("\t")
	if len(tmp) >= 4:
		r.append(float(tmp[0]))
		#density.append(float(tmp[1]))
		a1.append(float(tmp[2]))
		a1dot.append(float(tmp[3]))
		a2.append(float(tmp[4]))
		a2dot.append(float(tmp[5]))
		#Ht.append(float(tmp[6]))
		#Hr.append(float(tmp[7]))
		#k.append(float(tmp[8]))
f.close()

# Set data variables
##x = numpy.arange(0.0, 10.0, 0.5)
##y = x**1 + 3.
x = numpy.array(r[100:150])
y = numpy.array(a1[100:150])
y2 = numpy.array(a2[100:150])

# Set params based on order of polynomial
N = 12
params = []
for i in range(N):
	params.append(1.0)

# Function which we intend to fit the data
def fn(a):
	fn = a[0] * 1. # Set initial vector to the 0th order term
	for n in range(N-1):
		fn += a[n+1] * x**(n+1) # add the higher-order terms
	return fn

# Derivative of fitting function
def dfn(a):
	fn = a[1] * 1. # Set initial vector to the 0th order term
	for n in range(N-2):
		fn += (n+2) * a[n+2] * x**(n+1) # add the higher-order terms
	return fn

# Vectorised function which computes the data/function difference
def minfn(a):
	# Best-fit function
	return y - fn(a)

# Do the least-squares fit
#(fitparams, flag, cov, info, mesg) 
(fitparams, cov, info, mesg, flag) = opt.leastsq( minfn, params, args=(), full_output=True )
print "Flag:", flag
print "Mesg:", mesg

# Output resulting parameters
i = 0
for p in fitparams:
	print "   x^" + str(i) + ": " + str(p)
	i += 1

# Plot the data and plot the fitting function
#plot(x, y, 'k--', x, fn(fitparams), 'r-', x, dfn(fitparams), 'b--',)
#plot(x, y - fn(fitparams), 'k--')
plot(x, x*dfn(fitparams) + fn(fitparams) - y2, 'r-')
grid('show')
show()
