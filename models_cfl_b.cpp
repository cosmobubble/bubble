
class CFLb: public Model{

public:

	/// Clifton, Ferreira, Land, LTB model (b)
	CFLb(double _h, double _A, double _w, double _M0){
		/// Constructor: CFL model (b), k~exp(-cr^3)
		A = _A; // K(r) amplitude scale
		KR = 1./(_w*_w*_w); // K(r) curvature scale
		M0 = _M0; // Density amplitude scale
		h = _h; // Hubble parameter
		// NOTE: Parameters which reproduce a curve like in the CFL paper:
		// KR = 1e-10, A = -1e-11, M0 = 1e8 (but was this in a different unit system?)
		//file_id = int(_w/1e2);
		go();
	}
	
	CFLb(double _h, double _q, double _w){
		/// Constructor: CFL model (b), k~exp(-cr^3)
		/// M0 set to give h0 = h at a1(r=0, today) = 1
		A = -1.0 * _q * pow( (_h*fH0/C), 2.0); // K(r) amplitude scale, where (H0/C)^2 is the maximum allowed
		KR = 1./(_w*_w*_w); // K(r) curvature scale
		h = _h; // Hubble parameter
		M0 = (3. / (8.*M_PI*G)) * ( (fH0*fH0*h*h) + A*C*C ); // Density amplitude scale
		//file_id = int(_w/1e2);
		
		// Name this model based on the passed parameters
		stringstream modelinfo(stringstream::in | stringstream::out);
		modelinfo << "CFLb, h=" << h << ", q=" << _q << ", w=" << _w << endl;
		output_details(modelinfo.str());
		
		go(); // Build the model
	}
	
	double m(double r){
		/// Density-like function, m(r)
		return M0;
	}
	
	double mprime(double r){
		/// Radial derivative of density-like function
		return 0.0;
	}

	double k(double r){
		/// Curvature-like function, k(r)
		return A * exp(-KR * r*r*r);
	}

	double kprime(double r){
		/// Radial derivative of curvature-like function, k'(r)
		return -3. * KR * A * r*r * exp(-KR * r*r*r);
	}
	
	/*
	double tb(double r){
		/// Bang-time function, Myr
		return 50.0*exp(-1.5*KR*(r-1500.)*(r-1500.));
	}
	
	double tbprime(double r){
		/// Radial derivative of Bang-time function
		return -50.0 * 2. * 1.5 * KR * (r-1500.) * exp(-1.5*KR*(r-1500.)*(r-1500.));
	}*/

private:

	double A, KR, M0;

};
