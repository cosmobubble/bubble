
class Model{
	// Calculated grid variables

protected:
	
	gsl_integration_workspace *integ_w;
	const gsl_odeiv_step_type *ode_T;
	gsl_odeiv_step *ode_s;
	gsl_odeiv_control *ode_c;
	gsl_odeiv_evolve *ode_e;
	
	
	void init_integrators();
	void free_integrators();
	
	vector<vector <double> > g_a1;
	vector<vector <double> > g_a1dot;
	vector<vector <double> > g_a2;
	vector<vector <double> > g_a2dot;
	vector<double> g_x;
	vector<double> g_y;
	
	vector<double> rval;
	vector<double> tval;
	vector<double> zval;
	
	double tcell; // The distance between samples on the y grid
	
	void prepare_grid();
	void populate_grid();
	void generate_geodesic();
	void hubble_time();
	
	int get_x_index(double x);
	int get_y_index(double y);
	int get_max_j(double x);
	void populate_timesample_vector();
	void calculate_a2grid();
	
	double a2prime(double r, double t);
	double a1(double r, double t, bool tt = TIME_GLOBAL);
	double a1dot(double r, double t, bool tt = TIME_GLOBAL);
	double a2(double r, double t, bool tt = TIME_GLOBAL);
	double a2dot(double r, double t, bool tt = TIME_GLOBAL);
	
	double get_array_value(double r, double t, int v, bool time_type);

	double xM(double x);
	double xMprime(double x);
	double xK(double x);
	double xKprime(double x);
	double xTb(double x);
	double xTbprime(double x);
	
	void output_contour_handler(double (Model::*fn)(double, double), string fname);
	
	string TEST_FILENAME;
	string filename_ending;
	int file_id;

public:

	// TESTING
	static int static_a1dot (double t, const double a[], double adot[], void *params);
	static int a1dot_jac(double t, const double a[], double *dfda, double dfdt[], void *params);
	double s_a1dot(double a, double m, double k);
	double da1dot_da(double a, double m, double k);
	
	// A struct which carries parameters which can be passed to GSL ODE solver routines
	struct ModelPointers{
		double m; // A pre-calculated value of m(r)
		double k; // A pre-calculated value of k(r)
		double sign; // The sign of a geodesic
		double rstart; // Starting point of a calculated geodesic
		double rend; // End point of a calculated geodesic
		double t_rstart; // Time at which the geodesic was at rstart
		bool geodesic_direction; // The direction in which the geodesic is going
		Model *mod; // A pointer to a model, allowing access to all of its public functions from a static function
	};
	
	double adaptive_t_fnr(double rstart, double rend, double tstart, bool ingoing = false);
	static int ft_jac(double r, const double t[], double *dfdt, double dfdr[], void *params);
	static int static_ft (double r, const double t[], double dtdr[], void *params);
	
	static double static_z_integrand (double r, void * params);
	double adaptive_I(double rstart, double rend, double t_rstart, bool geodesic_direction=GEODESIC_OUTGOING);
	
	double adaptive_v_dipole(double robs);
	
	double adaptive_r_fnt(double tstart, double tend, double rstart, bool ingoing);
	static int fr_jac(double t, const double r[], double *dfdr, double dfdt[], void *params);
	static int static_fr (double t, const double r[], double drdt[], void *params);
	// END TESTING
	
	double fn_r(double t);
	double fn_t(double r);
	double fn_tz(double z);
	double z(double r);
	
	int XCELLS, YCELLS;
	double XMAX, XMIN, XSTEP, YSTEP;
	double TBMIN;
	
	double a1_0, x0, y0, dy, y_today, t_today, r0, t0;
	double dt;
	double h, H0;
	
	virtual double m(double r);
	virtual double mprime(double r);
	virtual double k(double r);
	virtual double kprime(double r);
	virtual double tb(double r);
	virtual double tbprime(double r);
	
	double density(double r, double t);
	double Ht(double r, double t);
	double Hr(double r, double t);
	double DM(double r);
	double DM_milne(double r);
	double dL_Milne(double r);
	double dL(double r);
	double DM_EdS(double r);
	double dL_EdS(double r);
	double EdS_fn_r(double t);
	double z_EdS(double r);
	double dA(double r);
	double vp_frw(double r);
	
	//double v_dipole(double r, double ZLIM); // Use this definition if you're doing integrand output for off-centre obs.
	double v_dipole(double r, double* r_end);
	double outgoing_redshift(double r_start, double t_start, double t_end);
	double ingoing_redshift(double r_start, double t_start, double t_end, double* r_end);
	
	// TESTING
	double general_offcentre(double robs, double tobs, double _gamma, double tend);
	
	void output_background(double t);
	void output_background();
	void output_geodesic();
	void output_model_stats();
	void output_contour();
	void output_velocities();
	void output_velocity_mismatch();
	void output_kszvpec_for_z();
	void output_fnz();
	//void output_zin();
	void output_details(string model_title);

Model(){
	// Constructor. Don't do anything here yet. Do it all in the derived class.
	file_id = -1;
}

void go(){
	/// Construct the class, do all the calculations
	
	if(file_id==-1){
		filename_ending = "";
	}else{
		stringstream tmp(stringstream::in | stringstream::out);
		tmp << "-" << file_id;
		filename_ending = tmp.str();
	}
	
	cerr << "\tprepare_grid()" << endl;
	prepare_grid(); // Build an empty grid
	
	cerr << "\tpopulate_grid()" << endl;
	populate_grid(); // Populate the grid with values
	
	cerr << "\tgenerate_geodesic()" << endl;
	generate_geodesic(); // Calculate the radial geodesic for the central observer today
}

};
