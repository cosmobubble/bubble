#!/usr/bin/python

import math

# SNCAT Catalogue from http://www.sai.msu.su/sn/sncat/, downloaded 28-Jan-2011
Sname = [] # Supernova name, field 0
z = [] # Redshift, field 10
RA = [] # Supernova RA, field 25
Dec = [] # Supernova Dec, field 26

# Union2 catalogue from http://supernova.lbl.gov/Union/descriptions.html#FullTable
Uname = [] # Supernova name, field 0
Uz = [] # Supernova redshift, field 1
DM = [] # Supernova distance modulus, field 8
eDM = [] # Error on the supernova distance modulus, field 9
Ui = [] # The index in the SNCAT catalogue corresponding to this supernova (-1 == no match)

# kSZ data from Table 1, Garcia-Bellido and Haugebolle, JCAP 09 (2008) 016
Kname = [] # Cluster name, field 0
Kz = [] # Cluster redshift, field 1
Kvp = [] # Cluster peculiar velocity, field 2
Kvpe1 = [] # Error on vpec (upper), field 3
Kvpe2 = [] # Error on vpec (lower), field 4
KRA = [] # Cluster RA (calculated from galactic l and b using online calc), field 8
KDec = [] # Cluster Dec (calculated from galactic l and b), field 9

# Peculiar velocity data from SFIpp (see Hitchhikers talk for OpenOffice spreadsheet)
Pname = [] # Group name, field 0
PRA = [] # Group RA, field 1
PDec = [] # Group Dec, field 2
Pz = [] # Redshift, field 3
Pvp = [] # Peculiar velocity (uncorrected), field 7
Pvpe = [] # Error on peculiar velocity, field 8


def Distance(r1, d1, r2, d2):
	"""Calculate the distance between (RA, Dec)1 and (RA, Dec)2"""
	dr = math.fabs(r1 - r2)
	if dr > 180.:
		dr = dr - 180.
	dd = math.fabs(d1 - d2)
	if dd > 180.:
		dd = dd - 180.
	
	return math.sqrt(dd*dd + dr*dr)
	

def convertRA(rastr):
	"""Convert an RA string into a decimal"""
	rastr = rastr.replace("- ", "-")
	rastr = rastr.replace("+ ", "+")
	r = rastr.split(" ")
	if len(r)==4:
		# There's a leading space
		return float(r[1])*(360./24.) + float(r[2])*(360./(24.*60.)) + float(r[3])*(360./(24.*3600.))
	else:
		return float(r[0])*(360./24.) + float(r[1])*(360./(24.*60.)) + float(r[2])*(360./(24.*3600.))

def convertDec(decstr):
	"""Convert a Dec string into a decimal"""
	decstr = decstr.replace("- ", "-")
	decstr = decstr.replace("+ ", "+")
	r = decstr.split(" ")
	# Get the sign right
	if float(r[1]) < 0.0:
		sgn = -1.
	else:
		sgn = 1.
		
	if len(r)==4:
		# There's a leading space
		return sgn * (  sgn*float(r[1]) + (float(r[2])/60.) + (float(r[3])/3600.)  )
	else:
		return sgn * (  sgn*float(r[0]) + (float(r[1])/60.) + (float(r[2])/3600.)  )


# Read the SNe database file
f = open('sncat_latest_view.txt', 'r')
for line in f.readlines():
	line = line.split(" |")
	if len(line) > 26:
		try:
			# Get the variables from the file, if we can
			ss = line[0].replace(" ", "")
			zz = float(line[10].replace(" ", ""))
			rr = convertRA(line[25])
			dd = convertDec(line[26])
			
			# Add the variables to the arrays
			Sname.append(ss)
			z.append(zz)
			RA.append(rr)
			Dec.append(dd)
		except:
			pass
f.close()

# Read the cosmology SNe z/deltaDM database
f = open('tableSCPUnion2.dat', 'r')
for line in f.readlines():
	line = line.split("\t")
	if len(line) > 9:
		try:
			# Get the variables from the file, if we can
			uu = line[0].replace(" ", "")
			zz = float(line[1])
			dd = float(line[8])
			ee = float(line[9])
			
			# Add the variables to the arrays
			Uname.append(uu)
			Uz.append(zz)
			DM.append(dd)
			eDM.append(ee)
			Ui.append(-1)
		except:
			pass
f.close()

# Read-in the kSZ data
f = open('GBH_kSZ_data.dat', 'r')
for line in f.readlines():
	line = line.split("\t")
	if len(line) > 8:
		try:
			# Get the variables from the file, if we can
			kk = line[0].replace(" ", "")
			zz = float(line[1])
			vp = float(line[2])
			vpe1 = float(line[3])
			vpe2 = float(line[4])
			kra = float(line[8])
			kdec = float(line[9])
			
			# Add the variables to the arrays
			Kname.append(kk)
			Kz.append(zz)
			Kvp.append(vp)
			Kvpe1.append(vpe1)
			Kvpe2.append(vpe2)
			KRA.append(kra)
			KDec.append(kdec)
		except:
			pass
f.close()

# Read-in the SFI++ data
f = open('SFIpp_groups.dat', 'r')
for line in f.readlines():
	line = line.split("\t")
	if len(line) > 8:
		try:
			# Get the variables from the file, if we can
			pp = line[0].replace(" ", "")
			rr = convertRA(line[1])
			dd = convertDec(line[2])
			zz = float(line[3])/3.0e5 # Convert from km/sec
			vp = float(line[7])
			vpe = float(line[8])
			
			# Add the variables to the arrays
			Pname.append(pp)
			PRA.append(rr)
			PDec.append(dd)
			Pz.append(zz)
			Pvp.append(vp)
			Pvpe.append(vpe)
		except:
			pass
f.close()


# Search SNe and try to cross-match between Union2 and SNCAT
for i in range(len(Uname)):
	# Loop through each item in Union2
	candidate = -1
	for j in range(len(Sname)):
		# Loop through each item in SNCAT
		# SNCAT names are more complicated than Union2 names - they have prefixes like "HST"
		if Uname[i].lower() in Sname[j].lower():
			candidate = j
			break
	if candidate >= 0:
		Ui[i] = candidate

# Output matching stats
num = 0
for i in range(len(Uname)):
	if(Ui[i]>=0):
		num += 1
		j = Ui[i]
		#print Uname[i], Sname[j], Uz[i], z[j], Dec[j]
print num, "matches were found, out of a possible", len(Uname), "(" + str(round(100.0*num/len(Uname), 2)) + "%)"

# See if any SNe can be found near to the kSZ clusters
for i in range(len(Kname)):
	for j in range(len(Uname)):
		# Check if the cluster has an RA/Dec coord identified for it
		if(Ui[j]>=0):
			k = Ui[j]
			if Distance(KRA[i], KDec[i], RA[k], Dec[k]) < 1.5:
				print "Cluster", Kname[i], "is", round(Distance(KRA[i], KDec[i], RA[k], Dec[k]), 3), "deg from SN", Sname[k], "and has dz =", Kz[i] - z[k]
				print "\t (RA, Dec) =", KRA[i], KDec[i], "(cluster),", RA[k], Dec[k], "(SN)"

# See if any v_pec measurements can be found near to the kSZ clusters
for i in range(len(Kname)):
	for j in range(len(Pname)):
		if Distance(KRA[i], KDec[i], PRA[j], PDec[j]) < 2.5:
				print "Cluster", Kname[i], "is", round(Distance(KRA[i], KDec[i], PRA[j], PDec[j]), 3), "deg from Group", Pname[j], "and has dz =", Kz[i] - Pz[j]
				print "\t (RA, Dec) =", KRA[i], KDec[i], "(cluster),", PRA[j], PDec[j], "(group)"

