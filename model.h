// Header for main.cpp
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv.h>
#include <gsl/gsl_integration.h>

// Boolean values
const bool TIME_LOCAL = false;
const bool TIME_GLOBAL = true;
const bool GEODESIC_INGOING = true;
const bool GEODESIC_OUTGOING = false;

// Units: Mass (Msun); Time (Myr); Distance (Mpc)
const double G = 4.4986e-21; // Gravitational constant G, in units of Mpc^3 Msun^-1 Myr^-2
const double C = 0.30659458; // Speed of light, in Mpc/Myr
const double GYR = 1000.; // One Gyr in units of time (Myr)
const double fH0 = 1.02269e-4; // H0 = 100h km s^-1 Mpc^-1 = fH0*h Myr^-1
//const double A1_REC = 9.17e-4; // FRW scale factor at recombination
//const double T_REC = 0.380; // Age of universe at recombination (Myr)
const double A1_REC = 1e-7; // FRW scale factor at an early time
const double T_REC = 1e-7; // Age of universe at an early time (but actually, made up) (Myr)

const double A_MAX = 1.0; // The maximum a1 value to calculate

const double ts = 1000.0; // 1 GYR
const double rs = 100.0; // 100 MPC

#include "model_init.cpp"

#include "model_geodesic_build.cpp"
#include "model_geodesic_interp.cpp"
#include "model_grid_build.cpp"
#include "model_grid_interp.cpp"
#include "model_velocity_profile.cpp"
//#include "model_velocity_profile_testing.cpp"
#include "model_obs.cpp"
#include "model_params.cpp"
#include "model_def.cpp"
#include "model_output.cpp"

// TESTING
#include "model_integrate_geodesic.cpp"

#include "offcentre_general.cpp"

/*
const double G = 6.67e-11; // Gravitational constant G, in units of m^3 kg^-1 s^-2
const double C = 3e8; // Speed of light, in m/s
const double GYR = 3.16e16; // One Gyr in units of time (s)
const double fH0 = 3.24e-18; // H0 = 100h km s^-1 Mpc^-1 = fH0*h Myr^-1
const double A1_REC = 1e-3; // FRW scale factor at recombination
const double T_REC = 1.26e13; // Age of universe at recombination (400 kyr)

const double A_MAX = 1.0; // The maximum a1 value to calculate

const double ts = 1e13; // ~ 1 Myr
const double rs = 3e24; // ~ 100 MPC
*/
