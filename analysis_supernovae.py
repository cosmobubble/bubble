#!/usr/bin/python

class obsDM(object):
	"""A collection of supernova/GRB data points"""
	
	def load_sn_data(self):
		"""Load the SN/GRB data from a file"""
		f = open(self.filename, 'r')
		for line in f.readlines():
			line = line.split(" ")
			if len(line) > 5 and line[0]!="#":
				self.zmin.append(float(line[0]))
				self.zmax.append(float(line[1]))
				self.z.append(float(line[2]))
				self.DM.append(float(line[3]))
				self.errDM.append(float(line[4]))
				self.source.append(str(line[5][:-1]))
	
	def __init__(self):
		self.filename = "obsdata/SNe_Wright.dat" # The file to retrieve the data from
		self.z = [] # The mean z value taken for this measurement
		self.zmin = [] # The minimum z bound for this measuremnet
		self.zmax = [] # The maximum z bound for this measuremnet
		self.DM = [] # The measured distance modulus
		self.errDM = [] # The error on the distance modulus
		self.source = [] # A string denoting the source of the data
		
		self.load_sn_data()

def get_closest(z, zvec):
	"""Get the data point from the model (zvec) with the closest z value 
	to some real deltaDM measurement (z). Returns the index in 
	zvec which comes closest. Assumes that zvec is sorted."""
	a = 0
	b = len(zvec) - 1
	
	while (b-a)>1:
		mid = a + int(0.5*float(b-a))
		if zvec[mid] > z:
			b = mid
		else:
			a = mid
	return a

def chisquared(model_z, model_DM):
	"""Return the chi-squared statistic for the SNe data, and the no. of data points"""
	# model_z and model_DM are vectors of data output by the model
	
	obs = obsDM() # Load the SNe data from the file
	x2 = 0.0 # The Chi-squared statistic
	count = 0
	
	for j in range(len(obs.DM)):
		#if obs.source[j][:3] != "GRB":
		count += 1
		i = get_closest(obs.z[j], model_z) # Get the index of the closest of the model-output data points
		x2 += (obs.DM[j] - model_DM[i])**2.0 / (obs.errDM[j]**2.0) # Calculate the contribution to the X^2 sum for this data point
	return x2, count
