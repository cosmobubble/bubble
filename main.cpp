using namespace std;
#include "model.h"
#include "models_cfl_a.cpp"
#include "models_cfl_b.cpp"
#include "models_cfl_c.cpp"
#include "models_frw.cpp"
#include "models_gbh.cpp"

int main(int argc, char* argv[]){
	/// Main function
	clock_t tclock = clock();
	
	//if(argc<4){cerr << "ERROR: Not enough command-line arguments." << endl; return 1;}
	//double hh = atof(argv[1]);
	//double AA = atof(argv[2]);
	//double ww = atof(argv[3]);
	
	// Instantiate a model
	/// Interesting one: CFLb mod(0.72, -5.75e-8, 1.0e-9); // (h, A, KR)
	/// Another interesting one, with cusp!: CFLb mod(0.72, 0.5, 1.5e3); // (h, A, w)
	//CFLa mod(0.72, 0.85, 1e3); // (h, A, w)
	//CFLb mod(0.72, 0.5, 1.5e3); // (h, A, w) // Good one!
	//CFLb mod(0.72, 0.7, 1.5e3); // (h, A, w) // Has been the standard one for a while
	CFLb mod(0.72, 0.8, 1.0e3); // (h, A, w)
	////CFLb mod(hh, AA, ww); // (h, A, w)
	//CFLb mod(0.72, 0.1, 1.5e4); // (h, A, w)
	//FRW mod(0.72, 0.0001, 0.9999); // (h, omega_m, omega_k) almost-Milne
	//FRW mod(0.72, 0.3, 0.6); // (h, omega_m, omega_k)
	//GBH mod(0.65, 0.13, 1e3, 0.5e3); // (h, omega_in, r0, deltar) Garcia-Bellido and Haugbolle
	///GBH mod(0.66, 0.13, 2.5e3, 0.64*2.5e3); // (h, omega_in, r0, deltar) Garcia-Bellido and Haugbolle
	//GBH mod(0.66, 0.3, 1.5e3, 0.3*1.5e3); // (h, omega_in, r0, deltar) Garcia-Bellido and Haugbolle
	
	mod.output_background();
	mod.output_geodesic();
	//mod.output_contour();
	mod.output_velocities();
	//mod.output_velocity_mismatch();
	//mod.output_kszvpec_for_z();
	//mod.output_model_stats();
	//mod.output_fnz();
	//mod.output_zin();
	
	// TESTING
	//cerr << "ADAPTIVE v_p = " << mod.adaptive_v_dipole(100.0) << endl;
	//cerr << "NORMAL v_p = " << mod.v_dipole(100.0) << endl;
	
	// TESTING
	/*
	cerr << "IN t(r) = " << mod.adaptive_t_fnr(1.0, 1000.0, mod.fn_t(1.0), GEODESIC_INGOING) << endl;
	cerr << endl;
	cerr << "OUT t(r) = " << mod.adaptive_t_fnr(1.0, 1000.0, mod.fn_t(1.0), GEODESIC_OUTGOING) << endl;
	cerr << endl;
	*/
	
	/*cerr << "r\tAD\tNORM\tr(z)" << endl;
	for(double r=0.0; r<2000.0; r+=50.0){
		cerr << r << "\t" << mod.adaptive_z(0.0, r) << "\t" << mod.z(r) << "\t" << mod.fn_r(mod.fn_tz(mod.z(r))) << endl;
	}*/
	
	/*
	cerr << "r\tz" << endl;
	for(double r=0.0; r<200.0; r+=20.0){
		cerr << r << "\t" << mod.z(r) << endl;
	}*/
	
	
	/*
	cerr << "g/PI\tz" << endl;
	
	double tend = mod.fn_tz(1.0);
	double rstart = 100.0;
	
	/*
	for(double g=0.0; g<=2.*M_PI; g+=2.*M_PI/20.0){
		cerr << g/M_PI << "\t" << mod.general_offcentre(rstart, mod.t_today, g, tend) << endl;
	} // end loop through gamma angles
	
	
	//cerr << 0 << "\t" << mod.general_offcentre(rstart, mod.t_today, 0.0, tend) << endl;
	cerr << 90 << "\t" << mod.general_offcentre(rstart, mod.t_today, M_PI*0.5, tend) << endl;
	//cerr << 180 << "\t" << mod.general_offcentre(rstart, mod.t_today, M_PI, tend) << endl;
	*/
	
	// Finish timing the run
	cerr << ((double)clock() - (double)tclock)/CLOCKS_PER_SEC << " seconds." << endl;
	return 0;
}
