
class GBH: public Model{

public:

	/// Constrained GBH model (Garcia-Bellido and Haugbolle 2008, Looking the void in the eyes)
	GBH(double _h, double _omega_in, double _r0, double _deltar){
		/// Constructor: Constrained GBH model, 4 free parameters
		h = _h;
		H0 = _h*fH0; // Local Hubble rate today
		omega_in = _omega_in; // Underdensity at void centre
		omega_out = 0.999; // Asymptotically-flat space
		r0 = _r0; // Size of the void
		deltar = _deltar; // Transition width of void profile
		
		// Name this model based on the passed parameters
		stringstream modelinfo(stringstream::in | stringstream::out);
		modelinfo << "GBH(cons), h=" << _h << ", Om_in=" << _omega_in << ", r0=" << _r0 << ", dr=" << _deltar << endl;
		output_details(modelinfo.str());
		
		go();
	}
	
	double m(double r){
		/// Density-like function, m(r)
		double H0rr = H0r(r);
		return (3./(8.*M_PI*G))*H0rr*H0rr * omega_m(r);
	}
	
	double mprime(double r){
		/// Radial derivative of density-like function
		double H0rr = H0r(r);
		return 	  (3./(8.*M_PI*G))* H0rr
				* ( 2.*H0prime(r)*omega_m(r) + H0rr*omega_mprime(r) );
	}

	double k(double r){
		/// Curvature-like function, k(r)
		double H0rr = H0r(r);
		return (omega_m(r) - 1.) * H0rr*H0rr/(C*C);
	}

	double kprime(double r){
		/// Radial derivative of curvature-like function, k'(r)
		// FIXME: Units are wrong!?
		double H0rr = H0r(r);
		return (omega_mprime(r)*H0rr*H0rr + 2.*(omega_m(r) - 1.)*H0rr*H0prime(r)) / (C*C);
	}

private:

	double H0, omega_in, omega_out, r0, deltar;
	
	double omega_m(double r){
		/// omega_m(r), defined in eqn. 16
		return omega_out + (omega_in - omega_out)
						 * (1. - tanh( 0.5*(r - r0)/deltar ))
						 / (1. + tanh( 0.5*r0/deltar ));
	}
	
	double omega_k(double r){
		/// omega_k(r) = 1 - omega_m(r)
		return 1. - omega_m(r);
	}
	
	double H0r(double r){
		/// H0(r), defined in eqn. 17
		double om = omega_m(r);
		double ok = 1. - om;
		
		return H0 * (  (1./ok) - om*pow(ok, -1.5)*asinh(sqrt(ok/om)) );
	}
	
	double omega_mprime(double r){
		/// omega_m'(r), the radial derivative of the matter density term
		double tanh_term = tanh( (r - r0)/(2. * deltar) );
		return   ((omega_in - omega_out) / (2. * deltar))
			   * (tanh_term * tanh_term - 1.)
			   / (1. + tanh( 0.5*r0/deltar ));
	}
	
	double H0prime(double r){
		/// The radial derivative of the Hubble rate today
		double om = omega_m(r);
		double ok = 1. - om;
		double omp = omega_mprime(r);
		
		return H0*omp*(
						  (1.5/(ok*ok))
						+ (   asinh(sqrt(ok/om))
							* pow(ok, -1.5)
							* (1.5*(om/ok) - 1.)
						  )
					  );
	} // end H0prime function

};
