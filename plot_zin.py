#!/usr/bin/python

from pylab import *
import sys

# Lists of calculated variables
r = []
zin = []
zout = []

# Get data
if(len(sys.argv)<2):
	f = open("data/zin", 'r')
else:
	f = open("data/zin-" + str(sys.argv[1]), 'r')

for line in f.readlines():
	tmp = line.split("\t")
	if len(tmp) >= 3:
		r.append(float(tmp[0]))
		zin.append(float(tmp[1]))
		zout.append(float(tmp[2]))
f.close()

subplot(211)
plot(r, zin, 'r+', r, zin, 'r-')
plot(r, zout, 'kx', r, zout, 'k-')
grid(True)
title('z_in(r) and z_out(r)')

zdiff = []
for i in range(len(zin)):
	zdiff.append(zin[i]-zout[i])

subplot(212)
plot(r, zdiff, 'r+', r, zdiff, 'r-')
grid(True)
title('z_in - z_out')

show()
