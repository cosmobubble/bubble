#!/bin/bash

# Checks source code for lines reading "FIXME" and so on
grep -n "FIXME" *.cpp *.py *.h
grep -n "TODO" *.cpp *.py *.h
grep -n "TESTING" *.cpp *.py *.h

