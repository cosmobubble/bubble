#!/usr/bin/python

from pylab import *
import sys

files = [1000, 1400, 1500, 1600, 1700, 2000, 2300, 2600, 3000]
cols = ['r-', 'g-', 'b-', 'k-', 'c-', 'm-', 'y-', 'y-', 'y-', 'y-']
#files = [1000, 1500, 1600, 1700, 3000]
#cols = ['r-', 'g-', 'b-', 'k-', 'c-']

lbl = []
for item in files:
	lbl.append(str(item))

# Lists of calculated variables
irr = []
itt = []
iHr = []
iI = []
idHr = []

orr = []
ott = []
oHr = []
oI = []
odHr = []

Hrdiff = []
Idiff = []
dHrdiff = []

# Get data
"""
if(len(sys.argv)<2):
	f = open("data/hr", 'r')
else:
	f = open("data/hr-" + str(sys.argv[1]), 'r')
"""
i = -1
for item in files:
	i+=1
	
	irr.append([])
	itt.append([])
	iHr.append([])
	iI.append([])
	idHr.append([])
	
	orr.append([])
	ott.append([])
	oHr.append([])
	oI.append([])
	odHr.append([])
	
	Hrdiff.append([])
	Idiff.append([])
	dHrdiff.append([])
	
	#f = open("data/hr-" + str(item), 'r')
	f = open("data/hr", 'r')
	
	for line in f.readlines():
		tmp = line.split("\t")
		if len(tmp) >= 5:
			if(tmp[0]=="o"):
				orr[i].append(float(tmp[1]))
				ott[i].append(float(tmp[2]))
				oHr[i].append(float(tmp[3]))
				oI[i].append(float(tmp[4]))
				odHr[i].append(float(tmp[5]))
			else:
				irr[i].append(float(tmp[1]))
				itt[i].append(float(tmp[2]))
				iHr[i].append(float(tmp[3]))
				iI[i].append(float(tmp[4]))
				idHr[i].append(float(tmp[5]))
	f.close()


subplot(311)
for i in range(5):
	for j in range(len(ott[i])):
		Hrdiff[i].append(iHr[i][j]-oHr[i][j])
	plot(ott[i], Hrdiff[i], cols[i], label=lbl[i])
grid(True)
title('Hr(r1(t), t) - Hr(r2(t), t)')
legend()

subplot(312)
for i in range(5):
	for j in range(len(ott[i])):
		Idiff[i].append(iI[i][j]-oI[i][j])
	plot(ott[i], Idiff[i], cols[i])
grid(True)
title('integral(Hr_1) - integral(Hr_2)')

subplot(313)
for i in range(1):
	i=3
	for j in range(len(ott[i])):
		dHrdiff[i].append(idHr[i][j]-odHr[i][j])
	plot(itt[i], iI[i], cols[i])
	plot(ott[i], oI[i], cols[i+1])
grid(True)
title('Hr\'(r1(t), t) - Hr\'(r2(t), t)')
xlabel("t (Myr)")
yscale('log')

show()
