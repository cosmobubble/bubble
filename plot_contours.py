#!/usr/bin/python

from pylab import *

def norm_time(t):
	return t/1000.

def log_density(p):
	return log(p)

# Lists of calculated variables
r = []
t = []

fn = []

gr = []
gt = []

# Get contour data
f = open("data/contour", 'r')
i = -1
for line in f.readlines():
	i+=1
	tmp = line.split("\t")
	
	# If we're on the first row, set this as the r values
	if(i==0):
		r = tmp[1:]
	else:
		if len(tmp) >= 2:
			t.append(float(tmp[0])) # Time coordinate
			fn.append(tmp[1:]) # Array of a1(r,t) values
f.close()

# Get geodesic data
g = open("data/geodesic", 'r')
for line in g.readlines():
	tmp = line.split("\t")
	if len(tmp) >= 2:
		gr.append(float(tmp[0]))
		gt.append(float(tmp[1]))
g.close()

# Get off-centre geodesic data
hro = []
hri = []
hto = []
hti = []
for i in range(100):
	hro.append([])
	hri.append([])
	hto.append([])
	hti.append([])
	
oc = ["offcentre-0", "offcentre-123.075", "offcentre-2584.56"]
i=-1
for fname in oc:
	i += 1
	h = open("data/" + fname, 'r')
	for line in h.readlines():
		tmp = line.split("\t")
		if len(tmp) >= 2:
			if(str(tmp[3])[0]=="o"):
				hro[i].append(float(tmp[0]))
				hto[i].append(float(tmp[1]))
			else:
				hri[i].append(float(tmp[0]))
				hti[i].append(float(tmp[1]))
	h.close()

# Change the units of the 
gt = map(norm_time, gt)

# First plot (physical variables)
subplot(111)
contourf(r, t, fn, 50)

plot(hro[0], map(norm_time, hto[0]), "w-") # Central observer's geodesic


j=2
plot(hro[j], map(norm_time, hto[j]), "k-")
plot(hri[j], map(norm_time, hti[j]), "k--")

#plot(hro[j], map(norm_time, hto[j]), color=str(0.5*float(j)/float(i+1)), linestyle='solid')

###plot(gr, gt, 'r--')
#grid(True)
title('Radial Hubble rate Hr(r(t),t)')
xlabel('Radial coordinate, r (Mpc)')
ylabel('Age of universe (Gyr)')
#yscale('log')
#xlim( (0, 2000) )
#ylim( (35, 45) )

show()
