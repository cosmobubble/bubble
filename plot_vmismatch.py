#!/usr/bin/python

from pylab import *

# Lists of calculated variables
r = []
z = []
vp_frw = []
vp_ksz = []

# Get data
f = open("data/vmismatch", 'r')

for line in f.readlines():
	tmp = line.split("\t")
	if len(tmp) >= 4:
		r.append(float(tmp[0]))
		z.append(float(tmp[1]))
		vp_frw.append(float(tmp[2]))
		vp_ksz.append(float(tmp[3]))
f.close()


subplot(121)
p = plot(vp_frw, vp_ksz, 'r+')
grid(True)
title('delta v_p')

subplot(122)
p = plot(z, vp_frw, 'r-', z, vp_ksz, 'b-')
grid(True)
title('v_p(z), red is FRW')

show()
