#!/usr/bin/python

# Plot the integrands for the off-centre observer radial geodesic calculations

from pylab import *
import sys

#rad = [10, 20, 50, 100, 150, 300, 500, 750, 1000]
#rad = [20, 50, 100, 300, 500, 750, 1000, 2000, 3400]
rad = [20, 500, 1000, 2000]
#sbplt = 331
sbplt = 221

# Lists of calculated variables
ro = []
to = []
go = []
ri1 = []
ti1 = []
gi1 = []
ri2 = []
ti2 = []
gi2 = []


# Get data
i = -1
for s in rad:
	i+=1
	
	ro.append([])
	to.append([])
	go.append([])
	ri1.append([])
	ti1.append([])
	gi1.append([])
	ri2.append([])
	ti2.append([])
	gi2.append([])
	
	f = open("data/offcentre-" + str(s), 'r')
	
	for line in f.readlines():
		tmp = line.split("\t")
		if len(tmp) >= 3:
			if(str(tmp[3][:-1]) == "o"):
				ro[i].append(float(tmp[0]))
				to[i].append(float(tmp[1]))
				go[i].append(float(tmp[2]))
			elif(str(tmp[3][:-1]) == "i1"):
				ri1[i].append(float(tmp[0]))
				ti1[i].append(float(tmp[1]))
				gi1[i].append(float(tmp[2]))
			elif(str(tmp[3][:-1]) == "i2"):
				ri2[i].append(float(tmp[0]))
				ti2[i].append(float(tmp[1]))
				gi2[i].append(float(tmp[2]))
	f.close()
	print "r_observer =", s, "-- Inbound (in):", len(gi1[i]), "-- Inbound (out):", len(gi2[i]), "-- Outbound:", len(go[i])
	
	subplot(sbplt+i)
	plot(to[i], go[i], 'kx', to[i], go[i], 'k-')
	plot(ti1[i], gi1[i], 'b+', ti1[i], gi1[i], 'b-')
	plot(ti2[i], gi2[i], 'r1', ti2[i], gi2[i], 'r-')
	title("r = " + str(rad[i]))
	yscale('log')
	xscale('log')
	xlim((5e2, 1e4))
	ylim((5e-5, 2e-3))
	grid(True)
#xlim((5e0, 1e4)) # For the last plot
#ylim((1e-4, 1e-1))
show()

