#!/usr/bin/python

from pylab import *
import sys

#files = [1000, 1400, 1500, 1600, 1700, 2000, 2300, 2600, 3000]
#cols = ['r-', 'g-', 'b-', 'k-', 'c-', 'm-', 'y-', 'y-', 'y-', 'y-']
files = [1000, 1600, 3000]
cols = ['r-', 'g-', 'b-']

lbl = []
for item in files:
	lbl.append(str(item))

# Lists of calculated variables
irr = []
itt = []
iHr = []
iI = []
idHr = []

orr = []
ott = []
oHr = []
oI = []
odHr = []

Hrdiff = []
Idiff = []
dHrdiff = []

oz = []
iz = []

# Get data
"""
if(len(sys.argv)<2):
	f = open("data/hr", 'r')
else:
	f = open("data/hr-" + str(sys.argv[1]), 'r')
"""
i = -1
for item in files:
	i+=1
	
	irr.append([])
	itt.append([])
	iHr.append([])
	iI.append([])
	idHr.append([])
	
	orr.append([])
	ott.append([])
	oHr.append([])
	oI.append([])
	odHr.append([])
	
	Hrdiff.append([])
	Idiff.append([])
	dHrdiff.append([])
	
	oz.append([])
	iz.append([])
	
	f = open("data/hr-" + str(item), 'r')
	#f = open("data/hr", 'r')
	
	for line in f.readlines():
		tmp = line.split("\t")
		if len(tmp) >= 5:
			if(tmp[0]=="o"):
				orr[i].append(float(tmp[1]))
				ott[i].append(float(tmp[2]))
				oHr[i].append(float(tmp[3]))
				oI[i].append(float(tmp[4]))
				odHr[i].append(float(tmp[5]))
			else:
				irr[i].append(float(tmp[1]))
				itt[i].append(float(tmp[2]))
				iHr[i].append(float(tmp[3]))
				iI[i].append(float(tmp[4]))
				idHr[i].append(float(tmp[5]))
	f.close()


subplot(311)
for i in range(len(files)):
	plot(ott[i], orr[i], cols[i], itt[i], irr[i], cols[i]+"-", label=lbl[i])
grid(True)
ylabel('r(t)')
xlabel("t (Myr)")
legend()

subplot(312)
for i in range(len(files)):
	plot(orr[i], ott[i], cols[i], irr[i], itt[i], cols[i]+"-")
grid(True)
ylabel('t(r)')
xlabel("r (Mpc)")

subplot(313)
for i in range(len(files)):
	for j in range(len(ott[i])):
		oz[i].append( exp(oI[i][j]) - 1.)
	for j in range(len(itt[i])):
		iz[i].append( exp(iI[i][j]) - 1.)
		
	plot(ott[i], oz[i], cols[i], itt[i], iz[i], cols[i]+"-")
grid(True)
ylabel('z(t)')
xlabel('t (Myr)')
#yscale('log')
ylim((0.0, 2.7e0))
xlim((1000, 8000))

show()
