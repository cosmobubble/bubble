
double Model::z(double r){
	/// Get the redshift z(r)
	
	// Get the index of the nearest value of r (need to search t)
	int a = 0; int b = rval.size()-1;
	int mid;
	
	// Check that the r being asked for is sensible
	if(!(rval[a] <= r and rval[b] >= r)){
		cerr << "WARNING: r=" << r << " falls outside the range of the calculated values " << rval[a] << " to " << rval[b] << ". geodesic.cpp::z()" << endl;
		return -0.0;
	}
	
	// Search for the cells bounding the correct time
	while( (b - a) > 1 ){
		mid = a + floor(0.5 * (double)(b - a)); // Find the middle cell
		if(r <= rval[mid]){
			b = mid; // a <= r <= mid
		}else{
			a = mid; // mid < r <= b
		} // end range check
	} // end searching loop
	
	// Check if we're hitting a boundary
	if(a == zval.size() - 1){
		// FIXME: Use the *backwards* gradient to do the interpolation
		// For now, just return the latest value
		return zval[a];
	}else{
		// Do proper forwards linear interpolation
		return zval[a] + (r - rval[a]) * (zval[a+1] - zval[a]) / (rval[a+1] - rval[a]);
	} // end boundary check
	
}



double Model::fn_r(double t){
	/// Find r given t by doing a linear interpolation
	
	int a = 0; int b = tval.size()-1;
	int mid;
	
	// Check that the t being asked for is sensible
	if(!(tval[a] >= t and tval[b] <= t)){
		cerr << "WARNING: t=" << t << " falls outside the range of the calculated values " << tval[a] << " to " << tval[b] << ". geodesic.cpp::fn_r()" << endl;
		return -0.0;
	}
	
	// Search for the cells bounding the correct r
	while( (b - a) > 1 ){
		mid = a + floor(0.5 * (double)(b - a)); // Find the middle cell
		if(t >= tval[mid]){
			b = mid; // a <= t <= mid
		}else{
			a = mid; // mid < t <= b
		} // end range check
	} // end searching loop
	
	// Get the index of the nearest value of t
	//int a = (int)floor((g->t_today - t)/tcell);
	
	// Check if we're hitting a boundary
	if(a == tval.size() - 1){
		// FIXME: Use the *backwards* gradient to do the interpolation
		// For now, just return the latest value
		return rval[a];
	}else{
		// Do proper forwards linear interpolation
		// FIXME: Check that this is interpolating the right way!
		return rval[a] + (tval[a] - t) * (rval[a+1] - rval[a]) / (tval[a+1] - tval[a]);
	} // end boundary check
	
}



double Model::fn_t(double r){
	/// Find t given r by doing a linear interpolation
	
	// Get the index of the nearest value of r (need to search t)
	int a = 0; int b = rval.size()-1;
	int mid;
	
	// Check that the r being asked for is sensible
	if(!(rval[a] <= r and rval[b] >= r)){
		cerr << "WARNING: r=" << r << " falls outside the range of the calculated values " << rval[a] << " to " << rval[b] << ". geodesic.cpp::fn_t()" << endl;
		return -0.0;
	}
	
	// Search for the cells bounding the correct time
	while( (b - a) > 1 ){
		mid = a + floor(0.5 * (double)(b - a)); // Find the middle cell
		if(r <= rval[mid]){
			b = mid; // a <= r <= mid
		}else{
			a = mid; // mid < r <= b
		} // end range check
	} // end searching loop
	
	// Check if we're hitting a boundary
	if(a == rval.size() - 1){
		// FIXME: Use the *backwards* gradient to do the interpolation
		// For now, just return the latest value
		return tval[a];
	}else{
		// Do proper forwards linear interpolation
		return tval[a] + (r - rval[a]) * (tval[a+1] - tval[a]) / (rval[a+1] - rval[a]);
	} // end boundary check
}



double Model::fn_tz(double z){
	/// Find t given z by doing a linear interpolation
	
	int a = 0; int b = zval.size()-1;
	int mid;
	
	// Check that the z being asked for is sensible
	if(!(zval[a] <= z and zval[b] >= z)){
		cerr << "WARNING: z=" << z << " falls outside the range of the calculated values " << zval[a] << " to " << zval[b] << ". geodesic.cpp::fn_tz()" << endl;
		return -0.0;
	}
	
	// Search for the cells bounding the correct r
	while( (b - a) > 1 ){
		mid = a + floor(0.5 * (double)(b - a)); // Find the middle cell
		if(z <= zval[mid]){
			b = mid; // a <= z <= mid
		}else{
			a = mid; // mid < z <= b
		} // end range check
	} // end searching loop
	
	// Check if we're hitting a boundary
	if(a == zval.size() - 1){
		// FIXME: Use the *backwards* gradient to do the interpolation
		// For now, just return the latest value
		return tval[a];
	}else{
		// Do proper forwards linear interpolation
		// FIXME: Check that this is interpolating the right way!
		return tval[a] + (zval[a] - z) * (tval[a+1] - tval[a]) / (zval[a+1] - zval[a]);
	} // end boundary check
	
}
