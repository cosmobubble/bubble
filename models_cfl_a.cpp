
class CFLa: public Model{

public:

	/// Clifton, Ferreira, Land, LTB model (a)
	CFLa(double _h, double _q, double _w){
		/// Constructor: CFL model (b), k~exp(-cr^2)
		/// M0 set to give h0 = h at a1(r=0, today) = 1
		A = -1.0 * _q * pow( (_h*fH0/C), 2.0); // K(r) amplitude scale, where (H0/C)^2 is the maximum allowed
		KR = 1./(_w*_w); // K(r) curvature scale
		h = _h; // Hubble parameter
		M0 = (3. / (8.*M_PI*G)) * ( (fH0*fH0*h*h) + A*C*C ); // Density amplitude scale
		go();
	}
	
	double m(double r){
		/// Density-like function, m(r)
		return M0;
	}
	
	double mprime(double r){
		/// Radial derivative of density-like function
		return 0.0;
	}

	double k(double r){
		/// Curvature-like function, k(r)
		double kk = A * exp(-KR * r*r);
		return kk;
	}

	double kprime(double r){
		/// Radial derivative of curvature-like function, k'(r)
		return -2. * KR * A * r * exp(-KR * r*r);
	}

private:

	double A, KR, M0;

};
