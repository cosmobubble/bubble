
double Model::density(double r, double t){
	/// The physical density at some point in the spacetime
	double _a1 = a1(r, t);
	double _a2 = a2(r, t);
	return ( 1. / (_a1*_a1*_a2) ) * (m(r) + (r*mprime(r)/3.));
}

double Model::Ht(double r, double t){
	/// The transverse Hubble rate at a given r, t, in physical units (km s^-1 Mpc^-1)
	return a1dot(r, t) / (a1(r, t) * fH0);
}

double Model::Hr(double r, double t){
	/// The radial Hubble rate at a given r, t, in physical units (km s^-1 Mpc^-1)
	return a2dot(r, t) / (a2(r, t) * fH0);
}


double Model::DM(double r){
	/// Find deltaDM (distance modulus) between this model and a Milne universe
	/// (Final result needs correcting by setting deltaDM(z=0) = 0)
	/// (In practise, need to use r slightly greater than 0, to avoid zeros)
	return 5. * log( dL(r) / dL_Milne(r) ) / log(10.);
}

double Model::DM_milne(double r){
	/// DM for Milne cosmology
	return 5. * log( dL_Milne(r) ) / log(10.);
}

double Model::dL_Milne(double r){
	/// The luminosity distance in a Milne universe
	/// dL = z(1 + z/2) * c/H0
	double zz = z(r);
	return zz * (1. + 0.5*zz) * C / (fH0 * Ht(0.0, t_today));
	//return zz * (1. + 0.5*zz) * C * g->t_today;
}

double Model::dL(double r){
	/// Find the (LTB) luminosity distance to some comoving radius r
	double zz = z(r);
	return pow(1. + zz, 2.) * r * a1(r, fn_t(r));
}

/////////////// EdS functions ///////////////

double Model::DM_EdS(double r){
	/// Distance modulus for EdS
	return 5. * log( dL_EdS(r) / dL_Milne(r) ) / log(10.);
}

double Model::dL_EdS(double r){
	/// The luminosity distance in an EdS universe
	double zz = z_EdS(r);
	return (1. + zz) * r;
}

double Model::EdS_fn_r(double t){
	/// r(t) in an EdS universe
	double rho = m(0.0); // This will work even if m=m(r), since rho=const in EdS (FIXME: M(x), not M(r)!)
	return -3. * C * t_today * (pow((t/t_today), 1./3.) - 1.);
}

double Model::z_EdS(double r){
	/// Redshift in an Einstein-de Sitter space
	return pow(1. - (r / (3. * C * t_today)), -2.) - 1.;
}

/////////////// End EdS functions ///////////////

double Model::dA(double r){
	/// Find the angular diameter distance to some comoving radius r
	return r * a1(r, fn_t(r));
}

double Model::vp_frw(double r){
	/// Find the FRW-equivalent peculiar velocity, vp = cz - H0.dL, in km/sec
	return -3e5*(z(r) - (H0*dL(r)/C));
}
