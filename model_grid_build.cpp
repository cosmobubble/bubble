
int Model::static_a1dot (double t, const double a[], double adot[], void *params) {
	/// Static wrapper function used for GSL integration of the LTB 
	/// Friedmann-like equation
	ModelPointers p = *(ModelPointers *)params;
	adot[0] = p.mod->s_a1dot(a[0], p.m, p.k);
	return GSL_SUCCESS;
}

int Model::a1dot_jac(double t, const double a[], double *dfda, double dfdt[], void *params){
	/// Static wrapper function used to define the LTB Friedmann-like 
	/// equation's Jacobian for GSL integration 
	ModelPointers p = *(ModelPointers *)params;
	gsl_matrix_view dfda_mat = gsl_matrix_view_array (dfda, 1, 1);
	gsl_matrix * m = &dfda_mat.matrix; 
	gsl_matrix_set (m, 0, 0, p.mod->da1dot_da(a[0], p.m, p.k) );
	dfdt[0] = 0.0;
	dfdt[1] = 0.0;
	return GSL_SUCCESS;
}

double Model::s_a1dot(double a1, double m, double k){
	return sqrt( (m / a1) - k );
}

double Model::da1dot_da(double a, double m, double k){
	-0.5 * m / ( a*a*sqrt( m/a - k ) );
}


int Model::get_max_j(double x){
	/// Get the j value which will ensure that the grid will be 
	/// populated at least up to the maximum global time for a given x
	int mid;
	int a = 0;
	int b = g_y.size()-1;
	
	// Target y
	double y = y_today - xTb(x);
	
	// Get i<=>y by searching g_y
	while( (b - a) > 1 ){
		mid = a + floor(0.5 * (double)(b - a)); // Find the middle cell
		if(y <= g_y[mid]){
			b = mid; // a <= y <= mid
		}else{
			a = mid; // mid < y <= b
		} // end range check
	} // end y-searching loop
	return a; // Index of the maximum y value to calculate for this x
}

void Model::populate_timesample_vector(){
	/// Populate the time sampling vector, which gives the local times 
	/// on the grid. Can use this to do variable time sampling
	
	// FIXME: Not Bang-time-safe?
	
	double yfinestep = 10.0/(ts*100.0); // Take 100 samples between 0 and t=50Myr
	double ystep = (y_today - (10.0/ts))/((double)(YCELLS-100));
	
	/*
	// Make YCELLS no. of samples
	for(int i=0; i<YCELLS; i++){
		g_y.push_back(  y0 + (double)i*ystep  );
	} // end loop through cells
	*/
	
	int i; double y;
	for(i=0; i<YCELLS; i++){
		if(y<10.0/ts){
			y+=yfinestep;
		}else{
			y+=ystep;
		}
		g_y.push_back( y );
	}
}

void Model::populate_grid(){
	/// Populate a mesh with values for a1 using an RK4 ODE solution
	/// to the LTB Friedmann equation
	clock_t tclock = clock();
	double x, y; int jmax;
	bool reached_asymptotic = false;
	
	// Set-up arrays etc.
	populate_timesample_vector();
	
	// Initialise the GSL adaptive integrator
	//const gsl_odeiv_step_type * T = gsl_odeiv_step_rk8pd; // RK8 Prince-Dormand adaptive algorithm
	const gsl_odeiv_step_type * T = gsl_odeiv_step_rkf45; // Runge-Kutta-Fehlberg (4,5) adaptive algorithm
	gsl_odeiv_step * s = gsl_odeiv_step_alloc (T, 1); // Allocate a 1D ODE array
	gsl_odeiv_control * c = gsl_odeiv_control_y_new (1e-12, 0.0); // Set the relative/absolute error tolerances
	gsl_odeiv_evolve * e = gsl_odeiv_evolve_alloc (1);
	int status; // Variable to hold status of GSL integration
	
	// Make sure we start at the right place
	double hh; double a1[1] = { A1_REC };
	x = x0 - XSTEP;
	ModelPointers p; p.mod = this;
	
	// Loop through x values
	for(int i=0; i<XCELLS+2 and !reached_asymptotic; i++){
		
		// Set-up for integration
		x += XSTEP; g_x.push_back(x); // Add the x value for this cell to the array
		p.m = xM(x); p.k = xK(x);
		a1[0] = A1_REC;
		
		// Calculate the maximum j value to go up to
		jmax = get_max_j(x);
		
		// Create new temporary time slice vectors
		// Add an extra 2 elements to each slice to avoid boundary problems when interpolating
		vector<double> tmp_a1(jmax+2, 0.0);
		vector<double> tmp_a1dot(jmax+2, 0.0);
		
		// Define system of ODEs (ODE + Jacobian)
		gsl_odeiv_system sys = {static_a1dot, a1dot_jac, 1, &p};
		
		// Loop through sample points
		y = y0; hh = 1e-16; // Make sure we reset hh
		for(int j=0; j<jmax+2; j++){
			
			// Integrate until the next sample point
			while (y < g_y[j]){
				status = gsl_odeiv_evolve_apply (e, c, s, &sys, &y, g_y[j], &hh, a1);
				if (status != GSL_SUCCESS){ cerr << "Model::populate_grid(): Integration failed." << endl; break; }
			} // end while loop
			if(isnan(a1[0])){throw 111;}
			
			// Add to mesh at this sample point
			tmp_a1[j] = a1[0];
			tmp_a1dot[j] = s_a1dot(a1[0], p.m, p.k)/ts;
		} // end time steps
		
		g_a1.push_back( tmp_a1 );
		g_a1dot.push_back( tmp_a1dot );
		
		// Check if we're in the asymptotic region (nest for efficiency?)
		// Here, a1(r) = a1(r+c), for all c > 0, so no point calculating the rest
		if(x>XMIN){
			if(g_a1[i][jmax] == g_a1[i-10][jmax]){
				reached_asymptotic = true;
				XMAX = x; XCELLS = XMAX / XSTEP;
				cerr << "\tReached asymptotic region at r=" << x*rs << endl;
			}
		} // end check x region
		
	} // end loop through x values
	
	// Get the a2 values now the dust has settled
	calculate_a2grid();
	
	// Free all of the integration variables
	gsl_odeiv_evolve_free (e);
	gsl_odeiv_control_free (c);
	gsl_odeiv_step_free (s);
	
	cerr << "\tpopulate_grid(): " << ((double)clock() - (double)tclock)/CLOCKS_PER_SEC << " seconds." << endl;
} // end initial integration function



void Model::calculate_a2grid(){
	/// Calculate the a2 values on the grid now that we have a1, a1dot values
	double x, y, r, tl, tg; int jmax;
	double drm, invdr;
	int xsize = g_x.size();
	
	// Loop through x values
	for(int i=0; i<XCELLS+2; i++){
		
		x = g_x[i]; r = x*rs;
		jmax = get_max_j(x); // Calculate the maximum j value to go up to
		
		if(i>0 and i<xsize-1){
			drm = rs * (x - g_x[i-1]);
			invdr = 1./drm;
		}
		// FIXME: Should this be 1/dr or 1/2dr? 1/dr gives *a* right-looking answer! But 2dr should be correct
		// Seems that shell-crossings more likely with 1/dr
		
		// Create new temporary time slice vectors
		// Add an extra 2 elements to each slice to avoid boundary problems when interpolating
		vector<double> tmp_a2(jmax+2, 0.0);
		vector<double> tmp_a2dot(jmax+2, 0.0);
		
		for(int j=0; j<jmax+2; j++){
			y = g_y[j];
			tl = y*ts; // Local time
			tg = tl + ts*xTb(x); // Global time
			if(i>0 and i<xsize-1){
				tmp_a2[j] = a1(r, tl, TIME_LOCAL) + r*invdr
						  * (a1(r, tl, TIME_LOCAL) - a1(r-drm, tg, TIME_GLOBAL));
				tmp_a2dot[j] = a1dot(r, tl, TIME_LOCAL) + r*invdr
						  * (a1dot(r, tl, TIME_LOCAL) - a1dot(r-drm, tg, TIME_GLOBAL));
				
				if(tmp_a2[j] <= 0.0){cerr << "WARNING: Shell crossing occurred at r=" << x*rs << ", y/y_today=" << y/y_today << " (NumericGrid::populate_grid)" << endl; throw 99;} // end check
			}else{
				// a2, a2dot = a1, a1dot at r=0
				tmp_a2[j] = a1(r, tl, TIME_LOCAL);
				tmp_a2dot[j] = a1dot(r, tl, TIME_LOCAL);
			}
		} // end j loop y values
		
		g_a2.push_back( tmp_a2 );
		g_a2dot.push_back( tmp_a2dot );
		
	} // end i loop through x values
	
}
