#!/usr/bin/python

from pylab import *
import analysis_supernovae as asn
import analysis_ksz as aksz

# Lists of calculated variables
r = [] # Comoving radial distance
density = [] # Physical density
# Scale factors
a1 = []
a1dot = []
a2 = []
a2dot = []
# Hubble rates
Ht = []
Hr = []
k = []
pk = []
pm = []

gr = []
gt = []
gz = []
gdm = []
gzeds = []
gdmeds = []
gdl = []
gdleds = []
gdmmilne = []

rr = []
zz = []
dz = []
rend = []

# Get data
f = open("data/background", 'r')

for line in f.readlines():
	tmp = line.split("\t")
	if len(tmp) >= 8:
		r.append(float(tmp[0]))
		density.append(float(tmp[1]))
		a1.append(float(tmp[2]))
		a1dot.append(float(tmp[3]))
		a2.append(float(tmp[4]))
		a2dot.append(float(tmp[5]))
		Ht.append(float(tmp[6]))
		Hr.append(float(tmp[7]))
		k.append(float(tmp[8]))
		pk.append(float(tmp[9]))
		pm.append(float(tmp[10]))
f.close()

# Get data
g = open("data/geodesic", 'r')

for line in g.readlines():
	tmp = line.split("\t")
	if len(tmp) >= 8:
		gr.append(float(tmp[0]))
		gt.append(float(tmp[1]))
		gz.append(float(tmp[2]))
		gdm.append(float(tmp[3]))
		gzeds.append(float(tmp[4]))
		gdmeds.append(float(tmp[5]))
		gdl.append(float(tmp[6]))
		gdleds.append(float(tmp[7]))
		gdmmilne.append(float(tmp[8]))
g.close()

# Get data
h = open("data/velocity", 'r')

for line in h.readlines():
	tmp = line.split("\t")
	if len(tmp) >= 3:
		rr.append(float(tmp[0]))
		zz.append(float(tmp[1]))
		dz.append(float(tmp[2]))
		rend.append(float(tmp[5]))
h.close()

h = open("data/modelinfo", 'r')
modname = h.readlines()[0][:-1]
print "\n", modname, "\n"

# First plot (physical variables)
subplot(231)
p = plot(r, density, 'k-')
grid(True)
title('Density')

# Second plot
subplot(232)
plot(r, pk, 'b-', r, pm, 'r-')
grid(True)
title('omega_k/m(r)')

# Third plot (Raw expansion rates)
subplot(233)
plot(r, a1, 'b-', r, a1dot, 'b--', r, a2, 'r-', r, a2dot, 'r:')
grid(True)
title('Expansion Rates')
#yscale('log')

subplot(234)
plot(r, Ht, 'b-', r, Hr, 'r-')
grid(True)
title('Hubble Rates')

"""subplot(234)
plot(gz, gr, 'b-')
grid(True)
title('r(z)')"""

# Fourth plot (Geodesics as function of r)
#subplot(234)
#plot(gz, gr, 'b-')
#grid(True)
#title('Geodesic r(z)')
#xlim((0.0, 1.0))

# vp(z)
# Use the legend to display the Chi-squared of the fit (FIXME: Makes bad assumptions)
ksz = aksz.obskSZ()
x2, num = aksz.chisquared(zz, dz)
lbl = "$\chi^2$=" + str(round(x2/num, 2))
subplot(235)
p = plot(zz, dz, 'r+', zz, dz, 'r-')
p = plot(zz, rend, 'b-')
errorbar(ksz.z, ksz.vp, yerr=ksz.e_lower, fmt="k.", label=lbl)
grid(True)
title('delta_vp(z)[r], r_end[b]')
legend()
#xlim((0.0, 6000.0))


# Distance modulus
# Use the legend to display the Chi-squared of the fit
sn = asn.obsDM()
x2, num = asn.chisquared(gz, gdm)
lbl = "$\chi^2$=" + str(round(x2/num, 2))

subplot(236)
errorbar(sn.z, sn.DM, yerr=sn.errDM, fmt="r.")
plot(gz, gdm, 'k-', label=lbl)
grid(True)
title('deltaDM(z)')
xlabel('z')
legend()

show()
