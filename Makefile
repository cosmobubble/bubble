bubble: clean main.o
	g++ -Llibgsl/lib -o bubble main.o -lgsl -lgslcblas -lm

main.o:
	g++ -Ilibgsl/include -c -O3 main.cpp

clean:
	rm -f bubble main.o

tf: clean_tf
	g++ -g -o twofluid clarkson_regis.cpp

clean_tf:
	rm -f twofluid

debug: clean
	g++ -Ilibgsl/include -g -c main.cpp
	g++ -Llibgsl/lib -o bubble main.o -lgsl -lgslcblas -lm
