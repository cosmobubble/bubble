
void Model::output_details(string model_title){
	/// Output some basic ID information about the model
	stringstream fname(stringstream::in | stringstream::out);
	fname << "data/modelinfo" << filename_ending;
	ofstream info; info.open(fname.str().c_str());
	
	info << model_title << endl; // Just output the title for the moment
	info.close();
}

void Model::output_background(double t){
	/// Output background variables at arbitrary time (file:data/background)
	stringstream fname(stringstream::in | stringstream::out);
	fname << "data/background" << filename_ending;
	ofstream bk; bk.open(fname.str().c_str());
	
	double loc_dens;
	for(double r=r0; r<20000.; r += 2.){
		loc_dens = (((8.*M_PI*G/3.) * m(r) / a1(r, t)) - C*C*k(r));
		bk  << r << "\t"
			<< density(r, t) << "\t"
			<< a1(r, t) << "\t"
			<< a1dot(r, t) << "\t"
			<< a2(r, t) << "\t"
			<< a2dot(r, t) << "\t"
			<< Ht(r, t) << "\t"
			<< Hr(r, t) << "\t"
			<< k(r) << "\t"
			<< -C*C*k(r)/loc_dens << "\t"
			<< (8.*M_PI*G/3.) * m(r) / (a1(r, t) * loc_dens) << "\t"
			<< endl;
	}
	bk.close();
}

void Model::output_background(){
	/// Output background variables *today* (file:data/background)
	output_background(t_today);
}

void Model::output_geodesic(){
	/// Output data for graphing geodesics (file:data/geodesic)
	stringstream fname(stringstream::in | stringstream::out);
	fname << "data/geodesic" << filename_ending;
	ofstream gs; gs.open(fname.str().c_str());
	double rmax = fn_r(fn_tz(10.0));
	
	for(double r=0.0; r < rmax /*r < rval[rval.size()-1]*/; r+=10.0/*2.0*/){
		gs  << r << "\t"
			<< fn_t(r) << "\t"
			<< z(r) << "\t" 
			<< DM(r) << "\t"
			<< z_EdS(r) << "\t"
			<< DM_EdS(r) << "\t"
			<< dL(r) << "\t"
			<< dL_EdS(r) << "\t"
			<< DM_milne(r) << "\t"
			<< endl;
	}
	gs.close();
}

void Model::output_model_stats(){
	/// Print information about the model to the terminal
	cerr << "------------------------------" << endl
		 << "t_today = " << t_today << " (" << t_today / GYR << " Gyr)" << endl
		 << "a1(r=0, t=now) = " << a1(0.0, t_today) << endl
		 << "a2(r=0, t=now) = " << a2(0.0, t_today) << endl
		 << "Ht(now) = " << Ht(0.0, t_today) << endl
		 << "Hr(now) = " << Hr(0.0, t_today) << endl
		 << "XCELLS = " << XCELLS << endl
		 << "YCELLS = " << YCELLS << endl
		 << "x_max = " << XMAX << endl
		 << "Central geodesic endpoint: r=" << rval[rval.size()-1]
			<< ", t=" << tval[rval.size()-1]
			<< ", z=" << zval[rval.size()-1] << endl
		 << "------------------------------" << endl;
}

void Model::output_contour(){
	/// Ouput data for 2D contour plots (file:data/contour)
	stringstream fname(stringstream::in | stringstream::out);
	fname << "data/contour" << filename_ending;
	output_contour_handler(&Model::Hr, fname.str());
}

void Model::output_contour_handler(double (Model::*fn)(double, double), string fname){
	/// Handler which does the actual outputting of the data.
	double step_t = (t_today - t0) / 200.0;
	ofstream gs; gs.open( fname.c_str() );
	
	// Header, which defines the r values
	gs << "t";
	for(double r=0.0; r < rval[rval.size()-1]; r+=10.0){ gs << "\t" << r; }
	gs << endl;
	
	// Output the values. First column is the time coordinate
	for(double t=t_today; t > t0; t -= step_t){
		gs  << t/GYR;
		for(double r=0.0; r < rval[rval.size()-1]; r+=10.0){  gs << "\t" << log((*this.*fn)(r, t));  }
		gs << endl;
	} // end t loop
	
	gs.close();
}


void Model::output_velocities(){
	/// Output the velocities calculated for off-centre observers
	cerr << "\toutput_velocities()" << endl;
	clock_t tclock = clock();
	
	stringstream fname(stringstream::in | stringstream::out);
	fname << "data/velocity" << filename_ending;
	ofstream vel; vel.open(fname.str().c_str());
	
	// Get the r when z~5
	//double rmax = fn_r(fn_tz(4.0));
	double rmax = fn_r(fn_tz(10.0));
	double r_end;
	
	// Output things until then //rmax/50.
	for(double r=0.0; r<rmax; r+=(rmax/75.)){
		double tt = fn_t(r);
		vel << r << "\t"
			 << z(r) << "\t"
			 << v_dipole(r, &r_end) << "\t"
			 << (Hr(r+20.0, tt) - Hr(r, tt))/20.0 << "\t"
			 << vp_frw(r) << "\t"
			 << r_end
			 << endl;
	}
	vel.close();
	cerr << "\toutput_velocities(): " << ((double)clock() - (double)tclock)/CLOCKS_PER_SEC << " seconds." << endl;
}


void Model::output_velocity_mismatch(){
	/// Output the mismatch between FRW expected recession velocity and 
	/// observed CMB dipole velocity, vp|FRW = cz - H0*dL vs. vp|kSZ
	cerr << "\toutput_velocity_mismatch()" << endl;
	clock_t tclock = clock();
	
	stringstream fname(stringstream::in | stringstream::out);
	fname << "data/vmismatch" << filename_ending;
	ofstream vel; vel.open(fname.str().c_str());
	
	// Get the r when z~0.6
	double rmax = fn_r(fn_tz(0.6));
	
	// Output things until then
	for(double r=0.0; r<rmax; r+=rmax/15.){
		double zz = z(r);
		vel << r << "\t"
			 << zz << "\t"
			 << C*zz - (fH0*h*dL(r)) << "\t"
			 << -1.*v_dipole(r)
			 << endl;
	}
	vel.close();
	cerr << "\toutput_velocity_mismatch(): " << ((double)clock() - (double)tclock)/CLOCKS_PER_SEC << " seconds." << endl;
}


void Model::output_fnz(){
	/// Output background variables as function of redhsift (file:data/fnz)
	stringstream fname(stringstream::in | stringstream::out);
	fname << "data/fnz" << filename_ending;
	ofstream bk; bk.open(fname.str().c_str());
	
	double loc_dens, zz, tt;
	for(double r=r0; r<3400.; r += 20.){
		zz = z(r);
		tt = fn_t(r);
		loc_dens = (((8.*M_PI*G/3.) * m(r) / a1(r, tt)) - C*C*k(r));
		bk  << r << "\t"
			<< density(r, tt) << "\t"
			<< a1(r, tt) << "\t"
			<< a1dot(r, tt) << "\t"
			<< a2(r, tt) << "\t"
			<< a2dot(r, tt) << "\t"
			<< Ht(r, tt) << "\t"
			<< Hr(r, tt) << "\t"
			<< k(r) << "\t"
			<< -C*C*k(r)/loc_dens << "\t"
			<< (8.*M_PI*G/3.) * m(r) / (a1(r, tt) * loc_dens) << "\t"
			<< zz << "\t"
			<< (Hr(r+20.0, tt) - Hr(r, tt))/20.0 << "\t"
			<< (Ht(r+20.0, tt) - Ht(r, tt))/20.0
			<< endl;
	}
	bk.close();
}


/*
void Model::output_zin(){
	/// Output z_in calculated for off-centre observers
	cerr << "\toutput_zin()" << endl;
	clock_t tclock = clock();
	
	stringstream fname(stringstream::in | stringstream::out);
	fname << "data/zin" << filename_ending;
	
	ofstream vel; vel.open(fname.str().c_str());
	// Get the r when z~5
	double rmax = fn_r(fn_tz(4.0));
	double t_dec = fn_tz(40.0);
	
	// Output things until then
	for(double r=0.0; r<rmax; r+=(rmax/50.)){
		double t_obs = fn_t(r);
		vel << r << "\t"
			 << ingoing_redshift(r, t_obs, t_dec) << "\t"
			 << outgoing_redshift(r, t_obs, t_dec)
			 << endl;
	}
	vel.close();
	cerr << "\toutput_velocities(): " << ((double)clock() - (double)tclock)/CLOCKS_PER_SEC << " seconds." << endl;
}*/

void Model::output_kszvpec_for_z(){
	/// Output the expected kSZ velocity given a set of z values
	vector<double> zdata;
	
	// Read-in z data
	ifstream dfile("zdata.dat");
	string tmp; char *pend;
	if (dfile.is_open()){
		while(!dfile.eof()){
			getline (dfile, tmp);
			if(strtod(tmp.c_str(), &pend) > 0.0){
				cerr << "z=" << strtod(tmp.c_str(), &pend) << endl;
				zdata.push_back(strtod(tmp.c_str(), &pend));
			} // end validity check
		} // end loop through file lines
    dfile.close();
	}else{cerr << "ERROR: Model::output_kszvpec_for_z: Could not open file." << endl;}
	
	// Output vp|kSZ values
	ofstream vel; vel.open("data/kszvelocities");
	
	// Output things until then
	for(int i=0; i<zdata.size(); i++){
		double r = fn_r(fn_tz(zdata[i]));
		vel << r << "\t"
			<< zdata[i] << "\t"
			<< v_dipole(r) << "\t"
			<< endl;
	}
	vel.close();
	
}
