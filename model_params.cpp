
void Model::prepare_grid(){
	/// Create new grids
	
	YCELLS = 400; // FIXME: Normally 300
	
	H0 = fH0 * h;
	y0 = T_REC/ts; // Initial time
	x0 = 0.0; // Radial coordinate to start at (x=0 is the void centre)
	a1_0 = A1_REC; // Initial a1 at x=0
	r0 = 0.0;
	t0 = y0 * ts;
	
	XMIN = 20; // The minimum x that should be reached before we start considering whether it has reached asymp. value
	XSTEP = 0.01; // FIXME: NORMALLY 0.01 // The step to take in x when populating the grid
	XMAX = 200;
	XCELLS = XMAX / XSTEP; // Maximum number of x cells to calculate
	
	// Get the time when the Hubble constant is reached locally (x=0, y=now)
	hubble_time();
	YSTEP = (y_today - y0) / ((double)YCELLS);
	dy = YSTEP / 200.; // Time step for integration: Has a big effect on how long the calculation takes!
	tcell = 1.0;
	dt = -1e-1;
}

void Model::hubble_time(){
	/// Calculate the age of the universe today (i.e. when a1dot/a1 = H0(obs))
	double k1, k2, k3, k4; // Runge-Kutta variables
	double m = xM(x0); double k = xK(x0);
	double y; double dy2 = -1e-4;
	double a1 = A_MAX;
	// Integrate until a1(x=0) = 1e-3 (~ decoupling) and set that time to 0.4 Myr
	// Can then calculate t_today
	
	for(y=0.0; a1>A1_REC; y+=dy2){
		
		k1 = s_a1dot(a1, m, k);
		k2 = s_a1dot(a1 + 0.5*k1*dy2, m, k);
		k3 = s_a1dot(a1 + 0.5*k2*dy2, m, k);
		k4 = s_a1dot(a1 + k3*dy2, m, k);
		
		// Calculate new a1
		a1 += (dy2/6.) * (k1 + 2.*k2 + 2.*k3 + k4);
		
	} // end time integration
	
	double ysub = y - T_REC/ts;
	y0 = y - ysub;
	y_today = 0.0 - ysub;
	
	t0 = y0 * ts;
	t_today = y_today * ts;
}
