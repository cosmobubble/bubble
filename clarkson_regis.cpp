#include <cmath>
#include <cstdlib>
#include <vector>
#include <iostream>

using namespace std;


// Units: Mass (Msun); Time (Myr); Distance (Mpc)
const double G = 4.4986e-21; // Gravitational constant G, in units of Mpc^3 Msun^-1 Myr^-2
const double C = 0.30659458; // Speed of light, in Mpc/Myr
const double GYR = 1000.; // One Gyr in units of time (Myr)
const double fH0 = 1.02269e-4; // H0 = 100h km s^-1 Mpc^-1 = fH0*h Myr^-1
double pig = 4.*M_PI*G/3.; // 4piG/3



// Initial radial profiles
double h = 0.72;
double om = 0.3; // Matter density at r=0 today
double ou = 1e-4; // Radiation density at r=0 today
double ok = 0.5; // Curvature density at r=0 today

double q = 0.5;
double w = 1e3;
double A = -1.0 * q * pow( (h*fH0/C), 2.0);
double PCR = 3.*pow(fH0*h, 2.0)/(8.*M_PI*G); // FRW critical density

// FIXME: Need updating given the new r=0 conditions
double a0(double r){ return 1e-4; }
double k0(double r){ return A * exp( -1. * pow(r/w, 3.0) ); }
double m0(double r){ return (3. / (8.*M_PI*G)) * ( (fH0*fH0*h*h) + A*C*C ); }
double p0(double r){ return 1.3e23; } // ~rho_cr?
double s0(double r){ return 1e-4; }
double u0(double r){ return 1.3e22; }
double v0(double r){ return 0.05*(r/w); }

int main(){
// Clarkson-Regis two-fluid PDE numerical solutions

// s = s(t) = a_perp
// a = a(t) = a_//
// u = mu(r, t)
// p = rho(r, t)
// Notation: ss = s(t+1) etc.

// Grid boundaries
double t0 = 0.4;
double t_today = 1./(fH0*h);
double r0 = 0.0;

// Integration variables
double r;
double dt = 0.1; // 0.1 Myr
double dr = 0.1; // 0.1 Mpc
double tlast = t0;
double TCELL = 10.;
int RCELLS = 10;

// Calculated grid variables
vector<vector<double> > g_a, g_k, g_m, g_p, g_s, g_u, g_v;

// Temporary grid variables
vector<double> 	a(RCELLS, 0.0), k(RCELLS, 0.0), m(RCELLS, 0.0), 
				p(RCELLS, 0.0), s(RCELLS, 0.0), u(RCELLS, 0.0),
				v(RCELLS, 0.0);
vector<double> 	aa(RCELLS, 0.0), kk(RCELLS, 0.0), mm(RCELLS, 0.0),
				pp(RCELLS, 0.0), ss(RCELLS, 0.0), uu(RCELLS, 0.0),
				vv(RCELLS, 0.0);
double sss;
double s_prime, u_prime, mm_prime;



// Set everything to its initial value before we begin
for(int i=0; i<RCELLS; i++){
	double rr = i*dr;
	aa[i] = a0(rr);	kk[i] = k0(rr);	mm[i] = m0(rr);
	pp[i] = p0(rr);	ss[i] = s0(rr);	uu[i] = u0(rr);
	vv[i] = v0(rr);
}

// Set initial conditions at r=0
// FIXME: Need to do this properly
a[0] = 1e-4; s[0] = 1e-4;
k[0] = -1.*ok*(fH0*fH0*h*h)/(s[0]*s[0]*C*C);
p[0] = PCR*om/(s[0]*s[0]*s[0]);
u[0] = PCR*ou/(s[0]*s[0]*s[0]*s[0]);
m[0] = 0.0;
v[0] = 0.0;

// Push the initial values onto the grid
g_a.push_back(aa); g_k.push_back(kk); g_m.push_back(mm);
g_p.push_back(pp); g_s.push_back(ss); g_u.push_back(uu);
g_v.push_back(vv);

// Do the integration to populate the grid
for(double t=t0; t<t_today; t+=dt){
	
	// FIXME: Do a check that all values are valid here (for testing purposes)
	
	///////////////////////////////////////
	// Do a special FRW integration at r=0
	aa[0] = a[0] + dt*h*fH0*sqrt(  (om/a[0]) + (ou/(a[0]*a[0])) + ok  );
	ss[0] = aa[0];
	pp[0] = PCR*om/(ss[0]*ss[0]*ss[0]);
	uu[0] = PCR*ou/(ss[0]*ss[0]*ss[0]*ss[0]);
	kk[0] = -1.*ok*(fH0*fH0*h*h)/(ss[0]*ss[0]*C*C);
	mm[0] = 0.0; // FIXME: Dodgy assumption?
	vv[0] = 0.0;
	
	
	///////////////////////////////////////
	// Loop through the rest of the r values
	r = r0;
	for(int i=1; i<RCELLS; i++){
		r += dr;
		
		// Update variables
		a[i] = aa[i]; k[i] = kk[i]; m[i] = mm[i];
		p[i] = pp[i]; s[i] = ss[i]; u[i] = uu[i]; v[i] = vv[i];
		
		// Update u, s derivatives
		// Can calculate the derivative since r>0 here
		u_prime = (u[i] - u[i-1])/dr;
		s_prime = (s[i] - s[i-1])/dr;

		// First layer (depend only on previous integration)
		ss[i] = s[i] + dt * sqrt( (2.*pig*m[i] / (s[i]*r*r*r)) - C*C*k[i] ); // Eqn. 1
		kk[i] = k[i] - (dt*4.*pig*a[i]*s[i]*u[i]*v[i]/(3.*r*C*C)); // Eqn. 3
		
		cerr << "\tkk[i] (a) = " << k[i] << endl
			 << "\tkk[i] (b) = " << dt*4.*a[i]*s[i]*u[i]*v[i] << endl
			 << "\tkk[i] (dt) = " << dt << endl
			 << "\tkk[i] (a[i]) = " << a[i] << endl
			 << "\tkk[i] (s[i]) = " << s[i] << endl
			 << "\tkk[i] (u[i]) = " << u[i] << endl
			 << "\tkk[i] (v[i]) = " << v[i] << endl;

		// Second layer (depend on first layer)
		mm[i] = m[i] - (u[i]*s[i]*s[i]*r*r/6.) * ( dt*4.*a[i]*v[i] + r*(ss[i] - s[i]) ); // Eqn. 2
		aa[i] = s[i] + r*s_prime; // Eqn. 7
		
		// Update mm derivative
		// Can calculate the derivative since r>0 here
		mm_prime = (mm[i] - mm[i-1])/dr;

		// Third layer
		vv[i] = v[i] + ( 
						(v[i]/3.) * (  (2.*(ss[i]-s[i])/s[i]) - (5.*(aa[i]-a[i])/a[i]) )
					-	( dt*(1. - k[i]*r*r)*u_prime*C*C / (4.*a[i]*a[i]*u[i]) )
				 );
		pp[i] = p[i] * ( 1.
					+ 	(dt*2.*pig*a[i]*s[i]*r*u[i]*v[i]/(3.*C*C*(1. - k[i]*r*r)))
					- 	((aa[i]-a[i])/a[i])
					-	(2.*(ss[i]-s[i])/s[i])
				 );

		// Last layer
		sss = ss[i] + dt * sqrt( (2.*pig*mm[i]/(ss[i]*r*r*r)) - kk[i]*C*C );
		
		// Avoid singularity
		uu[i] = 	(aa[i]*ss[i]*ss[i]*r*r*pp[i] - 2.*mm_prime)
				* 	( (4.*r*aa[i]*vv[i]*(sss-ss[i])/dt)/(C*C) - 3.*(1. - kk[i]*r*r))
				/ 	(3.*aa[i]*ss[i]*ss[i]*r*r*(1. - kk[i]*r*r));
		
		cout << "i=" << i << ", t=" << t << endl
			 << "aa=" << aa[i] << endl
			 << "kk=" << kk[i] << endl
			 << "mm=" << mm[i] << endl
			 << "pp=" << pp[i] << endl
			 << "ss=" << ss[i] << endl
			 << "uu=" << uu[i] << endl
			 << "vv=" << vv[i] << endl
			 << "uprime=" << u_prime << endl
			 << "sprime=" << s_prime << endl
			 << "mmprime=" << mm_prime << endl
			 << "r=" << r << endl << endl;
		if(isnan(uu[i])){
			cerr << "Term1 = " << (aa[i]*ss[i]*ss[i]*r*r*pp[i] - 2.*mm_prime) << endl
				 << "Term2 = " << ( (4.*r*aa[i]*vv[i]*(sss-ss[i])/dt) - 3.*(1. - kk[i]*r*r)) << endl
				 << "Term3 = " << (3.*aa[i]*ss[i]*ss[i]*r*r*(1. - kk[i]*r*r)) << endl
				 << "sss = " << ss[i] << " " << 2.*pig*mm[i]/(ss[i]*r*r*r) << " " << -kk[i]*C*C << endl
				 << "sss_inrt = " << (2.*mm[i]/(ss[i]*r*r*r)) - kk[i] << endl
				 << "\tmm[i] = " << mm[i] << ", ss[i] = " << ss[i] << ", kk[i] = " << kk[i] << endl;
			throw 10;
		}
		
	} // end r/i loop
	
	// If we've reached a sample point, add the most recently-calculated values to the grid
	if(t - tlast >= TCELL){
		g_a.push_back(aa); g_k.push_back(kk); g_m.push_back(mm);
		g_p.push_back(pp); g_s.push_back(ss); g_u.push_back(uu);
		g_v.push_back(vv);
		
		tlast = t;
	}
	
} // end t loop


cerr << g_u.size() << endl;
for(int j=0; j<g_u.size()-1; j++){
	cerr << g_u[j][4] << endl;
}


return 0;
}
