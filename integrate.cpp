



//////////////////////
/*
void Model::populate_grid(){
	/// Alternative populate_grid function which uses GSL's adaptive integrator
	clock_t tclock = clock();
	
	// Initialise the GSL adaptive integrator
	const gsl_odeiv_step_type * T = gsl_odeiv_step_rk8pd; // RK8 Prince-Dormand adaptive algorithm
	//const gsl_odeiv_step_type * T = gsl_odeiv_step_rkf45; // Runge-Kutta-Fehlberg (4,5) adaptive algorithm
	gsl_odeiv_step * s = gsl_odeiv_step_alloc (T, 1); // Allocate a 1D ODE array
	gsl_odeiv_control * c = gsl_odeiv_control_y_new (1e-10, 0.0); // Set the relative/absolute error tolerances
	gsl_odeiv_evolve * e = gsl_odeiv_evolve_alloc (1);
	
	// Struct containing pointers to member functions to be used by the GSL integration routines
	ModelPointers p;
	p.mod = this;
	
	bool reached_asymptotic = false;
	
	// Set-up evolution variables
	double t = 0.0; double tsamp = 0.0;
	double h = 1e-16;
	double r = 0.0;
	double a[1] = { A1_REC }; 
	int status; // Variable to hold status of GSL integration
	
	vector<double> tmp_a1(YCELLS+2); // Temporary vector of a1(r=const, t) values
	vector<double> tmp_a1dot(YCELLS+2);
	vector<double> tmp_a2(YCELLS+2);
	vector<double> tmp_a2dot(YCELLS+2);
	
	// Loop through the different r's
	for(int i=0; i<XCELLS+2 and !reached_asymptotic; i++){
		
		t = t0; // Initial t value
		a[0] = A1_REC; // Initial a1 value
		tsamp = t0 + YSTEP*ts; // Initial sample point
		
		// Update r value and calculate M(r), K(r)
		r = r0 + (double)i*XSTEP*rs;
		g_x.push_back(r/rs);
		p.m = xM(r); p.k = xK(r);
		gsl_odeiv_system sys = {static_a1dot, a1dot_jac, 1, &p};
		
		// Loop through sample points
		for(int j=0; j<YCELLS+2; j++){
			
			// Integrate until the next sample point
			while (t < tsamp){
				status = gsl_odeiv_evolve_apply (e, c, s, &sys, &t, tsamp, &h, a);
				if (status != GSL_SUCCESS){ cerr << "Yikes!" << endl; break; }
			} // end while loop
			
			// Add sample values to the array
			tmp_a1[j] = a[0];
			tmp_a1dot[j] = s_a1dot(a[0], p.m, p.k);
			if(i>1){
				tmp_a2[j] = g_a1[i-1][j] + (r/rs)*(1./XSTEP)*(tmp_a1dot[j] - g_a1[i-2][j]) ;
				tmp_a2dot[j] = ( g_a1dot[i-1][j] + (r/rs)*(1./XSTEP)*(tmp_a1dot[j] - g_a1dot[i-2][j]) );
				if(tmp_a2[j] <= 0.0){cerr << "WARNING: Shell crossing occurred at r=" << r << ", t=" << t << " (NumericGrid::populate_grid)" << endl; throw 99;} // end check
			}
			
			// Update to next sample time
			tsamp += YSTEP*ts;
		} // end loop through time samples
		
		g_a1.push_back(tmp_a1); // Add temporary r=const array to the 2D (r,t) array
		g_a1dot.push_back(tmp_a1dot);
		g_a2.push_back(tmp_a2);
		g_a2dot.push_back(tmp_a2dot);
		
	} // end loop through r values
	
	
	// Tidy-up values that couldn't be calculated efficiently in the loop
	for(int jj=0; jj<YCELLS+2; jj++){
		// Be careful with the x being used - x should be xlast, but use x0 for the first ones
		g_a2[0][jj] = g_a1[0][jj];
		g_a2[1][jj] = g_a1[1][jj];
		g_a2dot[0][jj] = g_a1dot[0][jj];
		g_a2dot[1][jj] = g_a1dot[1][jj];
	}
	
	// Free all of the integration variables
	gsl_odeiv_evolve_free (e);
	gsl_odeiv_control_free (c);
	gsl_odeiv_step_free (s);
	
	cerr << "\tpopulate_grid(): " << ((double)clock() - (double)tclock)/CLOCKS_PER_SEC << " seconds." << endl;
}
*/
