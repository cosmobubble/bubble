
// FIXME: Are the signs the right way around in any of this?

/*
void Model::offcentre_z_coords(double robserver, double tobserver,
							   double zobs, 
							   double *rz_in, double *tz_in,
							   double *rz_out, double *tz_out){
	/// Find the r and t at which the radial geodesic for an observer at 
	/// robs and tobs reaches a specific redshift zobs. Returns values in 
	/// *rz and *tz (_in for the geodesic going into the void, _out for 
	/// the one going out of the void)
	
	
}*/

double Model::outgoing_redshift(double r_start, double t_start, double t_end){
	/// Return the redshift for rays going out of the void
	double k1, k2, k3, k4;
	double dtr = dt * C; // dt in distance units
	double dr = 0.0;
	double dI = 0.0; double dIlast = 0.0; double I = 0.0;
	
	// Our carefully-chosen initial conditions
	double r = r_start;
	
	// TESTING: Off-centre geodesic plotting
	//ofstream gd; gd.open(TEST_FILENAME.c_str(), ios_base::app);
	//double tsamp = t_start*2.; // Large starting value, so we sample in the first instance
	
	for(double t=t_start; t - tb(r) > (t_end - dt); t+=dt){
		k1 = -1. * sqrt(1. - k(r)*r*r) / a2(r, t);
		k2 = -1. * sqrt(1. - k(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1))
				/ a2(r + 0.5*dtr*k1, t + 0.5*dt);
		k3 = -1. * sqrt(1. - k(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2))
				/ a2(r + 0.5*dtr*k2, t + 0.5*dt);
		k4 = -1. * sqrt(1. - k(r + dtr*k3)*(r + dtr*k3)*(r + dtr*k3))
				/ a2(r + k3*dtr, t + dt);
		
		dr = (dtr/6.)*(k1 + 2.*k2 + 2.*k3 + k4); r += dr;
		dI = -1. * a2dot(r, t) / a2(r, t);
		I += dI + dIlast; dIlast = dI;
		
		/*
		// TESTING: Geodesic output
		if(fabs(t-tsamp) > 100.){
			gd << r << "\t" << t << "\t" << Hr(fabs(r), t) << "\to" << endl;
			tsamp = t;
		}*/
		
	} // end integration loop
	
	//gd.close(); // TESTING
	
	return exp(I * 0.5 * dt) - 1.; // Redshift, z
}

double Model::ingoing_redshift(double r_start, double t_start, double t_end, double* r_end){
	/// Return the redshift for rays going out of the void
	double k1, k2, k3, k4;
	double dtr = dt * C; // dt in distance units
	double dr = 0.0;
	double dI = 0.0; double dIlast = 0.0; double I = 0.0;
	
	// Our carefully-chosen initial conditions
	double r = r_start; double rr = r_start; // rr can go -ve, r always +ve
	
	// TESTING: Off-centre geodesic plotting
	//ofstream gd; gd.open(TEST_FILENAME.c_str(), ios_base::app);
	//double tsamp = t_start*2.; // Large starting value, so we sample in the first instance
	
	for(double t=t_start; t - tb(r) > (t_end - dt); t+=dt){
		k1 = 1. * sqrt(1. - k(fabs(r))*r*r) / a2(fabs(r), t);
		k2 = 1. * sqrt(1. - k(fabs(r + 0.5*dtr*k1))*(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1))
				/ a2(fabs(r + 0.5*dtr*k1), t + 0.5*dt);
		k3 = 1. * sqrt(1. - k(fabs(r + 0.5*dtr*k2))*(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2))
				/ a2(fabs(r + 0.5*dtr*k2), t + 0.5*dt);
		k4 = 1. * sqrt(1. - k(fabs(r + dtr*k3))*(r + dtr*k3)*(r + dtr*k3))
				/ a2(fabs(r + k3*dtr), t + dt);
		
		dr = (dtr/6.)*(k1 + 2.*k2 + 2.*k3 + k4); rr += dr; r = fabs(rr);
		
		dI = -1. * a2dot(r, t) / a2(r, t);
		I += dI + dIlast; dIlast = dI;
		
		/*
		// TESTING: Geodesic output
		if(fabs(t-tsamp) > 100.){
			gd << r << "\t" << t << "\t" << a2dot(fabs(r), t) / a2(fabs(r), t) << "\ti1" << endl;
			tsamp = t;
		}*/
		
	} // end integration loop
	
	//gd.close();
	*r_end = r;
	return exp(I * 0.5 * dt) - 1.; // Redshift, z. Factor of 0.5*dt comes from simple integration
}

double Model::adaptive_v_dipole(double robs){
	/// Use the adaptive algorithms to calculate the offcentre 
	/// velocity dipole (in km/sec)
	
	double t_lss = fn_tz(1100.0); // Find the time at which the central observer thinks the CMB formed at
	double tobs = fn_t(robs);
	
	// The r value at which the outgoing geodesic hits the LSS
	// (This geodesic is along the same path as the central geodesic)
	double rlss_outgoing = fn_r(t_lss);
	// The r value at which the ingoing geodesic hits the LSS
	double rlss_ingoing = adaptive_r_fnt(tobs, t_lss, robs, GEODESIC_INGOING);
	
	init_integrators(); // Initialise the GSL integration routines
	
	// Use the adaptive integrator to find the integrand of z
	double I_outgoing = adaptive_I(robs, rlss_outgoing, tobs, GEODESIC_OUTGOING);
	double I_ingoing = adaptive_I(robs, rlss_ingoing, tobs, GEODESIC_INGOING);
	free_integrators(); // Free the memory used by the GSL integration/ODE solving routines
	
	// Calculate z_in, z_out
	double z_in = exp(I_ingoing) - 1.0;
	double z_out = exp(I_outgoing) - 1.0;
	
	
	
	return 3e5 * (z_in - z_out) / (2. + z_in + z_out);
}

double Model::v_dipole(double r, double* r_end = NULL){
	/// Find the dipole velocity (derived from the redshift) at radius r
	/// (in km/sec)
	
	// FIXME: This doesn't deal with varying Bang time properly
	// Should use endpoint as the local time after Big Bang?
	
	// Find the local time at which point r is observed by a central 
	// observer today
	double t_obs = fn_t(r);
	
	/*
	stringstream fname(stringstream::in | stringstream::out);
	fname << "data/offcentre-" << r;
	TEST_FILENAME = fname.str();
	ofstream gd; gd.open(TEST_FILENAME.c_str()); // TESTING: Wipe the offcentre file
	*/
	
	// Find the time at which the redshift is z=40
	// (proxy for z=1100, decoupling)
	// FIXME: Needs to go to z=1100
	// *Local* time at decoupling
	double t_global_dec = fn_tz(500.0);
	double t_dec = t_global_dec - tb(fn_r(t_global_dec)); // FIXME: z=40
	
	// Find the redshift for the ray going *out* of the void
	// FIXME: Can be replaced by the pre-calculated geodesic
	double z_out = outgoing_redshift(r, t_obs, t_dec);
	
	// Find the redshift for the ray going *through* the void
	double z_in = ingoing_redshift(r, t_obs, t_dec, r_end);
	
	// GBH give dT/T|Dipole = (z_in - z_out) / (2 + z_in + z_out)
	return 3e5 * (z_in - z_out) / (2. + z_in + z_out);
}
