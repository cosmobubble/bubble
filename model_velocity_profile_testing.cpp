
/// Use this module instead of model_velocity_testing.cpp if you want 
/// to output the integrands for the off-centre geodesic integrations

// FIXME: Are the signs the right way around in any of this?

double Model::outgoing_redshift(double r_start, double t_start, double t_end){
	/// Return the redshift for rays going out of the void
	//double dt = -1e-1;
	double k1, k2, k3, k4;
	double dtr = dt * C; // dt in distance units
	double dr = 0.0;
	double dI = 0.0; double dIlast = 0.0; double I = 0.0;
	
	// Our carefully-chosen initial conditions
	double r = r_start;
	double tlast = t_start; // How far we've travelled in t since the last sample
	
	
	// TESTING: Off-centre geodesic plotting
	ofstream gd; gd.open(TEST_FILENAME.c_str(), ios_base::app);
	double tsamp = t_start*2.; // Large starting value, so we sample in the first instance
	
	
	for(double t=t_start; t > t_end - dt; t+=dt){
		k1 = -1. * sqrt(1. - k(r)*r*r) / a2(r, t);
		k2 = -1. * sqrt(1. - k(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1))
				/ a2(r + 0.5*dtr*k1, t + 0.5*dt);
		k3 = -1. * sqrt(1. - k(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2))
				/ a2(r + 0.5*dtr*k2, t + 0.5*dt);
		k4 = -1. * sqrt(1. - k(r + dtr*k3)*(r + dtr*k3)*(r + dtr*k3))
				/ a2(r + k3*dtr, t + dt);
		
		dr = (dtr/6.)*(k1 + 2.*k2 + 2.*k3 + k4); r += dr;
		dI = -1. * a2dot(r, t) / a2(r, t);
		I += dI + dIlast; dIlast = dI;
		
		
		// TESTING: Geodesic output
		if(fabs(t-tsamp) > 100.){
			gd << r << "\t" << t << "\t" << a2dot(fabs(r), t) / a2(fabs(r), t) << "\to" << endl;
			tsamp = t;
		}
	} // end integration loop
	
	/*
	gd << "---" << endl;*/
	gd.close(); // TESTING
	//cerr << "out(1) = " << I  << endl;
	return exp(I * 0.5 * dt) - 1.; // Redshift, z
}

double Model::ingoing_redshift(double r_start, double t_start, double t_end){
	/// Return the redshift for rays going into the void
	/*double dt = -1e-1;*/ double t;
	double k1, k2, k3, k4;
	double dtr = dt * C; // dt in distance units
	double dr = 0.0;
	double dI = 0.0; double dIlast = 0.0; double I = 0.0;
	
	
	// TESTING: Off-centre geodesic plotting
	ofstream gd; gd.open(TEST_FILENAME.c_str(), ios_base::app);
	double tsamp = t_start*2.; // Large starting value, so we sample in the first instance
	
	
	// Our carefully-chosen initial conditions
	double r = r_start;
	double tlast = t_start; // How far we've travelled in t since the last sample
	
	// First, integrate along the ingoing geodesic, until r=0.0 is reached or t_end is reached
	for(t=t_start; r>0.0 and t>t_end; t+=dt){
		k1 = -1. * sqrt(1. - k(r)*r*r) / a2(r, t);
		k2 = -1. * sqrt(1. - k(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1))
				/ a2(r + 0.5*dtr*k1, t + 0.5*dt);
		k3 = -1. * sqrt(1. - k(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2))
				/ a2(r + 0.5*dtr*k2, t + 0.5*dt);
		k4 = -1. * sqrt(1. - k(r + dtr*k3)*(r + dtr*k3)*(r + dtr*k3))
				/ a2(r + k3*dtr, t + dt);
		
		dr = (-1. * dtr/6.)*(k1 + 2.*k2 + 2.*k3 + k4); r += dr; // Don't forget the minus sign for inbound geodesics!
		dI = -1. * a2dot(fabs(r), t) / a2(fabs(r), t); // Need |r| since dr is decreasing and will overshoot r=0.0 slightly
		I += dI + dIlast; dIlast = dI;
		
		
		// TESTING: Geodesic output
		if(fabs(t-tsamp) > 100.){
			gd << r << "\t" << t << "\t" << a2dot(fabs(r), t) / a2(fabs(r), t) << "\ti1" << endl;
			tsamp = t;
		}
		
	} // end integration loop
	
	// Check if we're at t=t_end. If not, keep integrating along an 
	// outgoing geodesic until we are! 
	if(t<=t_end){
		/*gd << "---" << endl;*/
		gd.close(); // TESTING
		//cerr << "in(1) = " << I  << endl;
		// FIXME: Minus-sign again?
		return exp(I * 0.5 * dt) - 1.; // Redshift, z. Units are OK, since I in (T^-1)
	}else{
		// Keep integrating along an outgoing geodesic from r=0
		// New initial conditions
		r = 0.0; tlast = t;
		double dI2 = 0.0; double dIlast2 = 0.0; double I2 = 0.0;
		
		// First, integrate along the ingoing geodesic, until r=0.0 is reached or t_end is reached
		// FIXME: K(x), not K(r) any more!
		for(t=tlast; t>t_end; t+=dt){
			k1 = -1. * sqrt(1. - k(r)*r*r) / a2(r, t);
			k2 = -1. * sqrt(1. - k(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1))
					/ a2(r + 0.5*dtr*k1, t + 0.5*dt);
			k3 = -1. * sqrt(1. - k(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2))
					/ a2(r + 0.5*dtr*k2, t + 0.5*dt);
			k4 = -1. * sqrt(1. - k(r + dtr*k3)*(r + dtr*k3)*(r + dtr*k3))
					/ a2(r + k3*dtr, t + dt);
			
			dr = (dtr/6.)*(k1 + 2.*k2 + 2.*k3 + k4); r += dr;
			dI2 = -1. * a2dot(r, t) / a2(r, t);
			I2 += dI2 + dIlast2; dIlast2 = dI2;
			
			
			// TESTING: Geodesic output
			if(fabs(t-tsamp) > 100.){
				gd << r << "\t" << t << "\t" << a2dot(fabs(r), t) / a2(fabs(r), t) << "\ti2" << endl;
				tsamp = t;
			}
			
		} // end integration loop
		
		
		//gd << "---" << endl;
		gd.close(); // TESTING
		
		//cerr << "in(1) = " << I  << endl;
		//cerr << "in(2) = " << I2 << endl;
		
		// Return the sum of the redshifts accumulated along the geodesic
		// FIXME: Should this be (I2 - I)?
		return exp( (I2 + I) * 0.5 * dt ) - 1.;
	} // end t=t_end check
}

double Model::v_dipole(double r, double ZLIM){
	/// Find the dipole velocity (derived from the redshift) at radius r
	/// (in km/sec)
	
	// Find the local time at which point r is observed by a central 
	// observer today
	double t_obs = fn_t(r);
	
	stringstream fname(stringstream::in | stringstream::out);
	fname << "data/offcentre-" << r;
	TEST_FILENAME = fname.str();
	ofstream gd; gd.open(TEST_FILENAME.c_str()); // TESTING: Wipe the offcentre file
	
	
	// Find the time at which the redshift is z=20
	// (proxy for z=1100, decoupling)
	// FIXME: Needs to go to z=1100
	double t_dec = fn_tz(ZLIM); //z=10 prev. /// WAS 40 for 1st run
	
	// Find the redshift for the ray going *out* of the void
	double z_out = outgoing_redshift(r, t_obs, t_dec);
	
	// Find the redshift for the ray going *through* the void
	double z_in = ingoing_redshift(r, t_obs, t_dec);
	
	// Output redshifts
	//cerr << "\tz_in = " << z_in << endl;
	//cerr << "\tz_out = " << z_out << endl;
	
	// Turn the redshift into a velocity, km/sec
	// (-ve means it appears to be moving out of the void???)
	
	//return (z_out - z_in) /* * 3e5 */;
	// GBH give dT/T|Dipole = (z_in - z_out) / (2 + z_in + z_out)
	return (z_in - z_out) / (2. + z_in + z_out);
}

