
double Model::m(double r){
	/// Density-like function m(r)
	return 0.0;
}

double Model::mprime(double r){
	/// Radial derivative of the density-like function,. m'(r)
	return 0.0;
}

double Model::k(double r){
	/// Curvature-like function k(r)
	return 0.0;
}

double Model::kprime(double r){
	/// Radial derivative of the curvature-like function, k'(r)
	return 0.0;
}

double Model::tb(double r){
	/// Bang-time function t_b(r)
	return 0.0;
}

double Model::tbprime(double r){
	/// Radial derivative of the bang-time function, t_b'(r)
	return 0.0;
}

// Dimensionless functions to be used in the grid integration
double Model::xM(double x){
	/// Dimensionless density-like function, M(x) = 8*pi*G/3 * ts^2 * m(x)
	return ((8.*M_PI*G)/3.) * ts*ts * m(x*rs);
}

double Model::xMprime(double x){
	/// Dimensionless radial derivative of density-like function, M'(x)
	/// M'(x) = 8*pi*G/3 * ts^2 * rs * dm(r)/dr
	return ((8.*M_PI*G)/3.) * ts*ts * rs * mprime(x*rs);
}

double Model::xK(double x){
	/// Dimensionless curvature-like function, K(x) = k(x) * ts^2 * C^2
	return k(rs*x) * ts*ts * C*C;
}

double Model::xKprime(double x){
	/// Dimensionless radial derivative of curvature-like function, K'(x)
	/// K'(x) = k(x) * ts^2 * C^2 * rs
	return kprime(rs*x) * ts*ts * C*C * rs;
}

double Model::xTb(double x){
	/// Bang-time function, t_B(x)
	// FIXME: Bang-time function. Is the dimensional scaling right?
	return tb(x*rs)/ts;
}

double Model::xTbprime(double x){
	/// Radial derivative of Bang-time function, t_B'(x)
	// FIXME: Bang-time function derivative. Is the dimensional scaling right?
	return (rs/ts)*tbprime(x*rs);
}
