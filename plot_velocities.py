#!/usr/bin/python

from pylab import *
import sys

# Lists of calculated variables
r = []
z = []
vp = []
dHr = []
vp_frw = []

# Get data
#f = open("data/velocity-" + str(sys.argv[1]), 'r')
fnames = ["A", "B", "C"]

for i in range(len(fnames)):
	r.append([])
	z.append([])
	vp.append([])
	dHr.append([])
	vp_frw.append([])
	
	f = open("data/velocity-"+fnames[i], 'r')
	for line in f.readlines():
		tmp = line.split("\t")
		if len(tmp) >= 4:
			r[i].append(float(tmp[0]))
			z[i].append(float(tmp[1]))
			vp[i].append(float(tmp[2]))
			dHr[i].append(float(tmp[3]))
			vp_frw[i].append(float(tmp[4]))
	f.close()

"""
subplot(131)
hhh = map(lambda x:x*1e2, dHr)
p = plot(z, vp, 'r+', z, vp, 'r-')
plot(z, hhh, 'bx', z, hhh, 'b-')
grid(True)
title('vp(z)')
"""
"""
subplot(111)
plot(z[0], vp[0], 'k-', label="$\Omega^{in}_k$=0.7, $r_0$=1.5 Gpc")
plot(z[1], vp[1], 'r-', label="$\Omega^{in}_k$=0.3, $r_0$=2.5 Gpc")
plot(z[2], vp[2], 'b-', label="$\Omega^{in}_k$=0.7, $r_0$=0.5 Gpc")
grid(True)
title('Velocity Mismatch (CFLb)')
xlabel('z')
ylabel('vp(z) [Mismatch]')
legend()
xlim((0.0, 6.0))
"""

subplot(121)
plot(vp, vp_frw, 'k-')
grid(True)
title('Velocity Mismatch')
xlabel('vp(z) [kSZ]')
ylabel('vp(z) [FRW Pec.]')

subplot(122)
p = plot(z, vp, 'r-', label="vp(z)|kSZ")
plot(z, vp_frw, 'b-', label="cz - H0.dL(z)")
grid(True)
title('vp(z)')
xlabel('z')
legend()

show()

