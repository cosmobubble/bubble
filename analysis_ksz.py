#!/usr/bin/python

class obskSZ(object):
	"""A collection of kSZ data points"""
	
	def load_ksz_data(self):
		"""Load the kSZ data from a file"""
		f = open(self.filename, 'r')
		for line in f.readlines():
			line = line.split("\t")
			if len(line) > 4 and line[0]!="#":
				self.z.append(float(line[1]))
				self.vp.append(float(line[2]))
				self.e_upper.append(float(line[3]))
				self.e_lower.append(float(line[4]))
	
	def __init__(self):
		self.filename = "obsdata/GBH_kSZ_data.dat" # The file to retrieve the data from
		
		self.z = [] # The z value taken for this measurement
		self.vp = [] # The measured peculiar velocity, in km/sec
		self.e_upper = [] # The upper error on vp
		self.e_lower = [] # The lower error on vp
		
		self.load_ksz_data() # Load the data from the file

def get_closest(z, zvec):
	"""Get the data point from the model (zvec) with the closest z value 
	to some real kSZ measurement (z). Returns the index in 
	zvec which comes closest. Assumes that zvec is sorted."""
	a = 0
	b = len(zvec) - 1
	
	while (b-a)>1:
		mid = a + int(0.5*float(b-a))
		if zvec[mid] > z:
			b = mid
		else:
			a = mid
	return a

def chisquared(model_z, model_vp):
	"""Return the chi-squared statistic for the kSZ vp(z) data, and the no. of data points"""
	# model_z and model_kSZ are vectors of data output by the model
	
	obs = obskSZ() # Load the SNe data from the file
	x2 = 0.0 # The Chi-squared statistic
	count = 0
	
	for j in range(len(obs.vp)):
		count += 1
		i = get_closest(obs.z[j], model_z) # Get the index of the closest of the model-output data points
		# FIXME: This isn't right, because we're forgetting the systematic error, and the data clearly isn't Gaussian - lopsided error bars!
		x2 += (obs.vp[j] - model_vp[i])**2.0 / (obs.e_lower[j]**2.0) # Calculate the contribution to the X^2 sum for this data point
	return x2, count
