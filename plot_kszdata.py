#!/usr/bin/python

from pylab import *
import numpy.random as nran
import sys

# Lists of calculated variables
r = []
z = []
vpmismatch = []
vpm = []
evpm = []
vpk = []
evpk = []
vpkfrw = []
vpkstat = []

linx = [-1500.0, 1500.0]

# Get data
f = open("vpksz_est.dat", 'r')

for line in f.readlines():
	tmp = line.split("\t")
	if len(tmp) >= 4:
		r.append(float(tmp[0]))
		z.append(float(tmp[1]))
		vpmismatch.append(float(tmp[2]))
		vpm.append(float(tmp[3]))
		evpm.append(float(tmp[4]))
		vpk.append(float(tmp[5]))
		evpk.append(float(tmp[6]))
f.close()

# Generate random ksz data for FRW, where vpm=vpk
for i in range(len(vpm)):
	vpkfrw.append(nran.normal(vpm[i], evpk[i]))
# Generate random ksz data for void, where vpk = vpm + vpmismatch
for i in range(len(vpm)):
	vpkstat.append(nran.normal(vpk[i], evpk[i]))

subplot(121)
errorbar(vpm, vpkstat, xerr=evpm, yerr=evpk, fmt='ko')
#errorbar(vpm, vpkfrw, xerr=evpm, yerr=evpk, fmt='bo')
plot(linx, linx, 'r-')
grid(True)
title('Velocity Mismatch (CFLb void)')
xlabel('vp(z) [Matter]')
ylabel('vp(z) [kSZ]')


subplot(122)
errorbar(vpm, vpkfrw, xerr=evpm, yerr=evpk, fmt='ko')
plot(linx, linx, 'r-')
grid(True)
title('Velocity Mismatch (FRW)')
xlabel('vp(z) [Matter]')
ylabel('vp(z) [kSZ]')

show()

