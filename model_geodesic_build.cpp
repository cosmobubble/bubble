
void Model::generate_geodesic(){
	/// Build a geodesic by integrating along dt to find (r,t) at 
	/// multiple sample points
	/// Also create a geodesic sample
	/// USES ADAPTIVE INTEGRATOR
	clock_t tclock = clock();
	
	// Set initial values
	double tnext = 0.0; double rcell = 10.0;
	
	// Add initial values to the r,t arrays
	double t = t_today;
	double r = 0.0;
	double I = 0.0;
	double zlast = 0.0;
	double zz = 0.0;
	rval.push_back(r); tval.push_back(t); zval.push_back(0.0);
	
	init_integrators(); // Initialise the GSL integration routines
	
	// Loop through t cells
	while (t > t0 + 0.01 + tb(r) and zz < 1200.0){
		
		// Adaptive step to find t(r) and integrand of z, I
		tnext = adaptive_t_fnr(r, r+rcell, t, GEODESIC_OUTGOING);
		I += adaptive_I(r, r+rcell, t);
		
		// Increment values
		r += rcell;
		t = tnext;
		zz = exp(I) - 1.0;
		
		// Check if integration step size could be changed
		if(zz - zlast > 10.0){
			rcell *= 0.1;
		}else if(zz - zlast < 0.001){
			rcell *= 10.0;
		}
		zlast = zz;
		
		// Add values to array
		rval.push_back(r);
		tval.push_back(t);
		zval.push_back(zz);
	}
	
	free_integrators(); // Free the memory used by the GSL integration/ODE solving routines
	
	// TESTING
	cerr << "\tgenerate_geodesic(): " << ((double)clock() - (double)tclock)/CLOCKS_PER_SEC << " seconds." << endl;
}


/*
void Model::generate_geodesic(){
	/// Build a geodesic by integrating along dt to find (r,t) at 
	/// multiple sample points
	/// Also create a geodesic sample
	
	clock_t tclock = clock();
	double k1, k2, k3, k4;
	double dt2 = (t0 - t_today)/1e6;
	double dtr = dt2 * C; // dt in distance units
	double dr = 0.0; double r = r0;
	double tlast = 2.*t_today; // How far we've travelled in t since the last sample. Set to large value initially so we get the first sample.
	double dI = 0.0; double dIlast = 0.0; double I = 0.0;
	double tcell2 = tcell;
	//bool switched = false;
	
	// Add initial values to the r,t arrays
	rval.push_back(r); tval.push_back(t_today); zval.push_back(0.0);
	
	// FIXME: Needs better high-z behaviour, very inaccurate ATM
	// Have to stop a step early, because k4 = k4(t + dt2), so need t + dt2 > t_end
	for(double t=t_today; t > (t0-dt2) + tb(r); t+=dt2){
		
		// Calculate Runge-Kutta variables
		k1 = -1. * sqrt(1. - k(r)*r*r) / a2(r, t);
		k2 = -1. * sqrt(1. - k(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1))
				/ a2(r + 0.5*dtr*k1, t + 0.5*dt2);
		k3 = -1. * sqrt(1. - k(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2))
				/ a2(r + 0.5*dtr*k2, t + 0.5*dt2);
		k4 = -1. * sqrt(1. - k(r + dtr*k3)*(r + dtr*k3)*(r + dtr*k3))
				/ a2(r + k3*dtr, t + dt2);
		
		// Add value to r
		dr = (dtr/6.)*(k1 + 2.*k2 + 2.*k3 + k4);
		r += dr;
		
		// Add value to redshift integral
		// FIXME: Doesn't properly deal with the first couple of values!
		dI = -1. * dt2 * a2dot(r, t) / a2(r, t);
		I += dI + dIlast;
		dIlast = dI;
		
		
		
		// Add the next sample, if it's time to do so
		if((tlast - t) > tcell2){
			//cerr << dI << endl;
			if(dI>1e-5 and -dt2 > (t_today - t0)/1e10){dt2 *= 0.1; dtr = dt2 * C;} // Adaptively shrink dt2
			tlast = t;
			rval.push_back(r);
			tval.push_back(t);
			// This is OK, the dimensions work out; C is cancelled in 
			// the calculation, and we're only multiplying by the 
			// constant factor (dt/2) once for speed.
			zval.push_back( exp(I * 0.5) - 1. );
			
		} // end sampling check
		
	} // end integration loop
	
	// TESTING
	cerr << "r=" << rval[rval.size()-1] << ", t=" << tval[rval.size()-1] << ", z=" << zval[rval.size()-1] << endl;
	cerr << "\tNo. samples = " << rval.size() << endl;
	cerr << "\tgenerate_geodesic(): " << ((double)clock() - (double)tclock)/CLOCKS_PER_SEC << " seconds." << endl;
}*/
