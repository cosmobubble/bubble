
class CFLc: public Model{

public:

	/// Clifton, Ferreira, Land, LTB model (c)
	CFLc(double _h, double _q, double _w){
		/// Constructor: CFL model (b), k~exp(-cr^3)
		/// M0 set to give h0 = h at a1(r=0, today) = 1
		A = -1.0 * _q * pow( (_h*fH0/C), 2.0); // K(r) amplitude scale, where (H0/C)^2 is the maximum allowed
		KR = 1./(_w); // K(r) curvature scale
		h = _h; // Hubble parameter
		M0 = (3. / (8.*M_PI*G)) * ( (fH0*fH0*h*h) + A*C*C ); // Density amplitude scale
		
		// Name this model based on the passed parameters
		stringstream modelinfo(stringstream::in | stringstream::out);
		modelinfo << "CFLc, h=" << h << ", q=" << _q << ", w=" << _w << endl;
		output_details(modelinfo.str());
		
		go();
	}
	
	double m(double r){
		/// Density-like function, m(r)
		return M0;
	}
	
	double mprime(double r){
		/// Radial derivative of density-like function
		return 0.0;
	}

	double k(double r){
		/// Curvature-like function, k(r)
		double kk = A * (1. - tanh(KR*r));
		return kk;
	}

	double kprime(double r){
		/// Radial derivative of curvature-like function, k'(r)
		return A * (1. - tanh(KR*r)*tanh(KR*r) );
	}

private:

	double A, KR, M0;

};
