
void Model::init_integrators(){
	/// Initialises the I integrator and t(r) ODE solver
	integ_w = gsl_integration_workspace_alloc (1000);
	ode_T = gsl_odeiv_step_rkf45; // Runge-Kutta-Fehlberg (4,5) adaptive algorithm
	ode_s = gsl_odeiv_step_alloc (ode_T, 1); // Allocate a 1D ODE array
	ode_c = gsl_odeiv_control_y_new (1e-5, 0.0); // Set the relative/absolute error tolerances rel=1e-12 by default
	ode_e = gsl_odeiv_evolve_alloc (1);
}

void Model::free_integrators(){
	/// Frees everything used by the I integrator and t(r) ODE solver
	gsl_integration_workspace_free(integ_w);
	gsl_odeiv_evolve_free (ode_e);
	gsl_odeiv_control_free (ode_c);
	gsl_odeiv_step_free (ode_s);
}


int Model::static_ft (double r, const double t[], double dtdr[], void *params){
	/// Static wrapper function used for GSL integration of the radial 
	/// geodesic equation dt/dr = f(r, t)
	ModelPointers p = *(ModelPointers *)params; // Get model parameters
	
	// Set the system of equations to be integrated (there's only one)
	dtdr[0] = p.sign * p.mod->a2(fabs(r), t[0]) / (C * sqrt(1. - p.mod->k(fabs(r))*r*r));
	
	return GSL_SUCCESS;
}

int Model::static_fr (double t, const double r[], double drdt[], void *params){
	/// Static wrapper function used for GSL integration of the radial 
	/// geodesic equation dr/dt = f(r, t)
	ModelPointers p = *(ModelPointers *)params; // Get model parameters
	
	// Set the system of equations to be integrated (there's only one)
	drdt[0] = p.sign * C * sqrt(1. - p.mod->k(fabs(r[0]))*r[0]*r[0]) / p.mod->a2(fabs(r[0]), t);
	
	return GSL_SUCCESS;
}

int Model::ft_jac(double r, const double t[], double *dfdt, double dfdr[], void *params){
	/// Static wrapper function used to define the Jacobian of the 
	/// radial geodesic equation for use in GSL ODE solver
	ModelPointers p = *(ModelPointers *)params; // Get model parameters
	
	// Construct a matrix for the Jacobian
	gsl_matrix_view dfdt_mat = gsl_matrix_view_array (dfdt, 1, 1);
	gsl_matrix * m = &dfdt_mat.matrix;
	
	// Set the Jacobian matrix elements
	double rr = fabs(r);
	gsl_matrix_set (m, 0, 0, p.sign * p.mod->a2dot(rr, t[0]) / (C * sqrt(1. - p.mod->k(rr)*rr*rr)) );
	
	dfdr[0] = 0.0; // Set df/dr = 0 initially (FIXME: is this right? Seems to work)
	
	return GSL_SUCCESS;
}

int Model::fr_jac(double t, const double r[], double *dfdr, double dfdt[], void *params){
	/// Static wrapper function used to define the Jacobian of the 
	/// radial geodesic equation for use in GSL ODE solver
	ModelPointers p = *(ModelPointers *)params; // Get model parameters
	
	// Construct a matrix for the Jacobian
	gsl_matrix_view dfdr_mat = gsl_matrix_view_array (dfdr, 1, 1);
	gsl_matrix * m = &dfdr_mat.matrix;
	
	// Set the Jacobian matrix elements
	double rr = fabs(r[0]);
	double okr = sqrt(1. - p.mod->k(rr)*rr*rr);
	double inva2 = 1. / p.mod->a2(rr, t);
	double element_dfdr = 
		- C * okr * inva2 * inva2 * p.mod->a2prime(rr, t)
		- ( 0.5 * C * inva2 * (p.mod->kprime(rr)*rr*rr + 2.*p.mod->k(rr)*rr) / okr );
	
	gsl_matrix_set (m, 0, 0, p.sign * element_dfdr);
	
	dfdt[0] = 0.0; // Set df/dr = 0 initially (FIXME: is this right?)
	
	return GSL_SUCCESS;
}

double Model::static_z_integrand (double r, void * params){
	/// Static member function for use in the GSL integration: this is 
	/// the integrand used when calculating z = exp(I) - 1
	ModelPointers p = *(ModelPointers *)params; // Get model parameters
	
	// Find what t should be at this point
	double t = p.mod->adaptive_t_fnr(p.rstart, r, p.t_rstart, p.geodesic_direction);
	
	// Return the value of the integrand at this point
	return p.mod->a2dot(r, t) / (C*sqrt(1. - p.mod->k(r)*r*r));
}


double Model::adaptive_I(double rstart, double rend, double t_rstart, bool geodesic_direction){
	/// Adaptively integrate the integrand used to find z (as fn r)
	
	// Prepare variables which will be used by the integrator
	double result, error;
	ModelPointers p; p.mod = this; p.geodesic_direction = geodesic_direction;
	p.rstart = rstart; p.rend = rend; p.t_rstart = t_rstart  ;
	
	// Prepare the integrator
	gsl_function F;
	F.function = &static_z_integrand;
	F.params = &p;
	
	// Do the integration
	gsl_integration_qag (&F, rstart, rend, 0, 1e-7, 1000, GSL_INTEG_GAUSS41, integ_w, &result, &error);
	
	return result;
}

double Model::adaptive_t_fnr(double rstart, double rend, double tstart, bool ingoing){
	/// Adaptively integrate the radial geodesic ODE from
	/// (rstart, tstart) until rend, and return t(rend). Set ingoing to 
	/// change the sign of the geodesic (going into or out of void)
	int status;
	
	// Make sure we start at the right place
	ModelPointers p; p.mod = this;
	double hh;
	double t[1] = { tstart }; // [1] just means it's a 1D array
	double r = rstart;
	
	// Set signs depending on whether the geodesic is ingoing or outgoing
	if(ingoing){ p.sign = +1.0; hh = -1.0 * 1e-16; rend *= -1.0; }
	else{ p.sign = -1.0; hh = +1.0 * 1e-16; }
	
	// Define system of ODEs (ODE + Jacobian)
	gsl_odeiv_system sys = {static_ft, ft_jac, 1, &p};
	
	// Integrate until we reach the desired r
	while (fabs(r) < fabs(rend)){
		status = gsl_odeiv_evolve_apply (ode_e, ode_c, ode_s, &sys, &r, rend, &hh, t);
		if (status != GSL_SUCCESS){ cerr << "Model::adaptive_t_fnr(): Integration failed." << endl; break; }
	} // end while loop
	if(isnan(t[0])){cerr << "ERROR: Model::adaptive_t_fnr, t is NaN\n"; throw 111;}
	
	return *t;
}


double Model::adaptive_r_fnt(double tstart, double tend, double rstart, bool ingoing){
	/// Adaptively integrate the radial geodesic ODE from
	/// (rstart, tstart) until tend, and return r(tend). Set ingoing to 
	/// change the sign of the geodesic (going into or out of void)
	int status;
	
	// Make sure we start at the right place
	ModelPointers p; p.mod = this;
	double hh;
	double r[1] = { rstart }; // [1] just means it's a 1D array
	double t = tstart;
	
	// Set signs depending on whether the geodesic is ingoing or outgoing
	if(ingoing){ p.sign = +1.0; hh = -1.0 * 1e-16;}
	else{ p.sign = -1.0; hh = +1.0 * 1e-16; }
	
	// Define system of ODEs (ODE + Jacobian)
	gsl_odeiv_system sys = {static_fr, fr_jac, 1, &p};
	
	// Integrate until we reach the desired r
	while (t > tend){
		status = gsl_odeiv_evolve_apply (ode_e, ode_c, ode_s, &sys, &t, tend, &hh, r);
		if (status != GSL_SUCCESS){ cerr << "Model::adaptive_r_fnt(): Integration failed." << endl; break; }
	} // end while loop
	if(isnan(r[0])){cerr << "ERROR: Model::adaptive_r_fnt, r is NaN\n"; throw 111;}
	
	return *r;
}
