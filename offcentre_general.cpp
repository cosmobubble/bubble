

double Model::general_offcentre(double robs, double tobs, double _gamma, double tend){
	/// Calculate general off-centre geodesics (not just radial ones)
	/// Warning: Can't handle r=0
	clock_t tclock = clock();
	
	// Set-up initial conditions
	double g = _gamma; // Polar angle, gamma
	double t = tobs; // Time
	double r = robs; double rr = robs; // Radial coordinate
	double th = 0.0; // Polar angle in LTB geometry, theta
	
	double dr, dt, dth, dp;
	
	double J = C * r * a1(r, t) * sin(g); // Angular momentum term
	double p = sqrt(1. - k(r)*r*r) * cos(g) / a2(r, t); // dr/du, change in radial coord along geodesic
	double invq, inva1r, okr, a2d, a2a, a2p; // Use invq = 1/q, since multiplication is faster than division
	
	double dz = 1e-10;
	double zz = z(robs);
	J = 0.0;
	
	cout << "t\tr\trr\tth\tp\tq\tz\tdt\tdr\tdth\tdp" << endl;
	cerr << "tend = " << tend << endl;
	
	int i = 0;
	
	
	while (t>tend) {
		
		// Calculate repeatedly-used values
		inva1r = 1. / ( r*a1(r, t) );
		okr = (1. - k(r)*r*r);
		a2d = a2dot(r, t);
		a2a = a2(r, t);
		a2p = 0.1*(a2(r+10.0, t) - a2(r, t)); // a2', radial derivative of a2
		invq = 	1. / (
						(  a2a * a2d * p*p / (okr*C)  )
					+	(  J*J*r*a1dot(r, t) * inva1r*inva1r*inva1r/(C*C*C)  )
					  );
		
		// Want to check if dr will be small enough
		// i.e. want dr ~ 0.1
		dz = 0.1 / (p * invq);
		
		// Do simple z-step forward
		dt  = dz * -1.0 * (1.0 + zz) * invq / C;
		dr  = dz * p * invq;
		dth = dz * J * invq * inva1r*inva1r / C;
		dp  = dz * invq * (
					( okr * J*J * inva1r*inva1r*inva1r / (a2a*C*C) )
				+	( 2.*p*(1.0 + zz)*a2d / (a2a*C) )
				-	( p*p * ( (a2p/a2a) + (0.5*(kprime(r)*r*r + 2.*k(r)*r)/okr )) )
				);
		
		// Increment values
		cout << t << "\t" << r << "\t" << rr << "\t" << th << "\t"
			 << p << "\t" << 1./invq << "\t"
			 << zz << "\t" << dt << "\t" << dr << "\t" << dth << "\t"
			 << dp << "\t" << dz << endl;
		
		rr += dr; r = fabs(rr); // FIXME: Is this right?
		t += dt;
		th += dth;
		p += dp;
		zz += dz;
		
	} // end loop through z values
	
	//cerr << "(r, t, th, p) = " << r << ", " << t << ", " << th << ", " << p << endl;
	//cerr << "\tgeneral_offcentre(): " << ((double)clock() - (double)tclock)/CLOCKS_PER_SEC << " seconds." << endl;
	
	return zz;
}
