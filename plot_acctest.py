#!/usr/bin/python

from pylab import *

# Lists of calculated variables
y = []
acc = []

# Get data
f = open("data/acctest", 'r')

for line in f.readlines():
	tmp = line.split("\t")
	if len(tmp) >= 2:
		y.append(float(tmp[0]))
		acc.append(float(tmp[1]))
f.close()


# First plot (physical variables)
subplot(111)
p = plot(y[10000:], acc[10000:], 'k--')
grid(True)
title('FP Accuracy, for j=100')

show()
