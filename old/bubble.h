// Header for main.cpp
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>


// Units: Mass (Msun); Time (Myr); Distance (Mpc)
const double G = 4.4986e-21; // Gravitational constant G, in units of Mpc^3 Msun^-1 Myr^-2
const double C = 0.30659458; // Speed of light, in Mpc/Myr
const double GYR = 1000.; // One Gyr in units of time (Myr)
const double fH0 = 1.02269e-4; // H0 = 100h km s^-1 Mpc^-1 = fH0*h Myr^-1
const double A1_REC = 1e-3; // FRW scale factor at recombination
const double T_REC = 0.4; // Age of universe at recombination (Myr)

const double A_MAX = 1.5; // The maximum a1 value to calculate

const double ts = 1000.0; // 1 GYR
const double rs = 100.0; // 100 MPC

/*
const double G = 6.67e-11; // Gravitational constant G, in units of m^3 kg^-1 s^-2
const double C = 3e8; // Speed of light, in m/s
const double GYR = 3.16e16; // One Gyr in units of time (s)
const double fH0 = 3.24e-18; // H0 = 100h km s^-1 Mpc^-1 = fH0*h Myr^-1
const double A1_REC = 1e-3; // FRW scale factor at recombination
const double T_REC = 1.26e13; // Age of universe at recombination (400 kyr)

const double A_MAX = 1.0; // The maximum a1 value to calculate

const double ts = 1e13; // ~ 1 Myr
const double rs = 3e24; // ~ 100 MPC
*/

#include "ModelDef.cpp"
#include "models_cfl.cpp"
#include "models_gbh.cpp"

#include "NumericGrid.cpp"
#include "geodesic.cpp"
#include "geodesic_general.cpp"
#include "output.cpp"
