

class Grid{


public:

// Constructor
Grid(int RSIZE, int TSIZE){
	/// Constructor for the grid class
	R_SIZE = RSIZE;	T_SIZE = TSIZE;
	build_grid(); // Build an empty grid
	populate_grid(); // Populate the grid with values
}


void build_grid(){
	/// Create new grids
		
	// Create the vectors which will hold the grids, and prepare them for being populated
	vector<double> empty_tslice(T_SIZE, 0.0); // Create T_SIZE time slices, all with empty values
	g_a1 = vector< vector<double> >(R_SIZE, empty_tslice);
	g_a1dot = vector< vector<double> >(R_SIZE, empty_tslice);
	g_a2 = vector< vector<double> >(R_SIZE, empty_tslice);
	g_a2dot = vector< vector<double> >(R_SIZE, empty_tslice);
	
	// FIXME: Temporary variable definitions
	t0 = 1.3; // The age of the universe
	t_end = 1e-5; // The earliest time to calculate back to
	r_max = 5e3; // 10 Gpc, the maximum r to calculate out to
	
	// Find initial (r,t) and step sizes (dr,dt)
	r0 = 1e-6; // Initial r-value (singular at r=0)
	dt = (t0 - t_end) / (double)T_SIZE; // Time step
	dr = (r_max) / (double)R_SIZE; // Radial step
}
	

void populate_grid(){
	/// Populate an (r,t) grid with values of {a1, a1dot, a2, a2dot}
	
	clock_t tclock = clock();
	// Initialise variables that we'll be needing in the loop
	double m = 0.0; double mp = 0.0; // M(r) and M'(r)
	double k = 0.0; double kp = 0.0; double kroot = 0.0; // K(r) and K'(r), and the negative square-root of K
	double b = 0.0; double bp = 0.0; // Beta(r) and Beta'(r)
	double A = 0.0; double B = 0.0; // The cosh/sinh variables
	double Aminus = 0.0; double Ainvmin = 0.0;
	double poly1 = 0.0; double poly2 = 0.0; double poly3 = 0.0; double poly4 = 0.0; double poly5 = 0.0;
	double c = 0.0; // The calculated constant to which we'll be trying to match the function of Theta
	double p = 0.0; double p2 = 0.0; double p4 = 0.0; // Theta, the parameter in Ribeiro's solution, and powers of it
	double acc = 0.0; double pp = 0.0; double p_last = 0.0; // Parameters for finding the root of the equation which gives theta
	double konst = (4. * M_PI * G / 3.); // Constant involving pi, G which crops up a lot
	double r = r0 - dr; // Start off just before the first r value we need
	double t;
	
	ofstream plotfile; plotfile.open("PARAMS");
	
	int TESTp0 = 0; int TESTp1 = 0;
	
	// Loop through each r and t (r,i and t,j)
	for(int i=0; i<R_SIZE; i++){
		
		// Calculate all of the constant-r values that are needed
		r += dr; // Increment r
		m = M(r); mp = Mprime(r);
		k = K(r); kp = Kprime(r);
		kroot = sqrt(-k);
		b = Beta(r); bp = Betaprime(r);
		t = t0 + dt; // Start off just before the first t value we need
		
		// Loop through all t values in this spatial slice
		for(int j=0; j<T_SIZE; j++){
			t -= dt; // Decrement t
			
			// Calculate the parameter theta = p
			// Solve c/(sinh[2p] - 2p) = 1 for p
			c = 3.*(t + b) * (kroot*kroot*kroot) / (4.*M_PI*G*m);
			if(c<1e-24){c=0.0;}
			
			acc = 100.0; pp = 0.1; p_last = p; //p;
			/*****************************/
			/// Find the parameter p = Theta, required to solve the equation for R etc.
			/// Do this by finding the zero of Ribeiro eqn. 10
			/// Can get faster convergence if a good estimate of p_last (~=p) is given
			// Use the Halley iterative method to find the zero of peqn
				// f(p) = c + 2p - sinh(2p)
				// f'(p) = 2 - 2cosh(2p)
				// f''(p) = -4sinh(2p)
			// Get an iterative approximation to better than 1e-8
			while(fabs(acc) > 1e-8){
				p_last = pp;
				pp = pp	-	( 2. * (c + 2.*pp - sinh(2.*pp)) * (2. - 2.*cosh(2.*pp)) )
							/	( 2. * pow( 2. - 2.*cosh(2.*pp) , 2.0) - (c + 2.*pp - sinh(2.*pp)) * (-4. * sinh(2.*pp)) );
				acc = (pp - p_last) / p_last; // Test accuracy
			}
			
			// FIXME: Error-checking, but remove because it slows things down
			if(isnan(p)){ cerr << "WARNING: Failed to converge on value of p in grid.cpp::populate_grid()" << endl; }
			p = pp;
			
			/*****************************/
			
			// Calculate values that we'll be needing in the subsequent evaluations
			// Use expansions for functions which are sensitive to the smallness of p
			int path = 0;

			if(p<1e-3){
				// All expansions calculated around p=0
				path = 0;
				p2 = p*p; p4 = p2*p2; // Powers of p (p^2 and p^4)
				A = 1. + 2.*p2 + (2.*p4/3.);
				B = 2.*p*(1. + (2.*p2/3.) + (2.*p4/15.));
				Aminus = 2.*p2 * ( 1. + (p2/3.) + (2.*p4/45.) );
				Ainvmin = (0.5/p2) + (1./6.)*(-1. + 0.2*p2);
				poly1 = 0.2*8.*p2*p4 + (24.*p4*p4/35.)*( 1. + 0.2*p2 );
				poly2 = (-2.*p4/3.)*( 1. + (4.*p2/15.) + (p4/35.) );
				poly3 = (1./6.) * (1. - 0.05*p2) - (1./(8.*p2));
				poly4 = (32.*p4*p2*p/5.) * (1. + (17.*p2/21.) + (11.*p4/35.) );
				poly5 = (8.*p4*p/3.) * (1. + (8.*p2/15.) + (41.*p4/315.) );
			}else{
				// Use the normal expressions
				path = 1;
				A = cosh(2.*p);
				B = sinh(2.*p);
				Aminus = A - 1.;
				Ainvmin = 1. / (Aminus);
				poly1 = 4.*Aminus + B*B - 6.*B*p;
				poly2 = Aminus - B*p;
				poly3 = -1. / (Aminus * (B*B + 4.));
				poly4 = B*B*B - Aminus*(5.*B - 6.*p);
				poly5 = Aminus * (B - 2.*p);
			}
			
			if(j==4){
			plotfile << r << "\t" <<
				path << "\t" << 
				A << "\t" << 
				B << "\t" << 
				Aminus << "\t" << 
				Ainvmin << "\t" << 
				poly1 << "\t" << 
				poly2 << "\t" << 
				poly3 << "\t" << 
				poly4 << "\t" << 
				poly5 << "\t" << 
				m  << "\t" << 
				k  << "\t" << 
				kroot  << "\t" << 
				mp  << "\t" << 
				bp << "\t" << 
				konst  << "\t" <<
				p << "\t" << 
				c << endl;
			}
			
			/* ***TESTING*** 
			if(r> 1099. and r < 1105.){
				cout 
				<< "r=" << r << endl
				<< "p=" << p << endl
				<< "p2=" << p2 << endl
				<< "p4=" << p4 << endl
				<< "A=" << A << endl
				<< "B=" << B << endl
				<< "Aminus=" << Aminus << endl
				<< "Ainvmin=" << Ainvmin << endl
				<< "poly1=" << poly1 << endl
				<< "poly2=" << poly2 << endl
				<< "poly3=" << poly3 << endl
				<< "poly4=" << poly4 << endl
				<< "poly5=" << poly5 << endl
				<< "konst=" << konst << endl
				<< "m=" << m << endl
				<< "k=" << k << endl
				<< "a1=" << -konst * m * Aminus / k << endl << endl;
			}*/
			
			// Calculate a1
			g_a1[i][j] = -konst * m * Aminus / k;
			
			// Calculate a1dot
			g_a1dot[i][j] = B * kroot * Ainvmin;
			
			// Calculate a2
			g_a2[i][j] = Ainvmin*(		(-0.5 * konst * (m / (k*k)) * (2.*k + r*kp) * poly1)
									+ 	(2.*konst * (3.*m + mp*r) * poly2 / k)
									+	(B * bp / (kroot*r))
								  );
			
			// Calculate a2dot
			g_a2dot[i][j] = poly3*( 	0.5*(2.*k + r*kp)*poly4/kroot
									+	(3.*m + mp*r)*(kroot*poly5 / m)
									-	(bp*k*k*r*Aminus / (m*konst) )
								  );
			
		} // end j loop (time slices)
	} // end i loop (r slices)
	
	//cout << "Taylor: " << TESTp0 << ", Normal: " << TESTp1 << endl;
	
	cerr << "Grid::populate_grid(): " << ((double)clock() - (double)tclock)/CLOCKS_PER_SEC << " seconds." << endl;
	plotfile.close();
} // end grid-populating function










////////////////////////////////////////////////
// Interpolation functions
////////////////////////////////////////////////

double a1(double r, double t){
	// Interpolate to find a1
	
	// Set up an interpolation set (calculates r,t values so we don't have to do it multiple times)
	InterpSet s;
	get_nearest(r, t, &s.i, &s.j);
	s.r1 = get_r(s.i); s.r2 = get_r(s.i+1);
	s.t1 = get_t(s.j); s.t2 = get_t(s.j+1);
	
	// Check if we're at the end of the array
	// FIXME: If so, just use the last array value (could do better than this)
	if(s.i==g_a1.size()-1 or s.j==g_a1[0].size()-1){
		// At the end of the array, so we have no forward sample points
		// FIXME: Use the backwards sample points and interpolate forwards
		return g_a1[s.i][s.j];
		/*return (1./(-dt*dr)) * (
					g_a1[s.i][s.j]		*(s.t2 - t)*(s.r2 - r)
				+	g_a1[s.i][s.j-1]	*(t - s.t1)*(s.r2 - r)
				+	g_a1[s.i-1][s.j]	*(s.t2 - t)*(r - s.r1)
				+	g_a1[s.i-1][s.j-1]	*(t - s.t1)*(r - s.r1)
			);*/
	}else{	
		// Do a simple (bilinear) interpolation
		return (1./(-dt*dr)) * (
					g_a1[s.i][s.j]		*(s.t2 - t)*(s.r2 - r)
				+	g_a1[s.i][s.j+1]	*(t - s.t1)*(s.r2 - r)
				+	g_a1[s.i+1][s.j]	*(s.t2 - t)*(r - s.r1)
				+	g_a1[s.i+1][s.j+1]	*(t - s.t1)*(r - s.r1)
			);
	} // end check for array end
}


double a1dot(double r, double t){
	// Interpolate to find a1dot
	
	// Set up an interpolation set (calculates r,t values so we don't have to do it multiple times)
	InterpSet s;
	get_nearest(r, t, &s.i, &s.j);
	s.r1 = get_r(s.i); s.r2 = get_r(s.i+1);
	s.t1 = get_t(s.j); s.t2 = get_t(s.j+1);
	
	// Check if we're at the end of the array
	// FIXME: If so, just use the last array value (could do better than this)
	if(s.i==g_a1dot.size()-1 or s.j==g_a1dot[0].size()-1){
		// At the end of the array, so we have no forward sample points
		// Just use the array value (FIXME)
		return g_a1dot[s.i][s.j];
	}else{	
		// Do a simple (bilinear) interpolation
		return (1./(-dt*dr)) * (
					g_a1dot[s.i][s.j]		*(s.t2 - t)*(s.r2 - r)
				+	g_a1dot[s.i][s.j+1]		*(t - s.t1)*(s.r2 - r)
				+	g_a1dot[s.i+1][s.j]		*(s.t2 - t)*(r - s.r1)
				+	g_a1dot[s.i+1][s.j+1]	*(t - s.t1)*(r - s.r1)
			);
	} // end check for array end
}

double a2(double r, double t){
	// Interpolate to find a2
	
	// Set up an interpolation set (calculates r,t values so we don't have to do it multiple times)
	InterpSet s;
	get_nearest(r, t, &s.i, &s.j);
	s.r1 = get_r(s.i); s.r2 = get_r(s.i+1);
	s.t1 = get_t(s.j); s.t2 = get_t(s.j+1);
	
	// Check if we're at the end of the array
	// FIXME: If so, just use the last array value (could do better than this)
	if(s.i==g_a2.size()-1 or s.j==g_a2[0].size()-1){
		// At the end of the array, so we have no forward sample points
		// Just use the array value (FIXME)
		return g_a2[s.i][s.j];
	}else{	
		// Do a simple (bilinear) interpolation
		return (1./(-dt*dr)) * (
					g_a2[s.i][s.j]		*(s.t2 - t)*(s.r2 - r)
				+	g_a2[s.i][s.j+1]	*(t - s.t1)*(s.r2 - r)
				+	g_a2[s.i+1][s.j]	*(s.t2 - t)*(r - s.r1)
				+	g_a2[s.i+1][s.j+1]	*(t - s.t1)*(r - s.r1)
			);
	} // end check for array end
}

double a2dot(double r, double t){
	// Interpolate to find a2dot
	
	// Set up an interpolation set (calculates r,t values so we don't have to do it multiple times)
	InterpSet s;
	get_nearest(r, t, &s.i, &s.j);
	s.r1 = get_r(s.i); s.r2 = get_r(s.i+1);
	s.t1 = get_t(s.j); s.t2 = get_t(s.j+1);
	
	// Check if we're at the end of the array
	// FIXME: If so, just use the last array value (could do better than this)
	if(s.i==g_a2dot.size()-1 or s.j==g_a2dot[0].size()-1){
		// At the end of the array, so we have no forward sample points
		// Just use the array value (FIXME)
		return g_a2dot[s.i][s.j];
	}else{	
		// Do a simple (bilinear) interpolation
		return (1./(-dt*dr)) * (
					g_a2dot[s.i][s.j]		*(s.t2 - t)*(s.r2 - r)
				+	g_a2dot[s.i][s.j+1]		*(t - s.t1)*(s.r2 - r)
				+	g_a2dot[s.i+1][s.j]		*(s.t2 - t)*(r - s.r1)
				+	g_a2dot[s.i+1][s.j+1]	*(t - s.t1)*(r - s.r1)
			);
	} // end check for array end
}






// Public variables
int R_SIZE; int T_SIZE; // The number of (r,t) elements in each grid

double t0; // The age of the universe
double t_end; // The earliest time to calculate back to
double r_max; // 10 Gpc, the maximum r to calculate out to
double r0; // Initial r-value (singular at r=0)
double dt; // Time step
double dr; // r step

struct InterpSet{
	/// A set of values to be used for interpolation
	int i, j;
	double r1, r2, t1, t2;
};



private:

void get_nearest(double r, double t, int* i, int* j){
	/// Get the nearest i and j for a given r, t
	*i = (int)floor(  ((r - r0) / (r_max - r0)) * (double)R_SIZE);
	*j = (int)floor(  ((t0 - t) / (t0 - t_end)) * (double)T_SIZE);
}

double get_r(int i){
	/// Return the r value of the cell with index 'i'
	return (double)i * (r_max - r0)/(double)R_SIZE + r0;
}

double get_t(int j){
	/// Return the t value of the cell with index 'j'
	return t0 - ( (double)j * (t0 - t_end)/(double)T_SIZE );
}

// Private grid variables
vector< vector<double> > g_a1;
vector< vector<double> > g_a1dot;
vector< vector<double> > g_a2;
vector< vector<double> > g_a2dot;
	
};
