
class Geodesic{
	
public:

double dt; // Time step to use for the integration
double tcell; // The distance between samples on the y grid
vector<double> rval;
vector<double> tval;
vector<double> zval;
//vector<double> zval2;
NumericGrid* g;
ModelDef* mod;

Geodesic(ModelDef* _mod, NumericGrid *_g){
	/// Constructor: A stored geodesic, r(t) and t(r) values calculated 
	/// using linear interpolation. It is faster to look-up by t(r) 
	/// than r(t) since the values are stored by fixed r-cell size.
	mod = _mod;
	g = _g;
	dt = -1e-1;
	tcell = 1.0;
	generate_geodesic();
}

//z-H0 * dL

double DM(double r){
	/// Find deltaDM (distance modulus) between this model and a Milne universe
	/// (Final result needs correcting by setting deltaDM(z=0) = 0)
	/// (In practise, need to use r slightly greater than 0, to avoid zeros)
	return 5. * log( dL(r) / dL_Milne(r) ) / log(10.);
}

double DM_milne(double r){
	/// DM for Milne cosmology
	return 5. * log( dL_Milne(r) ) / log(10.);
}

double dL_Milne(double r){
	/// The luminosity distance in a Milne universe
	/// dL = z(1 + z/2) * c/H0
	double zz = z(r);
	return zz * (1. + 0.5*zz) * C / mod->h;
	//return zz * (1. + 0.5*zz) * C * g->t_today;
}

double dL(double r){
	/// Find the (LTB) luminosity distance to some comoving radius r
	double zz = z(r);
	return pow(1. + zz, 2.) * r * g->a1(r, fn_t(r));
}

/////////////// EdS functions ///////////////

double DM_EdS(double r){
	/// Distance modulus for EdS
	return 5. * log( dL_EdS(r) / dL_Milne(r) ) / log(10.);
}

double dL_EdS(double r){
	/// The luminosity distance in an EdS universe
	double zz = z_EdS(r);
	return (1. + zz) * r;
}

double EdS_fn_r(double t){
	/// r(t) in an EdS universe
	double rho = mod->m(0.0); // This will work even if m=m(r), since rho=const in EdS (FIXME: M(x), not M(r)!)
	double t0 = g->t_today; // But is this the right t0 for EdS?
	return -3. * C * t0 * (pow((t/t0), 1./3.) - 1.);
}

double z_EdS(double r){
	/// Redshift in an Einstein-de Sitter space
	double t0 = g->t_today; // But is this the right t0 for EdS?
	return pow(1. - (r / (3. * C * t0)), -2.) - 1.;
}

/////////////// End EdS functions ///////////////

double dA(double r){
	/// Find the angular diameter distance to some comoving radius r
	return r * g->a1(r, fn_t(r));
}

double z(double r){
	/// Get the redshift z(r)
	
	// Get the index of the nearest value of r (need to search t)
	int a = 0; int b = rval.size()-1;
	int mid;
	
	// Check that the r being asked for is sensible
	if(!(rval[a] <= r and rval[b] >= r)){
		cerr << "WARNING: r=" << r << " falls outside the range of the calculated values " << rval[a] << " to " << rval[b] << ". geodesic.cpp::z()" << endl;
		return -0.0;
	}
	
	// Search for the cells bounding the correct time
	while( (b - a) > 1 ){
		mid = a + floor(0.5 * (double)(b - a)); // Find the middle cell
		if(r <= rval[mid]){
			b = mid; // a <= r <= mid
		}else{
			a = mid; // mid < r <= b
		} // end range check
	} // end searching loop
	
	// Check if we're hitting a boundary
	if(a == zval.size() - 1){
		// FIXME: Use the *backwards* gradient to do the interpolation
		// For now, just return the latest value
		return zval[a];
	}else{
		// Do proper forwards linear interpolation
		return zval[a] + (r - rval[a]) * (zval[a+1] - zval[a]) / (rval[a+1] - rval[a]);
	} // end boundary check
	
}

double fn_r(double t){
	/// Find r given t by doing a linear interpolation
	
	int a = 0; int b = tval.size()-1;
	int mid;
	
	// Check that the t being asked for is sensible
	if(!(tval[a] >= t and tval[b] <= t)){
		cerr << "WARNING: t=" << t << " falls outside the range of the calculated values " << tval[a] << " to " << tval[b] << ". geodesic.cpp::fn_r()" << endl;
		return -0.0;
	}
	
	// Search for the cells bounding the correct r
	while( (b - a) > 1 ){
		mid = a + floor(0.5 * (double)(b - a)); // Find the middle cell
		if(t >= tval[mid]){
			b = mid; // a <= t <= mid
		}else{
			a = mid; // mid < t <= b
		} // end range check
	} // end searching loop
	
	// Get the index of the nearest value of t
	//int a = (int)floor((g->t_today - t)/tcell);
	
	// Check if we're hitting a boundary
	if(a == tval.size() - 1){
		// FIXME: Use the *backwards* gradient to do the interpolation
		// For now, just return the latest value
		return rval[a];
	}else{
		// Do proper forwards linear interpolation
		// FIXME: Check that this is interpolating the right way!
		return rval[a] + (tval[a] - t) * (rval[a+1] - rval[a]) / (tval[a+1] - tval[a]);
	} // end boundary check
	
}

double fn_t(double r){
	/// Find t given r by doing a linear interpolation
	
	// Get the index of the nearest value of r
	//int i = (int)floor((r - g->r0)/tcell);
	
	// Get the index of the nearest value of r (need to search t)
	int a = 0; int b = rval.size()-1;
	int mid;
	//bool found = false;
	
	// Check that the r being asked for is sensible
	if(!(rval[a] <= r and rval[b] >= r)){
		cerr << "WARNING: r=" << r << " falls outside the range of the calculated values " << rval[a] << " to " << rval[b] << ". geodesic.cpp::fn_t()" << endl;
		return -0.0;
	}
	
	// Search for the cells bounding the correct time
	while( (b - a) > 1 ){
		mid = a + floor(0.5 * (double)(b - a)); // Find the middle cell
		if(r <= rval[mid]){
			b = mid; // a <= r <= mid
		}else{
			a = mid; // mid < r <= b
		} // end range check
	} // end searching loop
	
	// Check if we're hitting a boundary
	if(a == rval.size() - 1){
		// FIXME: Use the *backwards* gradient to do the interpolation
		// For now, just return the latest value
		return tval[a];
	}else{
		// Do proper forwards linear interpolation
		return tval[a] + (r - rval[a]) * (tval[a+1] - tval[a]) / (rval[a+1] - rval[a]);
	} // end boundary check
}

double fn_tz(double z){
	/// Find t given z by doing a linear interpolation
	
	int a = 0; int b = zval.size()-1;
	int mid;
	
	// Check that the z being asked for is sensible
	if(!(zval[a] <= z and zval[b] >= z)){
		cerr << "WARNING: z=" << z << " falls outside the range of the calculated values " << zval[a] << " to " << zval[b] << ". geodesic.cpp::fn_tz()" << endl;
		return -0.0;
	}
	
	// Search for the cells bounding the correct r
	while( (b - a) > 1 ){
		mid = a + floor(0.5 * (double)(b - a)); // Find the middle cell
		if(z <= zval[mid]){
			b = mid; // a <= z <= mid
		}else{
			a = mid; // mid < z <= b
		} // end range check
	} // end searching loop
	
	// Check if we're hitting a boundary
	if(a == zval.size() - 1){
		// FIXME: Use the *backwards* gradient to do the interpolation
		// For now, just return the latest value
		return tval[a];
	}else{
		// Do proper forwards linear interpolation
		// FIXME: Check that this is interpolating the right way!
		return tval[a] + (zval[a] - z) * (tval[a+1] - tval[a]) / (zval[a+1] - zval[a]);
	} // end boundary check
	
}


private:

void generate_geodesic(){
	/// Build a geodesic by integrating along dt to find (r,t) at 
	/// multiple sample points
	/// Also create a geodesic sample
	
	clock_t tclock = clock();
	double k1, k2, k3, k4;
	dt = (g->t0 - g->t_today)/1e6;
	double dtr = dt * C; // dt in distance units
	double dr = 0.0; double r = g->r0;
	double tlast = g->t_today; // How far we've travelled in t since the last sample
	double dI = 0.0; double dIlast = 0.0; double I = 0.0;
	//double dI2 = 0.0; double dIlast2 = 0.0; double I2 = 0.0;
	
	cerr << "Start generate_geodesic()" << endl;
	
	// Add initial values to the r,t arrays
	rval.push_back(r); tval.push_back(g->t_today);
	
	// FIXME: have to stop a step early, because it isn't dealing with t+dt < t_end
	cerr << "*** end time: " << g->t0 - dt << endl;
	for(double t=g->t_today; t > g->t0 - dt; t+=dt){
		
		// Calculate Runge-Kutta variables
		k1 = -1. * sqrt(1. - mod->k(r)*r*r) / g->a2(r, t);
		k2 = -1. * sqrt(1. - mod->k(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1))
				/ g->a2(r + 0.5*dtr*k1, t + 0.5*dt);
		k3 = -1. * sqrt(1. - mod->k(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2))
				/ g->a2(r + 0.5*dtr*k2, t + 0.5*dt);
		k4 = -1. * sqrt(1. - mod->k(r + dtr*k3)*(r + dtr*k3)*(r + dtr*k3))
				/ g->a2(r + k3*dtr, t + dt);
		
		// Add value to r
		dr = (dtr/6.)*(k1 + 2.*k2 + 2.*k3 + k4);
		r += dr;
		
		// Add value to redshift integral
		// FIXME: Doesn't properly deal with the first couple of values!
		dI = -1. * g->a2dot(r, t) / g->a2(r, t);
		I += dI + dIlast;
		dIlast = dI;
		
		// Add the next sample, if it's time to do so
		if((tlast - t) > tcell){
			tlast = t;
			rval.push_back(r);
			tval.push_back(t);
			// This is OK, the dimensions work out; C is cancelled in 
			// the calculation, and we're only multiplying by the 
			// constant factor (dt/2) once for speed.
			zval.push_back( exp(I * 0.5 * dt) - 1. );
		} // end sampling check
		
	} // end integration loop
	
	cerr << "Geodesic::generate_geodesic(): " << ((double)clock() - (double)tclock)/CLOCKS_PER_SEC << " seconds." << endl;
}

/*
double z(NumericGrid *g, Geodesic *geo, double r_end){
	/// Integrate along the geodesic to find the redshift
	
	double I = 0.0;
	double r = g->r0;
	double dr = 1e-4; // 1e-5?
	
	while(r < r_end){
		// Simple Simpson(?) integration
		// FIXME: Use RK4 for accuracy
		
		//cerr << "r=" << r << ", t(r)=" << geo->fn_t(r) << endl;
		
		I += 	g->a2dot(r, geo->fn_t(r)) / sqrt(1. - r*r*mod->k(r))
			 +	g->a2dot(r+dr, geo->fn_t(r+dr)) / sqrt(1. - (r+dr)*(r+dr)*mod->k(r+dr));
		
		r += dr;
		
	}// end integration of redshift equation
	
	I *= 0.5 * (dr/C); // Scale I by deferring multiplications until outside of the loop
	
	return exp(I) - 1.;
}*/

/*
double t_r(double r_end){
	/// Find the time at which an incoming radial geodesic will be at 
	/// radius r_end
	
	double k1, k2, k3, k4, invroot;
	double dr = 1e-6;
	double drt = dr / C; // dr in time units
	double t = g->t0;
	
	for(double r=g->r0; r < r_end; r+=dr){
		invroot = 1. / sqrt(1. - K(r + 0.5*dr)*(r + 0.5*dr)*(r + 0.5*dr));
		k1 = -1. * g->a2(r, t) / sqrt(1. - K(r)*r*r);
		k2 = -1. * g->a2(r + 0.5*dr, t + 0.5*drt*k1) * invroot;
		k3 = -1. * g->a2(r + 0.5*dr, t + 0.5*drt*k2) * invroot;
		k4 = -1. * g->a2(r + dr, t + drt*k3) 
			/ sqrt(1. - K(r + dr)*(r + dr)*(r + dr));
		
		t += (dr/6.)*(k1 + 2.*k2 + 2.*k3 + k4);
	} // end integration loop
	
	return t;
} */
	
};
