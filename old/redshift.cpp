
// OBSOLETE

class Redshift(){

public:

double z(NumericGrid *g, Geodesic *geo, double r_end){
	/// Integrate along the geodesic to find the redshift
	
	double I = 0.0;
	double r = g->r0;
	double dr = 1e-5;
	
	while(r < r_end){
		// Simple Simpson(?) integration
		// FIXME: Use RK4 for accuracy
		
		//cerr << "r=" << r << ", t(r)=" << geo->fn_t(r) << endl;
		
		I += 	g->a2dot(r, geo->fn_t(r)) / sqrt(1. - r*r*K(r))
			 +	g->a2dot(r+dr, geo->fn_t(r+dr)) / sqrt(1. - (r+dr)*(r+dr)*K(r+dr));
		
		r += dr;
		
	}// end integration of redshift equation
	
	I *= 0.5 * (dr/C); // Scale I by deferring multiplications until outside of the loop
	
	return exp(I) - 1.;
}

};

double t_r(double r_end, Grid *g){
	/// Find the time at which an incoming radial geodesic will be at 
	/// radius r_end
	
	double k1, k2, k3, k4, invroot;
	double dr = 1e-6;
	double drt = dr / C; // dr in time units
	double t = g->t0;
	
	for(double r=g->r0; r < r_end; r+=dr){
		invroot = 1. / sqrt(1. - K(r + 0.5*dr)*(r + 0.5*dr)*(r + 0.5*dr));
		k1 = -1. * g->a2(r, t) / sqrt(1. - K(r)*r*r);
		k2 = -1. * g->a2(r + 0.5*dr, t + 0.5*drt*k1) * invroot;
		k3 = -1. * g->a2(r + 0.5*dr, t + 0.5*drt*k2) * invroot;
		k4 = -1. * g->a2(r + dr, t + drt*k3) 
			/ sqrt(1. - K(r + dr)*(r + dr)*(r + dr));
		
		t += (dr/6.)*(k1 + 2.*k2 + 2.*k3 + k4);
	} // end integration loop
	
	return t;
}

double r_t(double t_end, Grid *g){
	/// Build a geodesic by integrating along dt to find (r,t) at 
	/// multiple sample points
	
	double dt = -1e-7;
	double k1, k2, k3, k4;
	double dtr = dt * C; // dt in distance units
	double r = g->r0;
	
	for(double t=g->t0; t > t_end - dt; t+=dt){
		k1 = -1. * sqrt(1. - K(r)*r*r) / g->a2(r, t);
		k2 = -1. * sqrt(1. - K(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1))
				/ g->a2(r + 0.5*dtr*k1, t + 0.5*dt);
		k3 = -1. * sqrt(1. - K(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2))
				/ g->a2(r + 0.5*dtr*k2, t + 0.5*dt);
		k4 = -1. * sqrt(1. - K(r + dtr*k3)*(r + dtr*k3)*(r + dtr*k3))
				/ g->a2(r + k3*dtr, t + dt);
		
		// Add value to r
		r += (dt/6.)*(k1 + 2.*k2 + 2.*k3 + k4);
		
	} // end integration loop
	
	return r;
}






















double a1(double r, double t){
	/// Interpolate to find a1
	
	
	// Set up an interpolation set (calculates r,t values so we don't have to do it multiple times)
	InterpSet s;
	get_nearest(r, t, &s.i, &s.j);
	s.r1 = get_r(s.i); s.r2 = get_r(s.i+1);
	s.t1 = get_t(s.j); s.t2 = get_t(s.j+1);
	
	
	// Check if we're at the end of the array
	// FIXME: If so, just use the last array value (could do better than this)
	if(s.i==g_a1.size()-1 or s.j==g_a1[0].size()-1){
		// At the end of the array, so we have no forward sample points
		// FIXME: Use the backwards sample points and interpolate forwards
		return g_a1[s.i][s.j];
		/*return (1./(-dt*dr)) * (
					g_a1[s.i][s.j]		*(s.t2 - t)*(s.r2 - r)
				+	g_a1[s.i][s.j-1]	*(t - s.t1)*(s.r2 - r)
				+	g_a1[s.i-1][s.j]	*(s.t2 - t)*(r - s.r1)
				+	g_a1[s.i-1][s.j-1]	*(t - s.t1)*(r - s.r1)
			);*/
	}else{	
		// Do a simple (bilinear) interpolation
		//cerr << "r=" << r << ", r1=" << s.r1 << ", r2=" << s.r2 << endl;
		return (1./(TSTEP*(s.r2 - s.r1))) * (
					g_a1[s.i][s.j]		*(s.t2 - t)*(s.r2 - r)
				+	g_a1[s.i][s.j+1]	*(t - s.t1)*(s.r2 - r)
				+	g_a1[s.i+1][s.j]	*(s.t2 - t)*(r - s.r1)
				+	g_a1[s.i+1][s.j+1]	*(t - s.t1)*(r - s.r1)
			);
	} // end check for array end
}


double a1dot(double r, double t){
	// Interpolate to find a1dot
	
	// Set up an interpolation set (calculates r,t values so we don't have to do it multiple times)
	InterpSet s;
	get_nearest(r, t, &s.i, &s.j);
	s.r1 = get_r(s.i); s.r2 = get_r(s.i+1);
	s.t1 = get_t(s.j); s.t2 = get_t(s.j+1);
	
	// Check if we're at the end of the array
	// FIXME: If so, just use the last array value (could do better than this)
	if(s.i==g_a1dot.size()-1 or s.j==g_a1dot[0].size()-1){
		// At the end of the array, so we have no forward sample points
		// Just use the array value (FIXME)
		return g_a1dot[s.i][s.j];
	}else{	
		// Do a simple (bilinear) interpolation
		return (1./(TSTEP*(s.r2 - s.r1))) * (
					g_a1dot[s.i][s.j]		*(s.t2 - t)*(s.r2 - r)
				+	g_a1dot[s.i][s.j+1]		*(t - s.t1)*(s.r2 - r)
				+	g_a1dot[s.i+1][s.j]		*(s.t2 - t)*(r - s.r1)
				+	g_a1dot[s.i+1][s.j+1]	*(t - s.t1)*(r - s.r1)
			);
	} // end check for array end
}

double a2(double r, double t){
	// Interpolate to find a2
	
	// Set up an interpolation set (calculates r,t values so we don't have to do it multiple times)
	InterpSet s;
	get_nearest(r, t, &s.i, &s.j);
	s.r1 = get_r(s.i); s.r2 = get_r(s.i+1);
	s.t1 = get_t(s.j); s.t2 = get_t(s.j+1);
	
	// Check if we're at the end of the array
	// FIXME: If so, just use the last array value (could do better than this)
	if(s.i==g_a2.size()-1 or s.j==g_a2[0].size()-1){
		// At the end of the array, so we have no forward sample points
		// Just use the array value (FIXME)
		return g_a2[s.i][s.j];
	}else{	
		// Do a simple (bilinear) interpolation
		
		return (1./(TSTEP*(s.r2 - s.r1))) * (
					g_a2[s.i][s.j]		*(s.t2 - t)*(s.r2 - r)
				+	g_a2[s.i][s.j+1]	*(t - s.t1)*(s.r2 - r)
				+	g_a2[s.i+1][s.j]	*(s.t2 - t)*(r - s.r1)
				+	g_a2[s.i+1][s.j+1]	*(t - s.t1)*(r - s.r1)
			);
	} // end check for array end
}

double a2dot(double r, double t){
	// Interpolate to find a2dot
	
	// Set up an interpolation set (calculates r,t values so we don't have to do it multiple times)
	InterpSet s;
	get_nearest(r, t, &s.i, &s.j);
	s.r1 = get_r(s.i); s.r2 = get_r(s.i+1);
	s.t1 = get_t(s.j); s.t2 = get_t(s.j+1);
	
	// Check if we're at the end of the array
	// FIXME: If so, just use the last array value (could do better than this)
	if(s.i==g_a2dot.size()-1 or s.j==g_a2dot[0].size()-1){
		// At the end of the array, so we have no forward sample points
		// Just use the array value (FIXME)
		return g_a2dot[s.i][s.j];
	}else{	
		// Do a simple (bilinear) interpolation
		return (1./(TSTEP*(s.r2 - s.r1))) * (
					g_a2dot[s.i][s.j]		*(s.t2 - t)*(s.r2 - r)
				+	g_a2dot[s.i][s.j+1]		*(t - s.t1)*(s.r2 - r)
				+	g_a2dot[s.i+1][s.j]		*(s.t2 - t)*(r - s.r1)
				+	g_a2dot[s.i+1][s.j+1]	*(t - s.t1)*(r - s.r1)
			);
	} // end check for array end
}
