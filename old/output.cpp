
void output_background(ModelDef *mod, NumericGrid *g, Geodesic *geo){
	/// Output background variables (file:background_data)
	ofstream bk; bk.open("background_data");
	double t = g->t_today;
	for(double r=g->r0; r<6000.; r += 2.){
		bk  << r << "\t"
			<< g->density(r, t) << "\t"
			<< g->a1(r, t) << "\t"
			<< g->a1dot(r, t) << "\t"
			<< g->a2(r, t) << "\t"
			<< g->a2dot(r, t) << "\t"
			<< g->Ht(r, t) << "\t"
			<< g->Hr(r, t) << "\t"
			<< mod->k(r) << endl;
	}
	bk.close();
}

void output_geodesic(NumericGrid *g, Geodesic *geo){
	/// Ouput data for graphing geodesics (file:geodesic_data)
	ofstream gs; gs.open("geodesic_data");
	double z=0.0;
	for(double r=0.0; r < 6000.0 and r < geo->rval[geo->rval.size()-1]; r+=2.0){
		z = geo->z(r);
		gs  << r << "\t"
			<< geo->fn_t(r) << "\t"
			<< geo->z(r) << "\t" 
			<< geo->DM(r) << "\t"
			<< geo->z_EdS(r) << "\t"
			<< geo->DM_EdS(r) << "\t"
			<< geo->dL(r) << "\t"
			<< geo->dL_EdS(r) << "\t"
			<< geo->DM_milne(r) << "\t"
			<< endl;
	}
	gs.close();
}


void output_grid_tests(NumericGrid *g){
	/// Output a couple of test runs of a1 at different r
	ofstream gt; gt.open("grid_test_data");
	for(double t=g->t0; t < g->t_today; t+=10.){
		double aaa = g->a1(0.0, t);
		gt  << t << "\t"
			<< g->a1(500.0, t) - aaa << "\t"
			<< g->a1(1000.0, t) - aaa << "\t"
			<< g->a1(1500.0, t) - aaa << "\t"
			<< g->a1(2000.0, t) - aaa << "\t"
			<< endl;
	}
	gt.close();
}

void output_model_stats(NumericGrid *g, Geodesic *geo){
	/// Print information about the model to the terminal
	cerr << "------------------------------" << endl
		 << "t_today = " << g->t_today << " (" << g->t_today / GYR << " Gyr)" << endl
		 << "a1(r=0, t=now) = " << g->a1(0.0, g->t_today) << endl
		 << "a2(r=0, t=now) = " << g->a2(0.0, g->t_today) << endl
		 << "Ht(now) = " << g->Ht(0.0, g->t_today) << endl
		 << "Hr(now) = " << g->Hr(0.0, g->t_today) << endl
		 << "XCELLS = " << g->XCELLS << endl
		 << "YCELLS = " << g->YCELLS << endl
		 << "x_max = " << g->XMAX << endl
		 << "------------------------------" << endl;
}

void output_a1dot(ModelDef *g){
	/// Output a1dot as a function of a, r
	ofstream aa; aa.open("a1dot_data");
	for(double r=0.0; r < 6000.0; r+=2.0){
		double a=1.0;
		aa  << r << "\t"
			<< g->xa1dot(a*2.0, r/rs) << "\t"
			<< g->xa1dot(a*1.2, r/rs) << "\t"
			<< g->xa1dot(a, r/rs) << "\t"
			<< g->xa1dot(a*0.9, r/rs) << "\t"
			<< g->xa1dot(a*0.5, r/rs) << "\t"
			<< g->xa1dot(a*0.1, r/rs) << "\t"
			<< endl;
	}
	aa.close();
}

void output_velocities(ModelDef* mod, Geodesic *geo, NumericGrid *g){
	/// Output the velocities calculated for off-centre observers
	ofstream vel; vel.open("velocity_data");
	// Get the r when z~5
	double rmax = geo->fn_r(geo->fn_tz(5.));
	
	// Output things until then
	for(double r=0.0; r<rmax; r+=100.){
		vel << r << "\t"
			 << geo->z(r) << "\t"
			 << v_dipole(mod, geo, g, r)
			 << endl;
	}
	vel.close();
}
