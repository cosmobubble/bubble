// OBSOLETE

double KR = 0.5e-9;//1e-11; // Last val: 5e-20; //3.7e-8; // Curvature scale, in Mpc^-3
double A = -8e-17; // Last val: -1e-24;//-5e-8;
double h = 0.72; // Hubble's constant today, in our units (T^-1)

double M(double r){
	/// The mass-like LTB function m(r)
	/// Dimensions: Msun/Mpc^3
	//double h = 0.72; double omega_m = 0.001;
	//return omega_m * 3. * h*h / (8. * M_PI * G);
	return 5e0; //1.44e11; //return 1e8; //1.44e11; // EdS rho_cr value
}

double K(double r){
	/// The curvature-like LTB function k(r)
	/// Dimensions: T^-2
	
	double kk = A * exp(-KR * r*r*r);
	if(kk==0.0){
		// K(r) strictly < 0
		// NOTE: This can put a step in K(r) and the evolution 
		// variables can also jump massively as a result!
		// Make sure this value is very small
		kk = -1e-60;
	}else if(kk>0.0){
		cerr << "ERROR: K(r)>0, this is not an allowed value! ltb_functions.cpp::K()" << endl;
		throw 12;
	}
	
	//double kk = 0.0; // EdS value

	return kk;
}

double Beta(double r){
	/// The bang-time function Beta(r)
	/// Dimensions: Time, T (1T = 9.778 Gyr)
	return 0.0;
}

double Mprime(double r){
	/// r-derivative of the mass-like LTB function m(r)
	/// Dimensions: 
	return 0.0;
}

double Kprime(double r){
	/// r-derivative of the curvature-like LTB function k(r)
	/// Dimensions: 
	return -3. * KR * A * r*r * exp(-KR * r*r*r);
	//return 0.0;
}

double Betaprime(double r){
	/// r-derivative of the bang-time function Beta(r)
	/// Dimensions: Time, T/Mpc (1T = 9.778 Gyr)
	return 0.0;
}
