using namespace std;
#include "bubble.h"

int main(int argc, char* argv[]){
	/// Main function
	clock_t tclock = clock();
	
	/*
	// Use command line arguments to define model parameters
	if(argc<2){cerr << "ERROR: Not enough command-line arguments." << endl; return 1;}
	double h = atof(argv[1]);
	double A = atof(argv[2]);
	double KR = atof(argv[3]);
	double M0 = atof(argv[4]);*/
	
	// Define model
	//CFLb v(0.72, -1e-11, 1.0e-10, 1e8); // (h, A, KR, M0)
	//CFLb v(0.72, -1e-11, 1.0e-10); // (h, A, KR)
	///CFLb v(0.72, -1e-9, 1.0e-10); // (h, A, KR)
	///////CFLb v(0.72, -5e-8, 1.0e-10); // (h, A, KR)
	//CFLb v(0.72, -5.75e-8, 1.0e-10); // (h, A, KR)
	CFLb v(0.72, -5.75e-8, 1.0e-9); // (h, A, KR)
	
	// Build a grid and geodesics
	NumericGrid g(&v, 300);
	Geodesic geo(&v, &g);
	
	// Output datafiles
	output_background(&v, &g, &geo);
	//output_grid_tests(&g);
	output_geodesic(&g, &geo);
	//output_velocities(&v, &geo, &g); // Dipole velocities
	output_model_stats(&g, &geo);
	output_a1dot(&v);
	
	//output_background(&gbh, &ggbh, &geo_gbh);
	//output_geodesic(&ggbh, &geo_gbh);
	//output_model_stats(&ggbh, &geo_gbh);
	
	// FIXME: Output this as a model statistic in output_model_stats
	//cerr << "Maximum geodesic r value: " << geo.rval[geo.rval.size()-1] << endl;
	
	// Finish timing the run
	cerr << ((double)clock() - (double)tclock)/CLOCKS_PER_SEC << " seconds." << endl;
	return 0;
}
