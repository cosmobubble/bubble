
class ModelDef{

public:

// Constants used to define the model
/// Hubble's constant today
double h, H0;

ModelDef(double _h = 0.72){
	/// Constructor: An LTB model
	h = _h;
	H0 = fH0 * h;
}

// Dimensionful (normal) functions which define the model

virtual double m(double r){
	/// Density-like function m(r)
	return 0.0;
}

virtual double mprime(double r){
	/// Radial derivative of the density-like function,. m'(r)
	return 0.0;
}

virtual double k(double r){
	/// Curvature-like function k(r)
	return 0.0;
}

virtual double kprime(double r){
	/// Radial derivative of the curvature-like function, k'(r)
	return 0.0;
}

virtual double tb(double r){
	/// Bang-time function t_b(r)
	return 0.0;
}

virtual double tbprime(double r){
	/// Radial derivative of the bang-time function, t_b'(r)
	return 0.0;
}

// Dimensionless functions to be used in the grid integration
double xM(double x){
	/// Dimensionless density-like function, M(x) = 8*pi*G/3 * ts^2 * m(x)
	return ((8.*M_PI*G)/3.) * ts*ts * m(x*rs);
}

double xMprime(double x){
	/// Dimensionless radial derivative of density-like function, M'(x)
	/// M'(x) = 8*pi*G/3 * ts^2 * rs * dm(r)/dr
	return ((8.*M_PI*G)/3.) * ts*ts * rs * mprime(x*rs);
}

double xK(double x){
	/// Dimensionless curvature-like function, K(x) = k(x) * ts^2 * C^2
	return k(rs*x) * ts*ts * C*C;
}

double xKprime(double x){
	/// Dimensionless radial derivative of curvature-like function, K'(x)
	/// K'(x) = k(x) * ts^2 * C^2 * rs
	return kprime(rs*x) * ts*ts * C*C * rs;
}

double xTb(double x){
	/// Bang-time function, t_B(x)
	// FIXME
	return tb(x*rs);
}

double xTbprime(double x){
	/// Radial derivative of Bang-time function, t_B'(x)
	// FIXME
	return tbprime(x*rs);
}

double xa1dot(double a1, double x){
	/// Calculate dimensionless a1dot, given a1 and r
	return sqrt( (xM(x) / a1) - xK(x) );
}

};
