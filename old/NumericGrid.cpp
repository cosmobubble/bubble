

class NumericGrid{


public:

// Global variable declarations
int XCELLS, YCELLS;
double a1_0, x0, y0, dy, y_today, t_today, r0, t0;
double XMAX, XMIN, XSTEP, YSTEP;
ModelDef *mod;

// Constructor
NumericGrid(ModelDef* _mod, int _YCELLS){
	/// Constructor for the grid class
	YCELLS = _YCELLS;
	mod = _mod;
	t_today = 0.0; // We set this properly in normalise()
	build_grid(); // Build an empty grid
	populate_grid(); // Populate the grid with values
	normalise(); // normalise so that a1(x=0, y=now) = 1
}


void build_grid(){
	/// Create new grids
	
	y0 = T_REC/ts; // Initial time
	x0 = 0.0; // Radial coordinate to start at (x=0 is the void centre)
	a1_0 = A1_REC; // Initial a1 at x=0
	r0 = 0.0;
	
	t_today = y_today * ts;
	t0 = y0 * ts;
	
	XMIN = 20; // The minimum x that should be reached before we start considering whether it has reached asymp. value
	XSTEP = 0.01; // The step to take in x when populating the grid
	XMAX = 100;
	XCELLS = XMAX / XSTEP; // Maximum number of x cells to calculate
	
	// Get the time when the Hubble constant is reached locally (x=0, y=now)
	hubble_time();
	YSTEP = (y_today - y0) / ((double)YCELLS);
	dy = YSTEP / 200.;//1e-1; // Time step for integration: Has a big effect on how long the calculation takes!
	
	// Create the vectors which will hold the grids, and prepare them for being populated
	vector<double> empty_tslice(YCELLS+2, 0.0); // Create YCELLS time slices, all with empty values

}
	


double xa1dot(double a1, double x){
	/// Calculate dimensionless a1dot, given a1 and r
	return sqrt( (mod->xM(x) / a1) - mod->xK(x) );
}

double xa1dot(double a1, double m, double k){
	/// Calculate dimensionless a1dot, given a1 and x
	/// Alternative function definition which prevents multiple 
	/// evaluations of M(x) and K(x)
	//cerr << (m / a1) / (-k) << endl;
	return sqrt( (m / a1) - k );
	
}

void hubble_time(){
	/// Calculate the age of the universe today (i.e. when a1dot/a1 = H0(obs))
	double k1, k2, k3, k4; // Runge-Kutta variables
	double m = mod->xM(x0); double k = mod->xK(x0);
	double y; double dy2 = -1. * 1e-4;
	
	double a1 = A_MAX;
	
	cerr << "H_now = " << xa1dot(1.0, m, k) / (fH0*ts) << endl;
	// Integrate until a1(x=0) = 1e-3 (~ decoupling) and set that time to 0.4 Myr
	// Can then calculate t_today
	
	for(y=0.0; a1>A1_REC; y+=dy2){
		
		k1 = xa1dot(a1, m, k);
		k2 = xa1dot(a1 + 0.5*k1*dy2, m, k);
		k3 = xa1dot(a1 + 0.5*k2*dy2, m, k);
		k4 = xa1dot(a1 + k3*dy2, m, k);
		
		// Calculate new a1
		a1 += (dy2/6.) * (k1 + 2.*k2 + 2.*k3 + k4);
		
	} // end time integration
	
	double ysub = y - T_REC/ts;
	y0 = y - ysub;
	y_today = 0.0 - ysub;
	
	t0 = y0 * ts;
	t_today = y_today * ts;
}

void normalise(){
	/// Normalise all of the background expansion variables so that 
	/// a1(x=0, y=now) = a2(x=0, y=now) = 1
	
	// Set the time today
	cerr << "Normalise()" << endl;
}

void populate_grid(){
	/// Populate a mesh with values for a1 using an RK4 ODE solution
	/// to the LTB Friedmann equation
	clock_t tclock = clock();
	double a1, x, y, ylast, m, k;
	double k1, k2, k3, k4; // Runge-Kutta variables
	bool reached_asymptotic = false;
	int j;
	
	x = x0 - XSTEP; // Make sure we start at the right place
	
	// Loop through x values
	for(int i=0; i<XCELLS+2 and !reached_asymptotic; i++){
		
		// Set-up for integration
		x += XSTEP;
		
		g_x.push_back(x); // Add the x value for this cell to the array
		j = 0; a1 = a1_0; ylast = 0.0;
		m = mod->xM(x); k = mod->xK(x);
		
		//cerr << "x=" << x << ", r=" << x*rs << ", m=" << m << ", k=" << k << endl;
		
		// Create new temporary time slice vectors
		// Add an extra 2 elements to each slice to avoid boundary problems when interpolating
		vector<double> tmp_a1(YCELLS+2, 0.0);
		vector<double> tmp_a1dot(YCELLS+2, 0.0);
		vector<double> tmp_a2(YCELLS+2, 0.0);
		vector<double> tmp_a2dot(YCELLS+2, 0.0);
		
		for(y=y0; j<YCELLS+2; y+=dy){
			
			k1 = xa1dot(a1, m, k);
			k2 = xa1dot(a1 + 0.5*k1*dy, m, k);
			k3 = xa1dot(a1 + 0.5*k2*dy, m, k);
			k4 = xa1dot(a1 + k3*dy, m, k);
			
			//if(j==298){cerr << "\tk1 = " << k1 << endl;}
			
			// Calculate new a1
			a1 += (dy/6.) * (k1 + 2.*k2 + 2.*k3 + k4);
			//cerr << "da1 = " << (dy/6.) * (k1 + 2.*k2 + 2.*k3 + k4) << endl;
			
			// Add to mesh if we're at a sample point
			if(y - ylast >= YSTEP){
				tmp_a1[j] = a1;
				//if(j==298){	cerr << a1 << endl; }
				tmp_a1dot[j] = k1 / ts; // k1=a1dot
				
				if(i>1){
					// FIXME: This isn't accurate enough. Should probably do a different integration type!
					tmp_a2[j] = g_a1[i-1][j] + x*XSTEP*(a1 - g_a1[i-2][j]) ;
					tmp_a2dot[j] = ( g_a1dot[i-1][j] + x*XSTEP*(k1/ts - g_a1dot[i-2][j]) );
					// Check for shell crossings
					if(tmp_a2[j] <= 0.0){cerr << "WARNING: Shell crossing occurred at x=" << x << ", y=" << y << " (NumericGrid::populate_grid)" << endl; throw 99;} // end check
				}
				ylast = y; j++;
			} // end sample-point check
		} // end time integration
		
		g_a1.push_back( tmp_a1 );
		g_a1dot.push_back( tmp_a1dot );
		g_a2.push_back( tmp_a2 );
		g_a2dot.push_back( tmp_a2dot );
		
		if(x>XMIN){
			// Check if we've reached an asymptotic value (at y=today)
			// Beware the minimum in a2! Need to set xmin to avoid this
			if(		(g_a2[i-1][YCELLS-1] - g_a2[i-2][YCELLS-1])
				/ 	(g_a2[i-1][YCELLS-1] - g_a2[i-6][YCELLS-1]) < 1e-14 ){
				cerr << "\tAsymptotic value reached at x=" << x << ", a2=" << g_a2[i-1][YCELLS-1] << endl;
				reached_asymptotic = true;
				XCELLS = g_a1.size() - 2;
				XMAX = g_x[XCELLS-1];
			}
		} // end asymptote check
		
	} // end loop through x values
	
	// Tidy-up values that couldn't be calculated efficiently in the loop
	for(int jj=0; jj<YCELLS+2; jj++){
		// be careful with the x being used - x should be xlast, but use x0 for the first ones
		g_a2[0][jj] = g_a1[0][jj] + x0*2.*(g_x[1] - g_x[0])* (g_a1[1][jj] - g_a1[0][jj]);
		g_a2[1][jj] = g_a1[1][jj] + x0*2.*(g_x[2] - g_x[1])* (g_a1[2][jj] - g_a1[1][jj]);
		
		g_a2dot[0][jj] = g_a1dot[0][jj] + x0*2.*(g_x[1] - g_x[0])* (g_a1dot[1][jj] - g_a1dot[0][jj]);
		g_a2dot[1][jj] = g_a1dot[1][jj] + x0*2.*(g_x[2] - g_x[1])* (g_a1dot[2][jj] - g_a1dot[1][jj]);
	}
	
	cerr << "NumericGrid::populate_grid(): " << ((double)clock() - (double)tclock)/CLOCKS_PER_SEC << " seconds." << endl;
} // end initial integration function






////////////////////////////////////////////////
// Interpolation functions
////////////////////////////////////////////////

double a1(double r, double t){
	return get_array_value(r, t, 0);
}
double a1dot(double r, double t){
	/// Units: inverse time
	return get_array_value(r, t, 1);
}
double a2(double r, double t){
	return get_array_value(r, t, 2);
}
double a2dot(double r, double t){
	/// Units: inverse time
	return get_array_value(r, t, 3);
}


double get_array_value(double r, double t, int v){
	/// Generic interpolation 
	
	int i, j, a, b, mid;
	double x1, x2, y1, y2;
	double xcorr = 0.0; double ycorr = 0.0;
	a = 0; b = g_x.size() - 3; // Since XCELLS = xsize - 2
	
	double x = r/rs;
	double y = t/ts;
	
	// Sanity checks on x, y
	if(x < g_x[a]){
		cerr << "ERROR: x=" << x << " outside calculated range: " << g_x[a] << " to " << g_x[b] << endl; throw 71;
	}
	if(y < y0 or y > y_today){ cerr << "ERROR: y=" << y << " outside calculated range y0=" << y0 << ", y_today=" << y_today << endl; throw 72;}
	
	// Check if x is too big, and if so, get the asymptotic value
	 if(x >= g_x[b-1]){
		 x = g_x[b-1]; // Set x to value where expn. vars. reach asymptotic values, it'll return the same results
	 }
	
	// Get i<=>x by searching g_x
	while( (b - a) > 1 ){
		mid = a + floor(0.5 * (double)(b - a)); // Find the middle cell
		if(x <= g_x[mid]){
			b = mid; // a <= x <= mid
		}else{
			a = mid; // mid < x <= b
		} // end range check
	} // end x-searching loop
	i = a;
	
	// Get j<=>y by looking it up from the index (assumes fixed grid cell size in y)
	j = (int)floor(  ((y - y0) / (y_today - y0)) * (double)YCELLS);
	if(j >= YCELLS){j = YCELLS - 1;} // Check we're not right at the top of the range
	
	// Check if x, y indices are safe to do forward interpolation
	double gxsize = g_x.size();
	if(i >= gxsize and j < YCELLS){
		// We're at the x boundary. FIXME: Check for validity
		xcorr = g_x[i] - g_x[i-1]; // Correct for shift in cell used to calculate gradient
		i -= 1; // We're at the x boundary, interpolate using a backwards step (fix the indices)
	}else if(i < gxsize and j >= YCELLS){
		ycorr = YSTEP; j -= 1; // We're at the y boundary
	}else if(i >= gxsize and j >= YCELLS){
		xcorr = g_x[i] - g_x[i-1];
		ycorr = YSTEP; i -= 1; j-=1; // We're at the x and y boundary!
	}
	
	// Get values for x1, x2, y1, y2
	x1 = g_x[i]; x2 = g_x[i+1];
	y1 = y0 + ((double)j * YSTEP); y2 = y1 + YSTEP;
	
	// Do a simple (bilinear) interpolation
	double arr1, arr2, arr3, arr4;
	switch(v){
		case 0: arr1 = g_a1[i][j]; arr2 = g_a1[i][j+1]; 
				arr3 = g_a1[i+1][j]; arr4 = g_a1[i+1][j+1]; break;
		case 1: arr1 = g_a1dot[i][j]; arr2 = g_a1dot[i][j+1]; 
				arr3 = g_a1dot[i+1][j]; arr4 = g_a1dot[i+1][j+1]; break;
		case 2: arr1 = g_a2[i][j]; arr2 = g_a2[i][j+1]; 
				arr3 = g_a2[i+1][j]; arr4 = g_a2[i+1][j+1]; break;
		case 3: arr1 = g_a2dot[i][j]; arr2 = g_a2dot[i][j+1]; 
				arr3 = g_a2dot[i+1][j]; arr4 = g_a2dot[i+1][j+1]; break;
		default: cerr << "WARNING: No case was chosen in NumericGrid::get_array_value()" << endl;
				 break;
	}
	
	return (1./(YSTEP*(x2 - x1))) * (
				arr1 * (ycorr + y2 - y)*(xcorr + x2 - x)
			+	arr2 * (ycorr + y - y1)*(xcorr + x2 - x)
			+	arr3 * (ycorr + y2 - y)*(xcorr + x - x1)
			+	arr4 * (ycorr + y - y1)*(xcorr + x - x1)
		);
}


double density(double r, double t){
	/// The physical density at some point in the spacetime
	double _a1 = a1(r, t);
	double _a2 = a2(r, t);
	return ( 1. / (_a1*_a1*_a2) ) * (mod->m(r) + (r*mod->mprime(r)/3.));
}

double Ht(double r, double t){
	/// The transverse Hubble rate at a given r, t, in physical units (km s^-1 Mpc^-1)
	return a1dot(r, t) / (a1(r, t) * fH0);
}

double Hr(double r, double t){
	/// The radial Hubble rate at a given r, t, in physical units (km s^-1 Mpc^-1)
	return a2dot(r, t) / (a2(r, t) * fH0);
}


private:

// Private grid variables
vector<vector <double> > g_a1;
vector<vector <double> > g_a1dot;
vector<vector <double> > g_a2;
vector<vector <double> > g_a2dot;
vector<double> g_x;
};
