
double outgoing_redshift(ModelDef* mod, NumericGrid *g, double r_start, double t_start, double t_end){
	/// Return the redshift for rays going out of the void
	double dt = -1e-1;
	double k1, k2, k3, k4;
	double dtr = dt * C; // dt in distance units
	double dr = 0.0;
	double dI = 0.0; double dIlast = 0.0; double I = 0.0;
	
	// Our carefully-chosen initial conditions
	double r = r_start;
	double tlast = t_start; // How far we've travelled in t since the last sample
	
	for(double t=t_start; t > t_end - dt; t+=dt){
		k1 = -1. * sqrt(1. - mod->k(r)*r*r) / g->a2(r, t);
		k2 = -1. * sqrt(1. - mod->k(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1))
				/ g->a2(r + 0.5*dtr*k1, t + 0.5*dt);
		k3 = -1. * sqrt(1. - mod->k(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2))
				/ g->a2(r + 0.5*dtr*k2, t + 0.5*dt);
		k4 = -1. * sqrt(1. - mod->k(r + dtr*k3)*(r + dtr*k3)*(r + dtr*k3))
				/ g->a2(r + k3*dtr, t + dt);
		
		dr = (dtr/6.)*(k1 + 2.*k2 + 2.*k3 + k4); r += dr;
		dI = -1. * g->a2dot(r, t) / g->a2(r, t);
		I += dI + dIlast; dIlast = dI;
	} // end integration loop
	return exp(I * 0.5 * dt) - 1.; // Redshift, z
}

double ingoing_redshift(ModelDef* mod, NumericGrid *g, double r_start, double t_start, double t_end){
	/// Return the redshift for rays going into the void
	double dt = -1e-1; double t;
	double k1, k2, k3, k4;
	double dtr = dt * C; // dt in distance units
	double dr = 0.0;
	double dI = 0.0; double dIlast = 0.0; double I = 0.0;
	
	// Our carefully-chosen initial conditions
	double r = r_start;
	double tlast = t_start; // How far we've travelled in t since the last sample
	
	// First, integrate along the ingoing geodesic, until r=0.0 is reached or t_end is reached
	for(t=t_start; r>0.0 and t>t_end; t+=dt){
		k1 = -1. * sqrt(1. - mod->k(r)*r*r) / g->a2(r, t);
		k2 = -1. * sqrt(1. - mod->k(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1))
				/ g->a2(r + 0.5*dtr*k1, t + 0.5*dt);
		k3 = -1. * sqrt(1. - mod->k(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2))
				/ g->a2(r + 0.5*dtr*k2, t + 0.5*dt);
		k4 = -1. * sqrt(1. - mod->k(r + dtr*k3)*(r + dtr*k3)*(r + dtr*k3))
				/ g->a2(r + k3*dtr, t + dt);
		
		dr = (-1. * dtr/6.)*(k1 + 2.*k2 + 2.*k3 + k4); r += dr; // Don't forget the minus sign for inbound geodesics!
		dI = -1. * g->a2dot(fabs(r), t) / g->a2(fabs(r), t); // Need |r| since dr is decreasing and will overshoot r=0.0 slightly
		I += dI + dIlast; dIlast = dI;
	} // end integration loop
	
	// Check if we're at t=t_end. If not, keep integrating along an 
	// outgoing geodesic until we are! 
	if(t<=t_end){
		return exp(-1. * I * 0.5 * dt) - 1.; // Redshift, z. Units are OK, since I in (T^-1)
	}else{
		// Keep integrating along an outgoing geodesic from r=0
		// New initial conditions
		r = 0.0; tlast = t;
		double dI2 = 0.0; double dIlast2 = 0.0; double I2 = 0.0;
		
		// First, integrate along the ingoing geodesic, until r=0.0 is reached or t_end is reached
		// FIXME: K(x), not K(r) any more!
		for(t=tlast; t>t_end; t+=dt){
			k1 = -1. * sqrt(1. - mod->k(r)*r*r) / g->a2(r, t);
			k2 = -1. * sqrt(1. - mod->k(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1)*(r + 0.5*dtr*k1))
					/ g->a2(r + 0.5*dtr*k1, t + 0.5*dt);
			k3 = -1. * sqrt(1. - mod->k(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2)*(r + 0.5*dtr*k2))
					/ g->a2(r + 0.5*dtr*k2, t + 0.5*dt);
			k4 = -1. * sqrt(1. - mod->k(r + dtr*k3)*(r + dtr*k3)*(r + dtr*k3))
					/ g->a2(r + k3*dtr, t + dt);
			
			dr = (dtr/6.)*(k1 + 2.*k2 + 2.*k3 + k4); r += dr;
			dI2 = -1. * g->a2dot(r, t) / g->a2(r, t);
			I2 += dI2 + dIlast2; dIlast2 = dI2;
		} // end integration loop
		
		// Return the sum of the redshifts accumulated along the geodesic
		return exp( (I2 - I) * 0.5 * dt ) - 1.;
	} // end t=t_end check
}

double v_dipole(ModelDef* mod, Geodesic* geo, NumericGrid *g, double r){
	/// Find the dipole velocity (derived from the redshift) at radius r
	/// (in km/sec)
	
	// Find the local time at which point r is observed by a central 
	// observer today
	double t_obs = geo->fn_t(r);
	
	// Find the time at which the redshift is z=20
	// (proxy for z=1100, decoupling)
	double t_dec = geo->fn_tz(10.0);
	
	// Find the redshift for the ray going *out* of the void
	double z_out = outgoing_redshift(mod, g, r, t_obs, t_dec);
	
	// Find the redshift for the ray going *through* the void
	double z_in = ingoing_redshift(mod, g, r, t_obs, t_dec);
	
	// Output redshifts
	//cerr << "\tz_in = " << z_in << endl;
	//cerr << "\tz_out = " << z_out << endl;
	
	// Turn the redshift into a velocity, km/sec
	// (-ve means it appears to be moving out of the void???)
	
	//return (z_out - z_in) /* * 3e5 */;
	// GBH give dT/T|Dipole = (z_in - z_out) / (2 + z_in + z_out)
	return fabs(z_in - z_out) / (2. + z_in + z_out);
}
