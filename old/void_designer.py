#!/usr/bin/python

import gtk
import pylab
from math import *
import scipy.integrate as S

a1_0 = 1e-5
t0 = 1e-5
A = -1e-1
KR = 1e-10
h = 0.72

def plot_density(rho, rr):
	"""Plot the calculated density profile"""
	pylab.cla()
	pylab.clf()
	pylab.subplot(111)
	p = pylab.plot(rr, rho, 'r-')
	pylab.grid(True)
	pylab.title('Density(r)')
	#pylab.show()
	pylab.savefig("tmp.png", dpi=70)
	Graph.set_from_file("tmp.png")
	
	

def rebuild_void(caller):
	"""Build a new void by using parameters taken from the sliders"""
	KR = 10.0 ** (-1.*Swidth.get_value())
	A = -1. * 10**(-1.*Sdepth.get_value())
	
	print "KR =", KR
	print "A =", A
	
	rho, rr = integrate()
	plot_density(rho, rr)

def M(r):
	"""Density function"""
	return 1e11

def K(r):
	"""Curvature-like function"""
	#A = -1e-1
	#KR = 1e-10
	KR = 10.0 ** (-1.*Swidth.get_value())
	A = -1. * 10**(-1.*Sdepth.get_value())
	return A * exp(-KR * r**3.)

def a1dot(a1, r, m, k):
	"""The RHS of the LTB Friedmann equation to be integrated"""
	G = 4.302e-13
	C = 3e3
	return sqrt((8.*pi*G*m / (3.*a1)) - k*C*C)

def hubble_time():
	"""Get the time when h=H0"""
	m = M(0.0)
	k = K(0.0)
	a1 = a1_0
	r0 = 0.0
	H = a1dot(a1_0, r0, m, k)/a1_0;
	t = t0
	dt = 1e-3
	
	# Integrate until H(t) = h, which tells us that t=t_today
	while H>h:
		
		k1 = a1dot(a1, r0, m, k)
		k2 = a1dot(a1 + 0.5*k1*dt, r0, m, k)
		k3 = a1dot(a1 + 0.5*k2*dt, r0, m, k)
		k4 = a1dot(a1 + k3*dt, r0, m, k)
		
		# Calculate new a1
		a1 += (dt/6.) * (k1 + 2.*k2 + 2.*k3 + k4)
		
		# Calculate H to see if we've gone far enough yet (using k1=a1dot)
		H = k1/a1
		t += dt
	
	return t

def integrate2():
	"""Use SciPy to integrate the LTB equations"""
	m = M(0.0)
	k = K(0.0)
	r0 = 0.0
	y = S.odeint(a1dot, y0, t, args=(), Dfun=None, col_deriv=0, full_output=0, ml=None, mu=None, rtol=None, atol=None, tcrit=None, h0=0.0, hmax=0.0, hmin=0.0, ixpr=0, mxstep=0, mxhnil=0, mxordn=12, mxords=5, printmessg=0)
	

def integrate():
	"""Integrate the LTB equations to find a rough estimate of density, rho"""
	rho = []
	rr = []
	dt = 1e-3
	#t_end = 10.
	t_now = hubble_time()
	print "HUBBLE TIME:", t_now
	#t_now = 10.
	dr = 1.0
	r = 0.0
	h = 0.72
	RMAX = 5000.
	i = 0
	
	a1_last = 0.0
	a1_last_2 = 0.0
	
	while r < RMAX:
		r += dr
		t = 1e-6
		a1 = a1_0
		m = M(r)
		k = K(r)
		
		# Do time integration to find a1, a1dot today
		while t < t_now:
			t += dt
				
			k1 = a1dot(a1, r, m, k)
			k2 = a1dot(a1 + 0.5*k1*dt, r, m, k)
			k3 = a1dot(a1 + 0.5*k2*dt, r, m, k)
			k4 = a1dot(a1 + k3*dt, r, m, k)
			
			# Calculate new a1
			a1 += (dt/6.) * (k1 + 2.*k2 + 2.*k3 + k4)
		
		# Shuffle previous a1 values	
		a1_last_2 = a1_last
		a1_last = a1
		
		i+=1
		if(i>2):
			if(i%10 == 1):
				a2 = a1_last + r*dr*(a1 - a1_last_2)
				rho.append(m/(a1*a1*a2)) # WARNING: Assumes mprime = 0
				rr.append(r)
	print "Finished integration"
	return rho, rr
				

# Build UI
w = gtk.Window()
w.resize(600, 400)
Awidth = gtk.Adjustment(9., 8., 15., 0.1, 1.0, 1.0)
Swidth = gtk.HScale(Awidth)
Adepth = gtk.Adjustment(1.0, 0.0, 10.0, 0.1, 1.0, 1.0)
Sdepth = gtk.HScale(Adepth)

# Labels
lw = gtk.Label("Void width scale")
ld = gtk.Label("Void depth scale")

# Buttons
go = gtk.Button("Update void")

# Image
Graph = gtk.Image()

# Packing
hbox = gtk.HBox(homogeneous=False)
vbox = gtk.VBox(homogeneous=False)
vbox.pack_start(lw)
vbox.pack_start(Swidth)
vbox.pack_start(ld)
vbox.pack_start(Sdepth)
vbox.pack_start(go)
hbox.pack_start(Graph)
hbox.pack_start(vbox)
w.add(hbox)

# Add UI elements

# Connect signals to UI
w.connect("destroy", lambda x: gtk.main_quit())
go.connect("clicked", rebuild_void)

# Start showing things
w.show_all()
gtk.main()
