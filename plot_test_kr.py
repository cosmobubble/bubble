#!/usr/bin/python

from pylab import *

# Lists of calculated variables
r = [] # Comoving radial distance
density = [] # Physical density
# Scale factors
a1 = []
a1dot = []
a2 = []
a2dot = []
# Hubble rates
Ht = []
Hr = []
k = []
pk = []
pm = []

files = [1000, 1500, 1600, 1700, 3000]
#yyy = [-2.13e-8, -1.05e-8, -0.85e-8, -0.65e-8, 0.0]
yyy = [0.585, 0.75, 0.79, 0.83, 1.0]

# Get data
f = open("data/background", 'r')

for line in f.readlines():
	tmp = line.split("\t")
	if len(tmp) >= 8:
		r.append(float(tmp[0]))
		density.append(float(tmp[1]))
		a1.append(float(tmp[2]))
		a1dot.append(float(tmp[3]))
		a2.append(float(tmp[4]))
		a2dot.append(float(tmp[5]))
		Ht.append(float(tmp[6]))
		Hr.append(float(tmp[7]))
		k.append(float(tmp[8]))
		pk.append(float(tmp[9]))
		pm.append(float(tmp[10]))
f.close()

subplot(111)
plot(r, pm, 'k-')
plot(files, yyy, 'ro')
grid(True)
title('k(r)')

show()
