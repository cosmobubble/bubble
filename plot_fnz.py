#!/usr/bin/python

from pylab import *

# Lists of calculated variables
r = []
density = []
a1 = []
a1dot = []
a2 = []
a2dot = []
Ht = []
Hr = []
k = []
pk = []
pm = []
bzz = []
dHr = []
dHt = []

gr = []
gt = []
gz = []
gdm = []
gzeds = []
gdmeds = []
gdl = []
gdleds = []
gdmmilne = []

rr = []
zz = []
dz = []

# Get data
f = open("data/fnz", 'r')

for line in f.readlines():
	tmp = line.split("\t")
	if len(tmp) >= 11:
		r.append(float(tmp[0]))
		density.append(float(tmp[1]))
		a1.append(float(tmp[2]))
		a1dot.append(float(tmp[3]))
		a2.append(float(tmp[4]))
		a2dot.append(float(tmp[5]))
		Ht.append(float(tmp[6]))
		Hr.append(float(tmp[7]))
		k.append(float(tmp[8]))
		pk.append(float(tmp[9]))
		pm.append(float(tmp[10]))
		bzz.append(float(tmp[11]))
		dHr.append(float(tmp[12]))
		dHt.append(float(tmp[13]))
f.close()

# Get data
g = open("data/geodesic", 'r')

for line in g.readlines():
	tmp = line.split("\t")
	if len(tmp) >= 8:
		gr.append(float(tmp[0]))
		gt.append(float(tmp[1]))
		gz.append(float(tmp[2]))
		gdm.append(float(tmp[3]))
		gzeds.append(float(tmp[4]))
		gdmeds.append(float(tmp[5]))
		gdl.append(float(tmp[6]))
		gdleds.append(float(tmp[7]))
		gdmmilne.append(float(tmp[8]))
g.close()

# Get data
h = open("data/velocity", 'r')

for line in h.readlines():
	tmp = line.split("\t")
	if len(tmp) >= 3:
		rr.append(float(tmp[0]))
		zz.append(float(tmp[1]))
		dz.append(float(tmp[2]))
h.close()

subplot(231)
p = plot(bzz, density, 'k-')
grid(True)
title('Density')
xlim((0.0, 1.4))
#ylim((0.0, 1e12))

subplot(232)
plot(bzz, k, 'b-') # bzz, pm, 'r-')
grid(True)
#title('omega_k/m(r)')
title('k(z)')
xlim((0.0, 1.4))

subplot(233)
plot(bzz, Ht, 'b-', bzz, Hr, 'r-')
grid(True)
title('Hubble Rates')
#xlim((0.0, 1.0))
xlim((0.0, 1.4))

subplot(234)
plot(bzz, dHr, 'r-', bzz, dHt, 'b-')
grid(True)
title('Radial derivatives of Hubble rates')
#xlim((0.0, 1.0))
#ylim((0.5, 2.0))
xlim((0.0, 1.4))

subplot(235)
p = plot(zz, dz, 'r+', zz, dz, 'r-')
grid(True)
title('vp(z)')
#xlim((0.0, 1.0))
xlim((0.0, 1.4))

subplot(236)
plot(gz, gdm, 'b-')
grid(True)
title('DM(z)')
#xlim((0.0, 1.0))
#ylim((-0.2, 0.4))
xlim((0.0, 1.4))

show()
