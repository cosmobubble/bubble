#!/usr/bin/python

from pylab import *
import sys

# Lists of calculated variables

rad = [1, 2, 4, 10, 20, 30, 40]
cols = ['r-', 'r--', 'b-', 'b--', 'g-', 'g--', 'k-']
r = []
z = []
dz = []
dHr = []

# Get data
subplot(111)
i=-1
for s in rad:
	i+=1
	f = open("data/velocity-" + str(s), 'r')
	print "data/velocity-" + str(s)
	for line in f.readlines():
		r.append([])
		z.append([])
		dz.append([])
		dHr.append([])
		tmp = line.split("\t")
		
		if len(tmp) >= 3:
			r[i].append(float(tmp[0]))
			z[i].append(float(tmp[1]))
			dz[i].append(float(tmp[2]))
			dHr[i].append(float(tmp[3]))
	plot(z[i], dz[i], cols[i], label=("z(LSS) = " + str(s)))
	f.close()

grid(True)
title('v_p(z), z(LSS) varies')
xlabel("z")
ylabel("v_p/c")
legend()

show()
